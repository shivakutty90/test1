from openerp.osv import fields, osv
from openerp import osv, models, fields, api

class crm_case_section(models.Model):
    _inherit = "crm.case.section"

    company_id = fields.Integer()

    def create(self, cr, uid, vals, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=None)
        vals['company_id'] = user.company_id.id

        return super(crm_case_section, self).create(cr, uid, vals, context)

class ecom_hr_employee(models.Model):
    _inherit = "hr.employee"


class event_event(models.Model):
    _inherit = "event.event"

    city = fields.Char('City')

class website(models.Model):
    _inherit = "website"

    def _get_events_citys(self):
        citys = []
        events =  self.env['event.event'].search([])
        for x in events:
            if x['city'] not in citys:
                citys.append(x['city'])
        citys.insert(0, 'All')        
        return citys

    def _get_events(self):
        return self.env['event.event'].search([])       
