{
	'name': 'Ecom Enhancements',
	'author': 'Jothimani',
	'depends': ['base', 'sale','hr','website','event_sale','event'],
	'data': ['views/views.xml',
			 'views/templates.xml'],
	'installable': True,
    # 'js': ['static/src/js/*.js'],
    # 'qweb': ['static/src/xml/*.xml'], 	
}
