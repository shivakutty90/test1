$(document).ready(function() {
	var city = getParameterByName('city');
	if (!city){
		var city = $('.city_filter').find('ul li:first').text();
		console.log(city);
	}
	$('.city_filter').find('.selection').text(city+' ');
	$('.city_filter').find('.selection').val(city+' ');
	// Get city name from url for setting the city value
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
});