from openerp import models, fields, api
import openerp.tools as tools
import itertools
from openerp.http import request

class sale_order_line_b2b(models.Model):
    _inherit = 'sale.order.line'

    product_id1 = fields.Many2one('product.template', string='Product1', store=True)

    @api.onchange('product_id1')
    def product_id1_change(self):
        if not len(self.product_id1.product_variant_ids) > 1:
            idss = self.env['product.product'].search([('product_tmpl_id', '=', int(self.product_id1))])
            if idss:
                self.product_id = idss[0] 
                sss = self.order_id.create({'order_line':[ (0, 0, {'name':'value','product_id':1,'delay':0.0,'product_uom_qty':0,'price_unit':1})]})
        else:
            self.product_id = False

    def get_attribute_value_ids(self, product):
        attribute_value_ids = []
        visible_attrs = set(l.attribute_id.id for l in product.attribute_line_ids if len(l.value_ids) > 1)
		
        for p in product.product_variant_ids:
            attribute_value_ids.append([p.id, [v.id for v in p.attribute_value_ids if v.attribute_id.id in visible_attrs]])
			
	return attribute_value_ids

    @api.model
    def get_variant_matrix(self):
        context = self.env.context
        product_id = context.get('product_id')
        product_obj = self.env['product.template']

        p_value_ids = []
        p_value_ids_variant_id = []

        if not product_id:
            return
        product = product_obj.browse(product_id)
        attribute_value_ids = self.get_attribute_value_ids(product)

        thlength = len(product.attribute_line_ids[0].value_ids)
        thead = [str(x.name) for x in product.attribute_line_ids[0].value_ids]
        thead.insert(0, str(product.attribute_line_ids[0].attribute_id.name))    # add attribute name
        # thead.insert(0, product.id)                                              # add product id in index 0


        # attribute_value_ids = self.get_attribute_value_ids(product)
        p_att_line_ids = product.attribute_line_ids
        for x in p_att_line_ids:
            p_value_ids.append([y.id for y in x.value_ids])
        tmp_compinations_list_of_tuple = list(itertools.product(*p_value_ids))
        p_att_value_compinations = map(list, tmp_compinations_list_of_tuple)
        tmp_compination_list = []
        for x in p_att_value_compinations:
            for i in xrange(0,len(attribute_value_ids)):
                if set(x) == set(attribute_value_ids[i][1]):
                    tmp_rows = [x]
                    tmp_rows.insert(0,int(attribute_value_ids[i][0]))
                    tmp_compination_list.append(tmp_rows)
        trlength = len(tmp_compination_list)/thlength
		
        final_tmp_compination_list = list(zip(*[tmp_compination_list[i:i+trlength] for i in range(0, len(tmp_compination_list), trlength)]))
        rows_list = map(list, final_tmp_compination_list)
        trow = []
        for row in rows_list:
            at_value = self.env['product.attribute.value'].browse(row[0][1][1:])
            at_value_names = [str(x.name) for x in at_value]
            y_axis_compinations = ""
            for i in xrange(0,len(at_value_names)):
                if i == 0:
                    y_axis_compinations += str(at_value_names[i])
                else:
                    y_axis_compinations += '/'+str(at_value_names[i])
            for i in xrange(0, len(row)):
                del row[i][1:]
            tr = [y_axis_compinations,list(itertools.chain(*row))]
            trow.append(tr)
        return [thead,trow]

