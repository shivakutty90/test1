{
    'name': 'Pre-Order and Sale Discount for Total Amount',
    'version': '1.0',
    'category': 'sale',
    'sequence': 6,
    'summary': "",
    'author': 'Cats&Dogs bvba',
    'website' : 'http://www.catsanddogs.com',
    'description': """
""",
    'depends': ['sale', 'sale_discount_total', 'website_preorder'],
    'data': [
    ],
    'installable': True,
    'auto_install': False,
}
