from openerp.osv import fields, osv
from openerp import api
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID


class SaleOrder(osv.Model):
    _inherit = 'sale.order'
    
    def _amount_all_wrapper(self, cr, uid, ids, field_name, arg, context=None):
        """ Wrapper because of direct method passing as parameter for function fields """
        return self._amount_all(cr, uid, ids, field_name, arg, context=context)

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_discount': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            cur = order.pricelist_id.currency_id
            val1 = val2 = val3 = 0.0
            for line in order.order_line:
                val1 += line.price_subtotal
                val2 += self._amount_line_tax(cr, uid, line, context=context)
                val3 += (line.product_uom_qty * line.price_unit) * line.discount / 100
            res[order.id]['amount_untaxed'] = round(cur_obj.round(cr, uid, cur, val1))
            res[order.id]['amount_tax'] = round(cur_obj.round(cr, uid, cur, val2))
            res[order.id]['amount_discount'] = round(cur_obj.round(cr, uid, cur, val3))
            res[order.id]['amount_total'] = round(res[order.id]['amount_untaxed'] + res[order.id]['amount_tax'])
        return res

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()

    _columns = {
        'amount_untaxed': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The amount without tax.", track_visibility='always'),
        'amount_tax': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Taxes',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The tax amount."),
        'amount_total': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The total amount."),
        
        'amount_discount': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Discount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line', 'discount_rate', 'discount_type'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The total amount."), 
    }

    @api.multi
    def compute_discount(self, discount):
        for order in self:
            val1 = val2 = 0.0
            disc_amnt = 0.0
            for line in order.order_line:
                val1 += (line.product_uom_qty * line.price_unit)
                if line.discount > 0.0:
                    line.discount = discount
                val2 += self._amount_line_tax(line)
                disc_amnt += (line.product_uom_qty * line.price_unit * line.discount)/100
            total = val1 + val2 - disc_amnt
            self.currency_id = order.pricelist_id.currency_id
            self.amount_discount = round(disc_amnt)
            self.amount_tax = round(val2)
            self.amount_total = round(total)
    
    def _generate_so_line(self, cr, uid, ids, product_id=None, set_qty=0, discount=0.0, context=None, **kwargs):
        """ Add or set product quantity, add_qty can be negative """
        sol = self.pool.get('sale.order.line')        
        so_obj = self.pool.get('sale.order')        
        quantity = 0
        for so in self.browse(cr, uid, ids, context=context):
            # Create line
            if product_id:
                values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=1, context=context)
                line_id = sol.create(cr, SUPERUSER_ID, values, context=context)
                # compute new quantity
                if set_qty:
                    quantity = set_qty            

                # Remove zero of negative lines
                if quantity <= 0:
                    sol.unlink(cr, SUPERUSER_ID, [line_id], context=context)
                else:
                    # update line
                    values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=quantity, line_id=line_id, context=context)
                    values['product_uom_qty'] = quantity
                    values['discount'] = discount
                    sol.write(cr, SUPERUSER_ID, [line_id], values, context=context)
            if discount > 0.0 and not so.discount_rate:
                so_obj.write(cr, SUPERUSER_ID, [so.id], {'discount_rate':discount, 'discount_type':'percent'}, context=context)                
        return True

