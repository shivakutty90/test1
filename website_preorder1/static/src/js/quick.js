var width = 0;
var currency = $('#products_grid').find('.currency').val();

function update_product_image(event_source, product_id) {
    var $img = event_source.parents('section').prev().find('img');
    if (isNaN(product_id)) product_id = undefined;
    if (product_id != undefined){
        $img.attr("src", "/website/image/product.product/" + product_id + "/image");
    }
}
function update_product_templ_image(event_source, product_id) {
    var $img = event_source.parents('section').prev().find('img');
    if (isNaN(product_id)) product_id = undefined;
    if (product_id != undefined){    
        $img.attr("src", "/website/image/product.template/" + product_id + "/image");
    }
}

function currencyFormatDE (num) {
    return num
       .toFixed(2) // always two decimal digits
       .replace(".", ",") // replace decimal point character with ,
       .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")// use . as a separator
}

$(document).ready(function () {

    // popup image
    // (function largeImg(){
    // })();
    // $(".oe_product_image").mouseover(function(){
    //     $(this).parent().find(".largeImg").css("display","block");
    // });
    // $(".largeImg,.close-btn").click(function(){
    //     $(".largeImg").css("display","none");
    // }); 

    $(window).scroll(function(){
        var winscroll = $(window).scrollTop();
        if(winscroll > 100){
            $(".products_pager").addClass("pos-fixed");
        }else{
            $(".products_pager").removeClass("pos-fixed");
        }    
    });    

    var self = $(this);

    load_total();
    function load_total(call_from) {
        total_qtys = 0;
        total_amount = 0.00;
        $('.js_quantity1').each(function(){
            var variant_id = parseInt($(this).data('varient-id'),10);
            var qty = parseInt($(this).val(), 10);
            var price = parseFloat($(this).data('varient-price'), 10);
            if(isNaN(qty)){

            }else{
                total_qtys += qty;
                total_amount += price*qty;
            }
            
        });
        if (total_qtys !== 0){
              self.find(".confirm_qty_btn").show();
              self.find(".reset_qtys").show();
          }else{
              self.find(".confirm_qty_btn").hide();
              self.find(".reset_qtys").hide();    
          }
      
        $('.tqty').find('span').text(currencyFormatDE(total_qtys)); /*set total quantities to matrix list view*/
        $('.total_amount').find('span').text(currencyFormatDE(total_amount)); /*set total prices to matrix list view*/
        if (call_from == undefined) {
            openerp.jsonRpc("/preorder/shop/get_discount", 'call', {'total_amount':total_amount}).then(function(val){
                    if (val != undefined){
                        $('.discount_percentage').find('span').text(val[0]);
                        $('.amount_after_discount').find('span').text(currencyFormatDE(val[1]));  
                        if (val[1] !== 0)
                        {
                            var discount_html = "<strike style='color:#a94442;'>"+ currencyFormatDE(total_amount) +"</strike>&nbsp;" + currencyFormatDE(total_amount-val[1])
                            $('.total_amount').find('span').html(discount_html);  
                        }  
                    }
                });
            }        
        
        }
    $('.variant_matrix').each(function(){
            $(this).find('.fixedTable-body table tr td input').each(function(){
                calculate_same_row_qtys($(this));
                calculate_same_column_qtys_prices($(this));
                calculate_total_qtys_prices($(this));
            });
        });

    // function calculate_same_row_qtys(self){
    //     var row_index = self.parents('tr').index()+1;
    //     var total_row_quantities = 0;
    //     self.parents('tr').find('td').each(function() {
    //         var qty = parseInt($(this).find('input').val(),10);
    //         if (isNaN(qty)) qty = undefined;
    //         if (qty != undefined){
    //             total_row_quantities += qty;
    //         }            
    //     });
    //     self.parents('section').find('.right_total').find("table tr:nth-child("+row_index+")").find('span').text(currencyFormatDE(total_row_quantities));
    // }

    function calculate_same_row_qtys(self){
        var row_index = self.parents('tr').index()+1;
        var total_row_quantities = 0;
        var total_row_prices = 0.00;
        self.parents('tr').find('td').each(function() {
            var qty = parseInt($(this).find('input').val(),10);
            var price = parseFloat($(this).find('input').data('varient-price'), 10);
            if (isNaN(qty)) qty = undefined;
            if (qty != undefined){
                total_row_quantities += qty;
                total_row_prices += price*qty;
            }
        });
        self.parents('section').find('.right_total').find("table tr:nth-child("+row_index+") td:nth-child(1)").find('span').text(currencyFormatDE(total_row_quantities));
        // self.parents('section').find('.right_total').find("table tr:nth-child("+row_index+") td:nth-child(2)").find('span').html("<p id='r_total222'>"+String(currency + " "+currencyFormatDE(total_row_prices))+"</p>");
        
        self.parents('section').find('.right_total').find("table tr:nth-child("+row_index+") td:nth-child(2)").find('span').text(String(currency + " "+currencyFormatDE(total_row_prices)));
    }


    function calculate_same_column_qtys_prices(self){
        var total_column_quantities = 0;
        var total_column_prices = 0.00;
        var column_index = self.parents('td').index()+1;

        self.parents('tbody').find('tr').each(function() {
            $(this).find("td:nth-child("+column_index+") > input").each(function(){
                var price = parseFloat($(this).data('varient-price'), 10);
                var qty = parseInt($(this).val(), 10);   
                if(isNaN(qty)) qty = 0;
                if (qty != undefined){      
                    total_column_prices += price*qty;
                    total_column_quantities += qty;
                }
            });        
        });
        self.parents('section').find('.bottom_total').find("table tbody tr:nth-child(1) td:nth-child("+column_index+") span").text(currencyFormatDE(total_column_quantities));
        self.parents('section').find('.bottom_total').find("table tbody tr:nth-child(2) td:nth-child("+column_index+") span").text(String(currency + " "+currencyFormatDE(total_column_prices)));
    }

    function calculate_total_qtys_prices(self){
        var total_quantities = 0;
        var total_prices = 0.00;
        // var currency = self.parents('#products_grid').find('.currency').val();
        self.parents('tbody').find('tr td input').each(function() {
                var price = parseFloat($(this).data('varient-price'), 10);
                var qty = parseInt($(this).val(), 10);       
                if(isNaN(qty)) qty = 0;
                if (qty != undefined){      
                    total_prices += price*qty;
                    total_quantities += qty;
                }            
        });     
        self.parents('section').find('.totalRighttbar').find("table tbody tr:nth-child(1) td span strong").text(currencyFormatDE(total_quantities));        
        self.parents('section').find('.totalRighttbar').find("table tbody tr:nth-child(2) td span strong").text(String(currency + " "+currencyFormatDE(total_prices)));   
    }

    // Hide confirm quantities button in matrix list view
    if (!($("#products_grid").find(".variant_matrix").length > 0)){ 
      $(this).find(".matrix_confirm_qty").addClass("hidden");
    }


    $(".js_quantity1").change(function() {
        self.find(".confirm_qty_btn").show();
        self.find(".reset_qtys").show();     
        calculate_same_row_qtys($(this));
        calculate_same_column_qtys_prices($(this));
        calculate_total_qtys_prices($(this));
        load_total('custom_call');     
        var qty = parseInt($(this).val(), 10);
        var variant_id = parseInt($(this).data('varient-id'),10);
        var variant_price = parseFloat($(this).data('varient-price'),10);
        if (isNaN(qty)) qty = 0;
        
        if (qty != undefined){
            openerp.jsonRpc("/preorder/shop/update_user_qty_and_get_discount", 'call', {'product_id':variant_id, 'qty':qty, 'price':variant_price, 'total_amount':total_amount}).then(function(val){
                    $('.discount_percentage').find('span').text(val[0]);
                    $('.amount_after_discount').find('span').text(currencyFormatDE(val[1]));  
                    if (val[1] !== 0)
                    {
                        var discount_html = "<strike style='color:#a94442;'>"+ currencyFormatDE(total_amount) +"</strike>&nbsp;" + currencyFormatDE(total_amount-val[1])
                        $('.total_amount').find('span').html(discount_html);  
                    }  
                });
            }
    });

// Fixed table sidebar and header start
    $('.fixedTable-body').scroll(function () {
        $(this).prev().find('table').css('margin-top', -$($(this)).scrollTop());
        $(this).parents('.fixedTable').find('.fixedTable-header table').css('margin-left', -$($(this)).scrollLeft());

        $(this).next().find('table').css('margin-top', -$($(this)).scrollTop());
        $(this).parents('.fixedTable').find('.bottom_total table').css('margin-left', -$($(this)).scrollLeft());        
    });

// css for matrix table scroll start
    $(".fixedTable").each(function(){
        var fixedTablebodyWidth = $(this).find(".fixedTable-body table").width();
        // var rightWidth = $(this).find(".right_total").width();

        $(this).find(".fixedTable-body").css("width",fixedTablebodyWidth + 20);
        $(this).find(".fixedTable-header").css("width",fixedTablebodyWidth + 20);
        $(this).find(".bottom_total").css("width",fixedTablebodyWidth + 20);

        var fixedTablesidebarWidth = $(this).find('.fixedTable-sidebar').width();
        $(this).find(".first_child").css("width",fixedTablesidebarWidth);        
        $(this).find(".totalSidebar").css("width",fixedTablesidebarWidth);        
        // $(this).find(".totalRighttbar").css("width",rightWidth);        
        // $(this).find(".header_qty_label").css("width",rightWidth);        
    });
// css for matrix table scroll end

	 $(this).on('click', '.preorder-btn', function (ev) {
        ev.preventDefault();        
        var token_id = parseInt($(this).data('token-id'));
        if (token_id){
            $('#preloader_pre_order').show();
            window.location.href = '/preorder/shop/' + token_id; 
            }
        else{
            window.location.href = '/page/preorder';  
            }
    });


    //~ $(this).on('click', '.preorder-btn', function (ev) {
    //~ $('#preloader_pre_order').show();
        //~ ev.preventDefault();        
        //~ var token_id = parseInt($(this).data('token-id'));
        //~ if (token_id){
          //~ // setTimeout(function(){
            //~ window.location.href = '/preorder/shop/' + token_id; 
            //~ $('#preloader_pre_order').hide();                
            //~ // },1000); 
        //~ }
        //~ else{
            //~ window.location.href = '/page/preorder';  
        //~ }
    //~ });


// Preorder price hover start
    $('.oe_website_preorder').each(function () {
        var oe_website_preorder = this;
        $(oe_website_preorder).find(".tiptext").mouseenter(function() {
            var $input = $(this);
            var product_id = $input.data('varient-id');
            
            $(oe_website_preorder).find("."+product_id).show();
        }).mouseleave(function() {
            $(oe_website_preorder).find(".hover_price").hide();
        });    
        });

// Matrix text input focus color start
$(this).each(function(){
    $(".js_quantity1").focusin(function () {
       $(this).css({ 'background': '#a9a9a9' });
       update_product_image($(this), +$(this).data('varient-id')); 
    });
    $(".js_quantity1").focusout(function () {
       $(this).css({ 'background': '' });
       update_product_templ_image($(this), +$(this).data('product-id')); 
    });
});
});

$(window).load(function() {
    // Animate loader off screen
    // $(".preloader").fadeOut("slow");    
        $('#loading').hide();
    
});
