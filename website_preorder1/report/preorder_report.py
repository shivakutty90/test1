# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import tools
from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import models, fields, api, _


class preorder_category_based_report(models.Model):    
    _name = "preorder.category.based.report"
    _description = "Preorder Orders Category Statistics"
    _auto = False
    
    total_amount = fields.Float('Total of pre-order amount')
    quantity = fields.Integer('Total number of pieces')  # TDE FIXME master: rename into nbr_lines
    product_categ_id = fields.Many2one('product.category', 'Name of Category', required=True)
    preorder_id = fields.Many2one('preorder.config', 'Preorder')
        
   
    def init(self, cr):
        """Initialize the sql view for the event registration """
        tools.drop_view_if_exists(cr, 'preorder_category_based_report')

        # TOFIX this request won't select events that have no registration
        cr.execute(""" CREATE VIEW preorder_category_based_report AS (
            select
                ptc.id AS id,
                SUM(puipl.qty) AS quantity, 
                SUM(puipl.subtotal) AS total_amount,                         
                pc.id AS preorder_id,
                ptc.id AS product_categ_id
            from
                preorder_config pc
                left join preorder_user_input pui on (pui.preorder_id = pc.id)                
                left join preorder_user_input_product_line puipl on (puipl.user_input_id = pui.id)
                left join product_product pp on (pp.id = puipl.product_id)
                left join product_template pt on (pt.id = pp.product_tmpl_id)
                left join product_category ptc on (ptc.id = pt.categ_id)
            where
                pp.id is not null and ptc.id is not null
            group by 
                 pc.id, ptc.id
        )
        """)        
        
class preorder_product_detail_report(models.Model):    
    _name = "preorder.product.detail.report"
    _description = "Preorder Orders Product Statistics"
    _auto = False
    
    default_code = fields.Char('Product Code')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    preorder_id = fields.Many2one('preorder.config', 'Preorder')
    product_categ_id = fields.Many2one('product.category', 'Product Category', required=True)
    quantity = fields.Integer('# Quantity of products')    
    
    def init(self, cr):
        tools.drop_view_if_exists(cr, 'preorder_product_detail_report')

        # TOFIX this request won't select events that have no registration
        cr.execute(""" CREATE VIEW preorder_product_detail_report AS (
           select
                pp.id AS id,
                SUM(puipl.qty) AS quantity,
                pp.id AS product_id,
                pc.id AS preorder_id, 
                pp.default_code AS default_code,
                ptc.id AS product_categ_id
            from
                preorder_config pc
                left join preorder_user_input pui on (pui.preorder_id = pc.id)
                --left join preorder_product_rel ppr on (ppr.preorder_id = poc.id)
                left join preorder_user_input_product_line puipl on (puipl.user_input_id = pui.id)
                left join product_product pp on (pp.id = puipl.product_id)
                left join product_template pt on (pt.id = pp.product_tmpl_id)
                left join product_category ptc on (ptc.id = pt.categ_id)
            where
                pp.id is not null
            group by 
                 pp.id, pc.id, ptc.id
        )
        """)    
#~ class preorder_customer_report(models.Model):
    #~ """Preorder Analysis"""
    #~ _name = "preorder.customer.report"
    #~ _description = "Preorder Orders Statistics"
    #~ _auto = False
    #~ 
    #~ 
    #~ name = fields.Many2one('res.partner', readonly=True)
    #~ p_id = fields.Many2one('preorder.config','PreOrder Ref')    
    #~ tot_product = fields.Integer('# of Unique Product')
    #~ tot_piece = fields.Integer('# of Piece')
    #~ 
    #~ 
    #~ def init(self, cr):
        #~ """Initialize the sql view for the event registration """
        #~ tools.drop_view_if_exists(cr, 'preorder_customer_report')
#~ 
        #~ # TOFIX this request won't select events that have no registration
        #~ cr.execute(""" CREATE VIEW preorder_customer_report AS (
            #~ select
                #~ poc.id::varchar || '/' || coalesce(poc.id::varchar,'') AS id,
                #~ poui.preorder_id AS p_id,
                #~ poui.partner_id AS name,
                #~ count(pl.product_id) AS tot_product,
                #~ count(poc.id) AS tot_piece
            #~ from
                #~ preorder_config poc
                #~ left join preorder_user_input poui on (poui.preorder_id = poc.id)
                #~ left join preorder_product_rel ppr on (ppr.preorder_id = poc.id)
                #~ left join preorder_user_input_product_line pl on (pl.user_input_id = poui.id)
            #~ group by 
                 #~ poc.id, poui.preorder_id, poui.partner_id
        #~ )
        #~ """)    
    #~ 
      

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
