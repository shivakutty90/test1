# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields
from openerp import tools

class report_preorder_management(models.Model):
    _name = "report.preorder.management"
    _auto = False

    name = fields.Many2one('preorder.config', "Name")
    start_date = fields.Date('Start Date') 
    end_date = fields.Date('End Date')
    no_preorder_completed =fields.Integer('Number of Preorder Completed')
    no_preorder_not_completed = fields.Integer('Number of Preorder Not yet Completed')
    status = fields.Char('Status')
    
    def init(self, cr):
        """Initialize the sql view for the event registration """
        tools.drop_view_if_exists(cr, 'report_preorder_management')

        # TOFIX this request won't select events that have no registration
        cr.execute(""" CREATE VIEW report_preorder_management AS (
            select
                poc.id::varchar || '/' || coalesce(poc.id::varchar,'') AS id,
                poc.id as name,
                poc.start_date as start_date,
                poc.expire_date as end_date,
                'NA' as status,
                (select COUNT(CASE WHEN poui.state = 'done' then 1 ELSE NULL END)) AS no_preorder_completed,
                (select COUNT(CASE WHEN poui.state in ('draft','confirm') then 1 ELSE NULL END)) AS no_preorder_not_completed
            from
                preorder_config poc
            left join 
                preorder_user_input poui on (poui.preorder_id = poc.id)
            group by 
                poc.id, poc.expire_date, poc.start_date

        )
        """)  
    
