# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import tools
from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import models, fields, api, _


class preorder_sale_report(models.Model):
    """Preorder Analysis"""
    _name = "preorder.sale.report"
    _order = 'preorder_date desc'
    _description = "Preorder Orders Statistics"
    _auto = False
    
    preorder_date = fields.Datetime('Preorder Date', readonly=True)
    preorder_id = fields.Many2one('preorder.config','PreOrder', required=True)
    draft_state = fields.Integer(' # No of Draft Preorder')
    confirm_state = fields.Integer(' # No of Confirmed Preorder')
    nbpreorder = fields.Integer('Number of Preorders')
    nbproducts = fields.Integer('Number of Products')
    #~ product_uom_qty = fields.Float('# of Qty', readonly=True)
    partner_id = fields.Many2one('res.partner', 'Partner', readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    #~ user_id = fields.many2one('res.users', 'Salesperson', readonly=True)
    categ_id = fields.Many2one('product.category','Category of Product', readonly=True)
    state = fields.Selection([
            ('draft', 'Unconfirmed'),
            ('confirm', 'Confirmed'), 
            ('done', 'Done'),            
        ], string='Status', default='draft', readonly=True, required=True, copy=False,
        )
    #~ user_id = fields.Many2one('res.users', 'Salesperson', readonly=True)
    price_total = fields.Float('Total Price', readonly=True)
    #~ discounted_total = fields.Float(' # Discount Price', readonly=True)
    name = fields.Char('Preorder Name', readonly=True)
    #~ pricelist_id = fields.Many2one('product.pricelist', 'Pricelist', readonly=True)
   
    
    
    
    def init(self, cr):
        """Initialize the sql view for the event registration """
        tools.drop_view_if_exists(cr, 'preorder_sale_report')

        # TOFIX this request won't select events that have no registration
        cr.execute(""" CREATE VIEW preorder_sale_report AS (
            SELECT
                p.id::varchar || '/' || coalesce(p.id::varchar,'') AS id,
                p.id AS preorder_id,
                p.name AS name,
                pu.partner_id AS partner_id,
                pu.total_amount AS price_total,
                --pu.discounted_total AS discounted_total,
                pu.created_date AS preorder_date,
                count(p.id) AS nbpreorder,
                pc.category_id AS categ_id,
                ppr.product_id AS product_id,
                count(ppr.product_id) AS nbproducts,
                CASE WHEN pu.state IN ('draft') THEN p.id ELSE 0 END AS draft_state,
                CASE WHEN pu.state IN ('open','done') THEN p.id ELSE 0 END AS confirm_state,
                pu.state AS state
            FROM
                preorder_config p
                LEFT JOIN preorder_user_input pu on (p.id =pu.preorder_id)
                LEFT JOIN preorder_partner_rel pp on (p.id =pp.preorder_id)
                LEFT JOIN preorder_category_rel pc on (p.id =pc.preorder_id)
                LEFT JOIN preorder_product_rel ppr on (p.id =ppr.preorder_id)

            GROUP BY
                pu.partner_id,
                ppr.product_id,
                pc.category_id,
                pu.id,
                state,
                p.id,
                pu.created_date,
                p.id,
                pu.total_amount,
                --pu.discounted_total,
                p.name
        )
        """)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
