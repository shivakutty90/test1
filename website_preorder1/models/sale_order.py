# -*- coding: utf-8 -*-
import random

from openerp import SUPERUSER_ID
from openerp.osv import osv, orm, fields
from openerp.addons.web.http import request
from openerp.tools.translate import _
from openerp import api


class sale_order(osv.Model):
    _inherit = "sale.order"
    
    def force_preorder_send(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        for order_id in ids:            
            ir_model_data = self.pool.get('ir.model.data')
            try:
                template_id = ir_model_data.get_object_reference(cr, uid, 'website_preorder', 'email_template_preorder')[1]
            except ValueError:
                template_id = False
            
            compose_ctx = dict(context,
                                   active_ids=ids)
            compose_id = self.pool['mail.compose.message'].create(
                cr, uid, {
                    'model': self._name,
                    'composition_mode': 'mass_mail',
                    'template_id': template_id,                    
                    'notify': True,
                }, context=compose_ctx)
            values = self.pool['mail.compose.message'].onchange_template_id(
                cr, uid, [compose_id], template_id, 'mass_mail', self._name, False, context=compose_ctx)['value']
            if values.get('attachment_ids'):
                values['attachment_ids'] = [(6, 0, values['attachment_ids'])]
            self.pool['mail.compose.message'].write(
                cr, uid, [compose_id],
                values,
                context=compose_ctx)
            self.pool['mail.compose.message'].send_mail(cr, uid, [compose_id], context=compose_ctx)
        return True
    
    def _generate_so_line(self, cr, uid, ids, product_id=None, set_qty=0, discount=0.0, context=None, **kwargs):
        """ Add or set product quantity, add_qty can be negative """
        sol = self.pool.get('sale.order.line')        
        quantity = 0
        for so in self.browse(cr, uid, ids, context=context):
            # Create line
            if product_id:
                values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=1, context=context)
                line_id = sol.create(cr, SUPERUSER_ID, values, context=context)
                # compute new quantity
                if set_qty:
                    quantity = set_qty            

                # Remove zero of negative lines
                if quantity <= 0:
                    sol.unlink(cr, SUPERUSER_ID, [line_id], context=context)
                else:
                    # update line
                    values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=quantity, line_id=line_id, context=context)
                    values['product_uom_qty'] = quantity
                    values['discount'] = discount
                    sol.write(cr, SUPERUSER_ID, [line_id], values, context=context)
                
        return True

    # @api.multi
    # def compute_discount(self, discount):
    #     for order in self:
    #         val1 = val2 = 0.0
    #         disc_amnt = 0.0
    #         for line in order.order_line:
    #             val1 += (line.product_uom_qty * line.price_unit)
    #             if line.discount > 0.0:
    #                 line.discount = discount
    #             val2 += self._amount_line_tax(line)
    #             disc_amnt += (line.product_uom_qty * line.price_unit * line.discount)/100
    #         total = val1 + val2 - disc_amnt
    #         self.currency_id = order.pricelist_id.currency_id
    #         self.amount_discount = round(disc_amnt)
    #         self.amount_tax = round(val2)
    #         self.amount_total = round(total)
        
class website(orm.Model):
    _inherit = 'website'

    def _generate_line(self, cr, uid, ids, force_create=False, context=None):
        sale_order_obj = self.pool['sale.order']
        
        user_input_obj = self.pool['preorder.user.input']
        user_input_id = request.session.get('user_input_id') or None
        sale_order = None
        user_input_br = user_input_obj.browse(cr, uid, user_input_id)        
        # create so if needed
        if force_create:
            # TODO cache partner_id session
            partner = self.pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id
            ### User Input State Updation
            for w in self.browse(cr, uid, ids):
                values = {
                    'user_id': w.user_id.id,
                    'partner_id': partner.id,
                    'pricelist_id': partner.property_product_pricelist.id,
                    'preorder_id':user_input_br.preorder_id.id,
                    'requested_date':user_input_br.preorder_id.delivery_date,
                    # 'discount_amount':user_input_br.discount_amount
                    #~ 'section_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'website', 'salesteam_website_sales')[1],
                }
                sale_order_id = sale_order_obj.create(cr, SUPERUSER_ID, values, context=context)
                values = sale_order_obj.onchange_partner_id(cr, SUPERUSER_ID, [], partner.id, context=context)['value']
                sale_order_obj.write(cr, SUPERUSER_ID, [sale_order_id], values, context=context)                
                sale_order = sale_order_obj.browse(cr, SUPERUSER_ID, sale_order_id, context=context)        
                sale_order_obj.force_preorder_send(cr, SUPERUSER_ID, [sale_order.id], context=context)
        return sale_order
