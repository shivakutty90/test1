import logging
import time
import datetime

from openerp import tools
from openerp.osv import fields, osv
from openerp.tools import float_is_zero
from openerp.tools.translate import _
from openerp import api, models

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product

_logger = logging.getLogger(__name__)
class product_attribute_line(osv.osv):
    _inherit = 'product.attribute.line'
    _order = "horizon desc"
    # _order = 'sequence'

    _columns = {
        'horizon':  fields.boolean('Horizontal'),
        # 'sequence': fields.integer('Sequence', help="Determine the display order"),
    }

class b2b_product_template(osv.osv):
    _inherit = 'product.template'

    def create(self, cr, uid, vals, context=None):
        ''' Store the initial standard price in order to be able to retrieve the cost of a product template for a given date'''
        attribute_line_ids = vals['attribute_line_ids']
        if attribute_line_ids:
            i = 0
            for x in attribute_line_ids:
                if x[2]['horizon'] == True:
                    i += 1
            if i > 1:
                raise osv.except_osv(_('Warning!'),_('Choose any one Attribute line for Horizontal axis of Variant Matrix'),) # jothimani
                
        product_template_id = super(b2b_product_template, self).create(cr, uid, vals, context=context)
        if not context or "create_product_product" not in context:
            self.create_variant_ids(cr, uid, [product_template_id], context=context)
        self._set_standard_price(cr, uid, product_template_id, vals.get('standard_price', 0.0), context=context)

        # TODO: this is needed to set given values to first variant after creation
        # these fields should be moved to product as lead to confusion
        related_vals = {}
        if vals.get('ean13'):
            related_vals['ean13'] = vals['ean13']
        if vals.get('default_code'):
            related_vals['default_code'] = vals['default_code']
        if related_vals:
            self.write(cr, uid, product_template_id, related_vals, context=context)

        return product_template_id

    def write(self, cr, uid, ids, vals, context=None):
        ''' Store the standard price change in order to be able to retrieve the cost of a product template for a given date'''
        if 'attribute_line_ids' in vals:
            attribute_line_ids = vals['attribute_line_ids']
            if attribute_line_ids:
                i = 0
                for x in attribute_line_ids:
                    if x[2] == False:
                        check = self.pool.get('product.attribute.line').browse(cr, uid, x[1], context=None)
                        if check.horizon == True:
                            i +=1
                    elif 'horizon' in x[2]:
                        if x[2]['horizon'] == True:
                            i += 1
                if i > 1:
                    raise osv.except_osv(_('Warning!'),_('Choose any one Attribute line for Horizontal axis of Variant Matrix'),) # jothimani

        if isinstance(ids, (int, long)):
            ids = [ids]
        if 'uom_po_id' in vals:
            new_uom = self.pool.get('product.uom').browse(cr, uid, vals['uom_po_id'], context=context)
            for product in self.browse(cr, uid, ids, context=context):
                old_uom = product.uom_po_id
                if old_uom.category_id.id != new_uom.category_id.id:
                    raise osv.except_osv(_('Unit of Measure categories Mismatch!'), _("New Unit of Measure '%s' must belong to same Unit of Measure category '%s' as of old Unit of Measure '%s'. If you need to change the unit of measure, you may deactivate this product from the 'Procurements' tab and create a new one.") % (new_uom.name, old_uom.category_id.name, old_uom.name,))
        if 'standard_price' in vals:
            for prod_template_id in ids:
                self._set_standard_price(cr, uid, prod_template_id, vals['standard_price'], context=context)
        res = super(b2b_product_template, self).write(cr, uid, ids, vals, context=context)
        if 'attribute_line_ids' in vals or vals.get('active'):
            self.create_variant_ids(cr, uid, ids, context=context)
        if 'active' in vals and not vals.get('active'):
            ctx = context and context.copy() or {}
            ctx.update(active_test=False)
            product_ids = []
            for product in self.browse(cr, uid, ids, context=ctx):
                product_ids = map(int,product.product_variant_ids)
            self.pool.get("product.product").write(cr, uid, product_ids, {'active': vals.get('active')}, context=ctx)
        return res        