# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import models, fields, api, _

class preorder_config(models.Model):
    _name = 'preorder.config'   
    _description = 'Pre-Order Configuration'         
    #~ _rec_name = 'code'
    
    name = fields.Char('Name')
    code = fields.Char('Code')    
    partner_category_ids = fields.Many2many('res.partner.category','preorder_partner_category_rel','preorder_id','partner_category_id', string='Customer Tags', help='Select customer to show the preorder')
    partner_ids = fields.Many2many('res.partner','preorder_partner_rel','preorder_id','partner_id', string='Customer', help='Select customer to show the preorder')
    logo = fields.Binary(string='Logo')
    start_date = fields.Datetime('Start Date')
    expire_date = fields.Datetime('Expire Date')    
    brand_ids =  fields.Many2many('product.brand', 'preorder_brand_rel','preorder_id','brand_id', string='Brands')
    category_ids = fields.Many2many('product.category', 'preorder_category_rel','preorder_id','category_id', string='Product Categories')
    product_ids = fields.Many2many('product.template', 'preorder_product_rel','preorder_id','product_id', string='Product')
    supplier_expire_date = fields.Datetime('Supplier Expire Date')
    delivery_date = fields.Date('Delivery Date')    
    multi_preorder = fields.Boolean('Multiple Pre-Order')    
    description = fields.Text('Description')
    show_previous_sales = fields.Boolean('Show Previous Sales')    
    responsible_manager = fields.Many2one('res.users','Responsible Manager')
    preorder_discount_rule_line = fields.One2many('preorder.discount.rule', 'preorder_id', string='Discount Rule')
    
    exclusion_product_ids =  fields.Many2many('product.template', 'preorder_exclusion_product_rel','preorder_id','product_id', string='Product Exclusion')
    exclusion_category_ids =  fields.Many2many('product.category', 'preorder_exclusion_category_rel','preorder_id','category_id', string='Product Category Exclusion')
    exclusion_brand_ids =  fields.Many2many('product.brand', 'preorder_exclusion_brand_rel','preorder_id','brand_id', string='Product Brand Exclusion')
    
    
    #~ def onchange_expire_date(self, cr, uid, ids, start_date,
                                #~ expire_date, context=None):
        #~ """Warn if the expire_date is sooner than the start_date"""
        #~ if (start_date and expire_date and expire_date < start_date):
            #~ return {'warning': {
                #~ 'title': _('Expire date is too soon than Start Date!'),
                #~ 'message': _("The date requested by the customer is "
                             #~ "sooner than the Expire date. You may be "
                             #~ "unable to honor the customer's request.")
                #~ }
            #~ }
        #~ return {}
    
    # Discount Rules
class preorder_discount_rule(models.Model):
    _name = 'preorder.discount.rule'   
    _description = 'Pre-Order Discount Rule'
    _order = "rule_amount, id"
    
    preorder_id = fields.Many2one('preorder.config','Preorder Ref')
    rule_name =  fields.Char('Rule Name')
    rule_discount =  fields.Float('Discount(%)')
    rule_amount =  fields.Float('Amount')
    
        
