{
    'name': 'Pre-Order',
    'description': 'Generating Pre-Order',
    'category': 'Sale',
    'version': '1.0',
    'author': 'Cats&Dogs bvba',
    'website' : 'http://www.catsanddogs.com',
    'depends': ['website_sale', 'product_brand', 'sale', 'website', 'sale_order_dates', 'purchase', 'supplier_direct_import'],
    'data': [        
        'views/menu_preorder.xml',
        'views/preorder_config_view.xml',
        'views/horizon_sort.xml',
        'views/template.xml',
        'views/shop_templates.xml',
        #~ 'views/preorder_management_view.xml',
        'views/sale_view.xml',          
        'views/preorder_user_input_view.xml',
        'views/pre_order_email_data.xml',
        'security/ir.model.access.csv',
        'report/report_preorder_management_view.xml',
        #~ 'report/preorder_sale_report_view.xml',
        'report/preorder_product_detail_report.xml',
        'report/product_category_based_report.xml'
    ],
    'js': [ ],
    #~ 'qweb' : ['static/src/xml/back.xml'],
    'installable': True
}
