# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp

class preorder_user_input(models.Model):
    _name = 'preorder.user.input'   
    _description = 'Pre-Order User Input'      
    _order = "preorder_id, id"
    
    #~ @api.one
    #~ @api.depends('preorder_user_input_product_line.subtotal')
    #~ def _compute_amount(self):
        #~ self.amount_total = sum(line.subtotal for line in self.preorder_user_input_product_line)
    
    preorder_id = fields.Many2one('preorder.config','PreOrder Ref')    
    name = fields.Char('Name')     
    created_date = fields.Datetime('Created Date',compute='_compute_start_date')
    partner_id = fields.Many2one('res.partner','Partner')
    preorder_user_input_product_line = fields.One2many('preorder.user.input.product.line', 'user_input_id', string='Line Ref')
    token = fields.Char("Identification token", readonly=True)
    total_amount = fields.Float('Total')
    discount_amount = fields.Float('Discount')
    product_count = fields.Integer('Product Count')
    #~ amount_total = fields.Float(string='Total', digits=dp.get_precision('Account'),
        #~ store=True, readonly=True, compute='_compute_amount')
    state = fields.Selection([
            ('draft', 'Unconfirmed'),
            ('confirm', 'Confirmed'), 
            ('done', 'Done'),            
        ], string='Status', default='draft', readonly=True, required=True, copy=False,
        )
    
    @api.one
    def _compute_start_date(self):
        self.created_date = fields.Datetime.now()
    
class preorder_user_input_product_line(models.Model):
    _name = 'preorder.user.input.product.line'   
    _description = 'Pre-Order User Input Product Line'      
    
    user_input_id = fields.Many2one('preorder.user.input','User Input Ref')
    product_id = fields.Many2one('product.product','Product')
    qty = fields.Float('Qty')
    discount = fields.Float('Discount (%)', readonly=True)
    subtotal = fields.Float('Subtotal')
