# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
import logging
import werkzeug
import werkzeug.utils
from datetime import date, datetime, timedelta
from math import ceil

import itertools
import collections
from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF
from openerp.tools.safe_eval import safe_eval

from openerp.osv import fields
from openerp import http
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
from openerp.addons.web.controllers.main import login_redirect

PPG = 1000 # Products Per Page
PPR = 4  # Products Per Row

_logger = logging.getLogger(__name__)

class table_compute(object):
    def __init__(self):
        self.table = {}

    def _check_place(self, posx, posy, sizex, sizey):
        res = True
        for y in range(sizey):
            for x in range(sizex):
                if posx+x>=PPR:
                    res = False
                    break
                row = self.table.setdefault(posy+y, {})
                if row.setdefault(posx+x) is not None:
                    res = False
                    break
            for x in range(PPR):
                self.table[posy+y].setdefault(x, None)
        return res

    def process(self, products):
        # Compute products positions on the grid
        minpos = 0
        index = 0
        maxy = 0
        for p in products:
            x = min(max(p.website_size_x, 1), PPR)
            y = min(max(p.website_size_y, 1), PPR)
            if index>=PPG:
                x = y = 1

            pos = minpos
            while not self._check_place(pos%PPR, pos/PPR, x, y):
                pos += 1
            # if 21st products (index 20) and the last line is full (PPR products in it), break
            # (pos + 1.0) / PPR is the line where the product would be inserted
            # maxy is the number of existing lines
            # + 1.0 is because pos begins at 0, thus pos 20 is actually the 21st block
            # and to force python to not round the division operation
            if index >= PPG and ((pos + 1.0) / PPR) > maxy:
                break

            if x==1 and y==1:   # simple heuristic for CPU optimization
                minpos = pos/PPR

            for y2 in range(y):
                for x2 in range(x):
                    self.table[(pos/PPR)+y2][(pos%PPR)+x2] = False
            self.table[pos/PPR][pos%PPR] = {
                'product': p, 'x':x, 'y': y,
                'class': " ".join(map(lambda x: x.html_class or '', p.website_style_ids))
            }
            if index<=PPG:
                maxy=max(maxy,y+(pos/PPR))
            index += 1

        # Format table according to HTML needs
        rows = self.table.items()
        rows.sort()
        rows = map(lambda x: x[1], rows)
        for col in range(len(rows)):
            cols = rows[col].items()
            cols.sort()
            x += len(cols)
            rows[col] = [c for c in map(lambda x: x[1], cols) if c != False]

        return rows

        # TODO keep with input type hidden

class QueryURL(object):
    def __init__(self, path='', **args):
        self.path = path
        self.args = args

    def __call__(self, path=None, **kw):
        if not path:
            path = self.path
        for k,v in self.args.items():
            kw.setdefault(k,v)
        l = []
        for k,v in kw.items():
            if v:
                if isinstance(v, list) or isinstance(v, set):
                    l.append(werkzeug.url_encode([(k,i) for i in v]))
                else:
                    l.append(werkzeug.url_encode([(k,v)]))
        if l:
            path += '?' + '&'.join(l)
        return path

def get_pricelist():
    cr, uid, context, pool = request.cr, request.uid, request.context, request.registry    
    partner = pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id
    pricelist = partner.property_product_pricelist
    return pricelist

class website_preorder(http.Controller):
    
    def get_pricelist(self):
        return get_pricelist()

    @http.route(['/page/preorder', '/page/preorder/<model("preorder.config"):preorder>'], type='http', auth='user', website=True)
    def preorder(self, preorder=None, token=None):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        request.session['user_input_id'] = None 
        user_input_obj = http.request.env['preorder.user.input']
        domain = []
        tags = []
        product_domain = []
        user_input_ids = []
        keep = QueryURL('/page/preorder', preorder=preorder and int(preorder))
        if preorder:
            domain = [('id','=',int(preorder))]
            preorder = pool['preorder.config'].browse(cr, SUPERUSER_ID, int(preorder), context=context)
            url = "/page/preorder/%s" % slug(preorder)
        # Date Based Pre-Orders
        d_now_str = datetime.now().strftime(DTF)
        domain += [('start_date', '<=', d_now_str), ('expire_date', '>=', d_now_str)]
        # Date Based Pre-Orders End
        user_id = pool['res.users'].search(cr, SUPERUSER_ID, [('id','=',uid)], context=context)
        if user_id:
            partner = pool['res.users'].browse(cr, SUPERUSER_ID, user_id[0], context=context).partner_id
            if partner.category_id:
                tags = [c.id for c in partner.category_id]                
            if not tags and partner.parent_id:
                tags = [c.id for c in partner.parent_id.category_id]
            domain += ['|',('partner_ids', '=', partner.id), ('partner_category_ids', 'in', tags)]
            preorder_config_obj = http.request.env['preorder.config'].search(domain)
            for p in preorder_config_obj:
                if isinstance(p, int):
                    preorder_br = pool['preorder.config'].browse(cr, SUPERUSER_ID, p, context=context)
                else:
                    preorder_br = p                    
                    
                product_domain += self._get_search_domain(cr, uid, preorder_br)   
                product_obj = pool.get('product.template')
                product_count = product_obj.search_count(cr, uid, product_domain, context=context)                
                    
                user_input_sr = user_input_obj.search([('preorder_id', '=', preorder_br.id),
                                                         ('partner_id', '=', partner.id)])
                if not user_input_sr:
                    user_input_id = pool['preorder.user.input'].create(cr, SUPERUSER_ID, {
                                                            'preorder_id':preorder_br.id,
                                                            'name':preorder_br.name,
                                                            'partner_id':partner.id,
                                                            'state':'draft',
                                                            'product_count':product_count})
                else:
                    if preorder_br.multi_preorder:
                        ds_user_input_sr = user_input_obj.search([('preorder_id', '=', preorder_br.id),
                                                             ('partner_id', '=', partner.id),('state', '=', 'done')])
                        if len(user_input_sr) == len(ds_user_input_sr):
                            user_input_id = pool['preorder.user.input'].create(cr, SUPERUSER_ID, {
                                                                'preorder_id':preorder_br.id,
                                                                'name':preorder_br.name,
                                                                'partner_id':partner.id,
                                                                'state':'draft',
                                                                'product_count':product_count})
                            user_input_ids.append(user_input_id)
                            u_ids = [u_id.id for u_id in user_input_sr]
                            user_input_ids.extend(u_ids)
                            continue
                        else:
                            u_ids = [u_id.id for u_id in user_input_sr]
                            user_input_ids.extend(u_ids)
                            continue
                            #~ user_input_id = user_input_sr.id
                    else:
                        user_input_id = user_input_sr.id
                user_input_ids.append(user_input_id)
        filter_preorders = http.request.env['preorder.config'].search([])
        user_input = user_input_obj.search([('id', 'in', user_input_ids)])
        return http.request.render('website_preorder.preorder_init', {
                                            'keep': keep,
                                            'preorder': preorder,
                                            'filter_preorders': filter_preorders,
                                            'token':user_input})

    def get_attribute_value_ids_matrix(self, product):
        attribute_value_ids = []
        visible_attrs = set(l.attribute_id.id for l in product.attribute_line_ids if len(l.value_ids) >= 1)

        for p in product.product_variant_ids:
            attribute_value_ids.append([p.id, [v.id for v in p.attribute_value_ids if v.attribute_id.id in visible_attrs]])
        return attribute_value_ids

    def _get_search_domain(self, cr, uid, preorder_br):
        domain = []
        preorder_products = []
        if preorder_br.brand_ids and preorder_br.category_ids:
            category_ids = [c.id for c in preorder_br.category_ids]
            filter_category_ids = self._find_categories(cr, uid, category_ids)
            brand_ids = [b.id for b in preorder_br.brand_ids]
            domain += ['|','&',('product_brand_id', 'in', brand_ids),('categ_id', 'child_of', filter_category_ids)]
        else:
            if preorder_br.brand_ids:
                brand_ids = [b.id for b in preorder_br.brand_ids]
                domain += ['|',('product_brand_id', 'in', brand_ids)]
            else:
                if preorder_br.category_ids:
                    category_ids = [c.id for c in preorder_br.category_ids]
                    filter_category_ids = self._find_categories(cr, uid, category_ids)
                    domain += ['|',('categ_id', 'child_of', filter_category_ids)]
        if preorder_br.product_ids:
            preorder_products += [p.id for p in preorder_br.product_ids]
        domain += [('id', 'in', preorder_products)]
        return domain
    
    def _find_categories(self, cr, uid, category_ids):
        del_list = []
        if category_ids:
            for p in range(0, len(category_ids) -1):
                child_of_ids = request.registry['product.category'].search(cr, uid, [('id','child_of',[category_ids[p]])])
                if child_of_ids:
                    del child_of_ids[0]
                    found_child_of = lambda x, y: any(i in y for i in x)
                    if found_child_of(category_ids, child_of_ids):
                        del_list.append(category_ids[p])
                        continue
            return list(filter(lambda a: a not in del_list, category_ids))

    @http.route([
        '/preorder/shop/<model("preorder.user.input"):user_input>',
        '/preorder/shop/<model("preorder.user.input"):user_input>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, user_input=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry        
        # attrib_list = request.httprequest.args.getlist('attrib')
        # attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        # attrib_set = set([v[1] for v in attrib_values])
        user_input_obj = pool['preorder.user.input']
        domain = []
        view_state = False
        if user_input:
            request.session['user_input_id'] = int(user_input)
            request.session['preorder_id'] = user_input.preorder_id.id
            user_input_br = user_input_obj.browse(cr, uid, int(user_input.id), context=context)
            domain += self._get_search_domain(cr, uid, user_input_br.preorder_id)
            if user_input_br.state == 'done':
                view_state = True
        user_id = pool['res.users'].browse(cr, uid, uid, context=None)       
        
        if not context.get('pricelist'):
            pricelist = self.get_pricelist()
            context['pricelist'] = int(pricelist)
        else:
            pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)

        product_obj = pool.get('product.template')

        url = "/preorder/shop"
        product_count = product_obj.search_count(cr, uid, domain, context=context)
        if search:
            post["search"] = search
        if user_input:
            user_input = pool['preorder.user.input'].browse(cr, uid, int(user_input), context=context)
            url = "/preorder/shop/%s" % slug(user_input)
        # if attrib_list:
        #     post['attrib'] = attrib_list
        pager = request.website.pager(url=url, total=product_count, page=page, step=PPG, scope=7, url_args=post)
        product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        products = product_obj.browse(cr, SUPERUSER_ID, product_ids, context=context)
        for_temp_product_ids = product_obj.search(cr, uid, domain, order='website_published desc, website_sequence desc', context=context)
        if for_temp_product_ids:
            preorder_user_input_obj = http.request.env['preorder.user.input']
            user_input_br = preorder_user_input_obj.browse(int(user_input))
            lines = []
            if user_input_br and user_input_br.state not in ['confirm', 'done']:
                for product_id in for_temp_product_ids:
                    product_br = pool['product.template'].browse(cr, uid, product_id)
                    lines.extend([(0,0,{'product_id':p.id,'qty':0}) for p in product_br.product_variant_ids])
                user_input_br.write({
                                    'state':'confirm',
                                    'preorder_user_input_product_line':lines})

        else:
            return request.redirect('/page/preorder')
        # keep = QueryURL('/preorder/shop', user_input=user_input and int(user_input), search=search, attrib=attrib_list)
        keep = QueryURL('/preorder/shop', user_input=user_input and int(user_input), search=search)
        
        # style_obj = pool['product.style']
        # style_ids = style_obj.search(cr, uid, [], context=context)
        # styles = style_obj.browse(cr, uid, style_ids, context=context)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        user_input_ids = user_input_obj.search(cr, uid, [], context=context)
        user_inputs = user_input_obj.browse(cr, uid, user_input_ids, context=context)

        # attributes_obj = request.registry['product.attribute']
        # attributes_ids = attributes_obj.search(cr, uid, [], context=context)
        # attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)

        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)

        currency = user_id.partner_id.property_product_pricelist.currency_id.symbol
        thead = []
        trows = []
        non_variant_details = []
        categoried_products = {}
        for product in products:
            if product.public_categ_ids:
                if categoried_products.get(product.public_categ_ids[0].name):
                    categoried_products[str(product.public_categ_ids[0].name)].append(product)
                else:
                    categoried_products[str(product.public_categ_ids[0].name)] = [product]
            else:
                if categoried_products.get('Unknown'):
                    categoried_products['Unknown'].append(product)
                else:
                    categoried_products['Unknown'] = [product]

            if not product.attribute_line_ids:
                price, product_uom, prefill_qty_available, view_state, enduser_price, c_uom_price, c_uom_name = self.get_product_details(int(product.product_variant_ids[0].id))
                non_variant_details.append([product.id, price, product_uom, prefill_qty_available, view_state, enduser_price])

            if product.attribute_line_ids and product.sale_ok:
                result = self.get_matrix_row(product)
                thead.append(result[0])
                for x in result[1]:
                    trows.append(x)        
        values = {
            'non_variant_details': non_variant_details,
            'view_state':view_state,
            'currency': currency,
            'thead': thead,
            'trows': trows,
            'search': search,
            'user_input': user_input,
            # 'attrib_values': attrib_values,
            # 'attrib_set': attrib_set,
            'pager': pager,
            # 'pricelist': pricelist,
            'products': products,
            'categoried_products': collections.OrderedDict(sorted(categoried_products.items())),
            'bins': table_compute().process(products),
            'rows': PPR,
            # 'styles': styles,
            'user_inputs': user_inputs,
            # 'attributes': attributes,
            'categories': categs,
            'compute_currency': compute_currency,
            'keep': keep,
            # 'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            # 'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib',i) for i in attribs]),
        }
        return request.website.render("website_preorder.products", values)

    @http.route(['/preorder/shop/get_matrix_row'], type='json', auth="public", methods=['POST'], website=True)
    def get_matrix_row(self, product):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        if type(product) == int:
            product = pool['product.template'].browse(cr, uid, product, context=None)
        temp_attribute_value_ids = []

        attribute_value_ids = self.get_attribute_value_ids_matrix(product)

        thlength = len(product.attribute_line_ids[0].value_ids)

        th = [str(x.name) for x in product.attribute_line_ids[0].value_ids]
        th.insert(0, str(product.attribute_line_ids[0].attribute_id.name))    # add attribute name
        th.insert(0, product.id)                                              # add product id in index 0

        product_attribute_line_ids = product.attribute_line_ids
        for x in product_attribute_line_ids:
            temp_attribute_value_ids.append([y.id for y in x.value_ids])
        tmp_compinations_list_of_tuple = list(itertools.product(*temp_attribute_value_ids))
        product_original_compinations = map(list, tmp_compinations_list_of_tuple)
        product_temp_compinations = []
        
        for x in product_original_compinations:
            product_temp_compinations.append([[attribute_value_ids[i][0],x] for i in range(0, len(attribute_value_ids)) if set(x) == set(attribute_value_ids[i][1])])
        for i in range(0, len(product_temp_compinations)):
            if not product_temp_compinations[i]:
                product_temp_compinations[i] = False
        product_compinations = [sub[0] if sub else sub for sub in product_temp_compinations]

        trlength = len(product_compinations)/thlength

        product_compinations = list(zip(*[product_compinations[i:i+trlength] for i in range(0, len(product_compinations), trlength)]))
        rows_list = map(list, product_compinations)
        trow = []
        for row in rows_list:
            for i in xrange(0, len(row)):
                if 0 <= i:
                    if row[i]:
                        att_value_ids_for_name = row[i][1][1:]
                        break
            attribute_value = pool['product.attribute.value'].browse(cr,uid,att_value_ids_for_name,context=None)
            attribute_value_names = [str(x.name) for x in attribute_value]
            y_axis_compinations = ""
            for i in xrange(0,len(attribute_value_names)):
                if i == 0:
                    y_axis_compinations += str(attribute_value_names[i])
                else:
                    y_axis_compinations += '/'+str(attribute_value_names[i])
            for i in xrange(0, len(row)):
                if row[i] != False:
                    del row[i][1:]
            tr = [sub[0] if sub else sub for sub in row]
            tr_withprices = []
            for x in tr:
                if x == False:
                    tr_withprices.append([])
                else:
                    price, product_uom, prefill_qty_available, view_state, enduser_price, c_uom_price, c_uom_name = self.get_product_details(int(x))
                    last_year_qty, show_previous_sales = self.previous_year_qty(int(x))
                    tr_withprices.append([x, price, product_uom, prefill_qty_available, view_state, last_year_qty, show_previous_sales, enduser_price, c_uom_price, c_uom_name])
            tr_withprices.insert(0, y_axis_compinations)
            tr_withprices.insert(0,int(product.id))
            trow.append(tr_withprices)
        return th, trow

    @http.route(['/preorder/shop/get_product_details'], type='json', auth="public", website=True)
    def get_product_details(self, product_id, use_order_pricelist=False, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        user_input_obj = pool['preorder.user.input']
        user_input_line_obj = pool['preorder.user.input.product.line']
        product_uom_obj = pool['product.uom']
        product = pool['product.product'].browse(cr, SUPERUSER_ID, product_id, context=context)
        product_uom = product.uom_id
        partner = pool['res.users'].browse(cr, uid, uid, context=context).partner_id
        user_input_id = request.session.get('user_input_id')
        prefilled_qty= 0
        view_state = False
        c_price = None
        c_uom_name= None
        add_qty = 0
        if use_order_pricelist:
            pricelist_id = request.session.get('sale_order_code_pricelist_id') or partner.property_product_pricelist.id
        else:
            pricelist_id = partner.property_product_pricelist.id
            # TO-DO simplyfy below lines products to-> product
        # prices = pool['product.pricelist'].price_rule_get_multi(cr, uid, [], [(product, add_qty, partner)], context=context)
        # print prices[product_id][pricelist_id][0], "<-- Previous price calculation"

        product_uom_sr = product_uom_obj.search(cr, SUPERUSER_ID, [('category_id','=', product_uom.category_id.id),('uom_type','=', 'reference')])
        if product_uom_sr:
            product_uom_br = product_uom_obj.browse(cr, SUPERUSER_ID, product_uom_sr[0])
            if product_uom.uom_type != 'reference':
                if product_uom.uom_type == 'bigger':
                    c_qty = add_qty / product_uom.factor
                else:
                    c_qty = add_qty * product_uom.factor

                c_ctx = dict(
                    context,
                    uom=product_uom_br.id
                )
                c_price = pool['product.pricelist'].price_get(cr, SUPERUSER_ID, [pricelist_id],
                    product.id, c_qty or 1.0, partner.id, c_ctx)[pricelist_id]
                c_uom_name = product_uom_br.name
            else:
                c_price = None
                c_uom_name = None

        ctx = dict(
            context,
            uom=product_uom.id
        )
        price = pool['product.pricelist'].price_get(cr, SUPERUSER_ID, [pricelist_id],
                product.id, add_qty or 1.0, partner.id, ctx)[pricelist_id]

        if user_input_id:
            if user_input_obj.browse(cr, uid, user_input_id).state == 'done':
                view_state = True
            input_line_sr = user_input_line_obj.search(cr, uid, [('user_input_id', '=', user_input_id), ('product_id', '=', product.id)])
            if input_line_sr:
                prefilled_qty = user_input_line_obj.browse(cr, uid, input_line_sr[0]).qty
        return (round(price, 2), product_uom.name, int(prefilled_qty), view_state, product.enduser_price or 0.0, c_price, c_uom_name)
        # if product.virtual_available > 0:
        #     return (price, True, int(prefilled_qty), view_state)
        # else:
        #     return (price, False, int(prefilled_qty), view_state)

    # @http.route(['/preorder/shop/reset_qty'], type='http', auth="public", website=True)
    # def reset_qty(self, **kw):
    #     cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
    #     user_input_id = request.session.get('user_input_id')
    #     user_input_obj = pool['preorder.user.input']
    #     user_input_line_obj = pool['preorder.user.input.product.line']        
    #     user_input_br = user_input_obj.browse(cr, uid, user_input_id)
    #     if user_input_br.preorder_user_input_product_line:
    #         for line in user_input_br.preorder_user_input_product_line:
    #             user_input_line_obj.write(cr, uid, line.id, {'qty':0, 'subtotal':0.0})
    #     user_input_obj.write(cr, uid, [user_input_br.id], {'total_amount': 0.0})
    #     return request.redirect('/preorder/shop/%s' % user_input_id)

    @http.route(['/preorder/shop/confirm_qty'], type='http', auth="public", methods=['POST'], website=True)
    def cart_confirm_qty(self, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        order = request.website._generate_line(force_create=1)
        user_input_id = request.session.get('user_input_id')
        user_input_obj = pool['preorder.user.input']
        user_input_line_obj = pool['preorder.user.input.product.line']
        #~ partner = pool['res.users'].browse(cr, uid, uid, context=context).partner_id        

        user_input_br = user_input_obj.browse(cr, uid, user_input_id)
        discounted_percentage, discounted_total, exclusion_product_ids = self.get_discount(user_input_br.total_amount)   
        if user_input_br.preorder_user_input_product_line:
            count = 0
            for line in user_input_br.preorder_user_input_product_line:
                if line.qty > 0:
                    discount = float(discounted_percentage[:-1]) or 0.0
                    if discount > 0 and exclusion_product_ids and line.product_id.id not in exclusion_product_ids:
                        user_input_line_obj.write(cr, uid, line.id, {'discount':discount})
                    else:
                        user_input_line_obj.write(cr, uid, line.id, {'discount':0.0})
                    order._generate_so_line(product_id=line.product_id.id, set_qty=line.qty, discount=line.discount)
                else:
                    count += 1
            if count == len(user_input_br.preorder_user_input_product_line):
                pass # For Warning
        user_input_obj.write(cr, uid, user_input_br.id, {'state':'done'})
        return request.redirect('/preorder/shop/confirmation')

    @http.route(['/preorder/shop/confirmation'], type='http', auth="public", website=True)
    def preorder_confirmation(self, **post):
        print " Function call --> preorder_confirmation"
        return request.website.render("website_preorder.preorder_confirmation")    
    
    @http.route(['/preorder/shop/get_discount'], type='json', auth="public", methods=['POST'], website=True)
    def get_discount(self, total_amount, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry       
        discounted_total = 0.0       
        discounted_percentage = '0.0 %'
        aft_exclusion_total = float(total_amount)
        exclusion_products = []  
        domain = []
        product_product_ids = []
        user_input_obj = pool['preorder.user.input']
        discount_rule_obj = pool['preorder.discount.rule']
        product_obj = pool.get('product.template')
        user_input_id = request.session.get('user_input_id')        
        if user_input_id:
            user_input_br = user_input_obj.browse(cr, uid, int(user_input_id))            
            if user_input_br.preorder_id:
                ############# Discount Exclusion - Start #############
                if user_input_br.preorder_id.exclusion_brand_ids and user_input_br.preorder_id.exclusion_category_ids:
                    category_ids = [c.id for c in user_input_br.preorder_id.exclusion_category_ids]
                    filter_category_ids = self._find_categories(cr, uid, category_ids)
                    brand_ids = [b.id for b in user_input_br.preorder_id.exclusion_brand_ids]
                    domain += ['|','&',('product_brand_id', 'in', brand_ids),('categ_id', 'child_of', filter_category_ids)]
                else:
                    if user_input_br.preorder_id.exclusion_brand_ids:
                        brand_ids = [b.id for b in user_input_br.preorder_id.exclusion_brand_ids]
                        domain += ['|',('product_brand_id', 'in', brand_ids)]
                    else:
                        if user_input_br.preorder_id.exclusion_category_ids:
                            category_ids = [c.id for c in user_input_br.preorder_id.exclusion_category_ids]
                            filter_category_ids = self._find_categories(cr, uid, category_ids)
                            domain += ['|',('categ_id', 'child_of', filter_category_ids)]
                if user_input_br.preorder_id.exclusion_product_ids:
                    exclusion_products += [p.id for p in user_input_br.preorder_id.exclusion_product_ids]
                domain += [('id', 'in', exclusion_products)]
                product_ids = product_obj.search(cr, uid, domain, order='website_published desc, website_sequence desc', context=context)
                if product_ids:
                    product_product_ids = pool.get('product.product').search(cr, uid, [('product_tmpl_id', 'in', product_ids)])
                ############# Discount Exclusion - End #############
                
                if user_input_br.preorder_user_input_product_line:
                    for line in user_input_br.preorder_user_input_product_line:
                        if line.product_id.id in product_product_ids:
                            aft_exclusion_total -= line.subtotal
                if aft_exclusion_total > 0.0:
                    discount_rule_sr = discount_rule_obj.search(cr, uid, [('preorder_id','=',user_input_br.preorder_id.id)], order='rule_amount desc')                   
                    if discount_rule_sr:
                        for r in discount_rule_sr:
                            discount_rule_br = discount_rule_obj.browse(cr, uid, r)
                            if total_amount > discount_rule_br.rule_amount:
                                discounted_percentage = str(float(discount_rule_br.rule_discount)*100)+'%'
                                discounted_total = aft_exclusion_total * float(discount_rule_br.rule_discount)
                                break
        return [discounted_percentage, discounted_total, product_product_ids]

    @http.route(['/preorder/shop/update_user_qty_and_get_discount'], type='json', auth="public", methods=['POST'], website=True)
    def update_user_qty_and_get_discount(self, product_id, qty, price, total_amount, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        user_input_id = request.session.get('user_input_id')
        user_input_obj = pool['preorder.user.input']
        user_input_line_obj = pool['preorder.user.input.product.line']
        discounted_total = 0.0
        discounted_percentage = '0.0 %'
        discount = 0.0
        partner = pool['res.users'].browse(cr, uid, uid, context=context).partner_id
        if user_input_id:
            user_input_sr = user_input_obj.search(cr, uid, [('id', '=', user_input_id), ('partner_id', '=', partner.id)])
            if user_input_sr:                         
                input_line_sr = user_input_line_obj.search(cr, uid, [('user_input_id', '=', user_input_sr[0]), ('product_id', '=', product_id)])
                if input_line_sr:
                    user_input_line_obj.write(cr, uid, input_line_sr, {'qty':qty, 'subtotal':qty * float(price), 'discount':discount})
            discounted_percentage, discounted_total, exclusion_product_ids = self.get_discount(total_amount)               
            user_input_obj.write(cr, uid, user_input_sr, {'total_amount' :total_amount, 'discount_amount':discounted_total})
        return [discounted_percentage, discounted_total]

    def sale_and_return_qty(self, cr, uid, product_id, partner_id, start, end):
        sale_total = 0
        return_total = 0
        cr.execute("SELECT sum(m.product_uom_qty) "
                   "FROM stock_move m "
                   "WHERE state IN ('done') "
                   "AND ((SELECT partner_id FROM stock_picking WHERE id=m.picking_id) "
                   "NOT IN (SELECT DISTINCT p.id FROM res_partner p JOIN ir_property i "
                   "ON 'res.partner,' || p.id = i.res_id WHERE i.value_reference = 'account.account,93') "
                   "OR partner_id NOT IN (SELECT DISTINCT p.id FROM res_partner p JOIN ir_property i "
                   "ON 'res.partner,' || p.id = i.res_id WHERE i.value_reference = 'account.account,93')) "
                   "AND date >= %s "
                   "AND date <= %s "
                   "AND product_id = %s "
                   "AND partner_id = %s "
                   "AND location_id = 12 "
                   "AND location_dest_id = 9;", (start, end, product_id, partner_id))
        sales_result = cr.fetchone()
        if all(sales_result):
            sale_total = sales_result[0]

        cr.execute("SELECT sum(m.product_uom_qty) "
                   "FROM stock_move m "
                   "WHERE state IN ('done') "
                   "AND ((SELECT partner_id FROM stock_picking WHERE id=m.picking_id) "
                   "NOT IN (SELECT DISTINCT p.id FROM res_partner p JOIN ir_property i "
                   "ON 'res.partner,' || p.id = i.res_id WHERE i.value_reference = 'account.account,93') "
                   "OR partner_id NOT IN (SELECT DISTINCT p.id FROM res_partner p JOIN ir_property i "
                   "ON 'res.partner,' || p.id = i.res_id WHERE i.value_reference = 'account.account,93')) "
                   "AND date >= %s "
                   "AND date <= %s "
                   "AND product_id = %s "
                   "AND partner_id = %s "
                   "AND location_id = 9 "
                   "AND location_dest_id = 12;", (start, end, product_id, partner_id))
        return_result = cr.fetchone()
        if all(return_result):
            return_total = return_result[0]
        return (sale_total-return_total)

    @http.route(['/preorder/shop/previous_year_qty'], type='json', auth="public", methods=['POST'], website=True)
    def previous_year_qty(self, product_id, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        user_input_id = request.session.get('user_input_id')
        user_input_br = pool['preorder.user.input'].browse(cr, uid, user_input_id)
        last_year = date.today().year - 1
        start_year = datetime(last_year, 1, 1, 0, 0, 0, 0).strftime("%Y-%m-%d %H:%M:%S")
        end_year = datetime(last_year, 12, 31, 0, 0, 0, 0).strftime("%Y-%m-%d %H:%M:%S")
        partner = pool['res.users'].browse(cr, uid, uid, context=context).partner_id
        return (self.sale_and_return_qty(cr, uid, product_id, partner.id, start_year, end_year), user_input_br.preorder_id.show_previous_sales)
