function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    var myVar = setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);
        
        if (--timer < 0) {
            clearInterval(myVar);
            var page_count = $(".page_count").text();
            var current_page = $(".current_page").text();
            if(page_count == current_page)
                {
                $("#submit_survey").click();
                }
            else
                {
                    $("#next_page").click();
                }
        }
            
    }, 1000);
}

$(document).ready(function(){
       var timing =  $('#set_timer').text();
       var timeArray = timing.split(" ");
       timeArray = timeArray[0].split(":");
       display = $('#time');
       //~ alert(display);
       set_minute = parseInt(timeArray[0])*60+parseInt(timeArray[1]);
       startTimer(set_minute, display);
    });

