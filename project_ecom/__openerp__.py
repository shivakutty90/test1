# -*- coding: utf-8 -*-

{
    'name': "Project Ecom",
    'author' : 'Shiva & Ayan',
    'website' : 'http://www.odoo.com',
    'license' : 'AGPL-3',
    'category': 'web',
    'summary': "Signup Modification",    
    'version': '1.0',
    'description': """
        """,
    'depends': ['website','website_sale','payment','point_of_sale'],
    'data': [
        #~ 'security/event_security.xml',
        'views/point_of_sale_view.xml',
        'views/partner_data.xml',
        'ecom_report.xml',
        'views/product_view.xml',
        'views/wallet.xml',
        'views/payment_acquirer.xml',
        'data/wallet.xml',
        'report/ecom_report_point.xml',  
        'views/project_ecom_view.xml',
        #~ 'product_view.xml',    
        'views/partner_view.xml',
        'views/res_users_view.xml',
        'views/sale_view.xml',
    ],
    'qweb': ['static/src/xml/pos.xml'],
    'installable': True,
    'auto_install': True
}
