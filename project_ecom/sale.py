# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.openerp.com>
#
#    This program is free software: you can redistribute it and / or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _

class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    
    #~ def write(self, cr, uid, ids, values, context=None):
        #~ res = super(sale_order_line, self).write(cr, uid, ids, values, context=context)
        #~ line = self.browse(cr, uid, ids, context=context)
        #~ if line.product_id:
            #~ if line.product_id.wallet:
                #~ partner = self.pool.get('res.partner').browse(cr, uid, line.order_id.partner_id.id)
                #~ print partner.wallet_credit_amt, "----------------", line.price_subtotal
                #~ total = partner.wallet_credit_amt + line.price_subtotal
                #~ self.pool.get('res.partner').write(cr, uid, partner.id, {'wallet_credit_amt':total})
        #~ return True


