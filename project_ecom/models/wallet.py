# -*- coding: utf-'8' "-*-"
from hashlib import sha1
import logging
import urlparse

from openerp.addons.payment.models.payment_acquirer import ValidationError
from openerp.addons.project_ecom.controllers.main import WalletController
from openerp.osv import osv, fields
from openerp.tools.float_utils import float_compare

_logger = logging.getLogger(__name__)


class AcquirerWallet(osv.Model):
    _inherit = 'payment.acquirer'
    
    def _get_providers(self, cr, uid, context=None):
        providers = super(AcquirerWallet, self)._get_providers(cr, uid, context=context)
        providers.append(['wallet', 'Wallet'])
        return providers
    
    def _format_wallet_data(self, cr, uid, context=None):
        
        post_msg = '''<div>
<h3>Please use the following transfer details</h3>
<h4></h4>

<h4>Communication</h4>
<p>Please use the order name as communication reference.</p>
</div>'''
        return post_msg

    def create(self, cr, uid, values, context=None):
        if values.get('provider') == 'wallet' and not values.get('post_msg'):
            values['post_msg'] = self._format_wallet_data(cr, uid, context=context)
        return super(AcquirerWallet, self).create(cr, uid, values, context=context)

    def wallet_get_form_action_url(self, cr, uid, id, context=None):
        return '/payment/wallet/feedback'

class TxWallet(osv.Model):
    _inherit = 'payment.transaction'

    def _wallet_form_get_tx_from_data(self, cr, uid, data, context=None):
        reference, amount, currency_name = data.get('reference'), data.get('amount'), data.get('currency_name')
        tx_ids = self.search(
            cr, uid, [
                ('reference', '=', reference),
            ], context=context)

        if not tx_ids or len(tx_ids) > 1:
            error_msg = 'received data for reference %s' % (pprint.pformat(reference))
            if not tx_ids:
                error_msg += '; no order found'
            else:
                error_msg += '; multiple order found'
            _logger.error(error_msg)
            raise ValidationError(error_msg)

        return self.browse(cr, uid, tx_ids[0], context=context)

    def _wallet_form_get_invalid_parameters(self, cr, uid, tx, data, context=None):
        invalid_parameters = []

        if float_compare(float(data.get('amount', '0.0')), tx.amount, 2) != 0:
            invalid_parameters.append(('amount', data.get('amount'), '%.2f' % tx.amount))
        if data.get('currency') != tx.currency_id.name:
            invalid_parameters.append(('currency', data.get('currency'), tx.currency_id.name))

        return invalid_parameters

    def _wallet_form_validate(self, cr, uid, tx, data, context=None):
        _logger.info('Validated wallet payment for tx %s: set as pending' % (tx.reference))
        return tx.write({'state': 'pending'})
