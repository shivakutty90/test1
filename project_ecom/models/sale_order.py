# -*- coding: utf-8 -*-
import random

from openerp import SUPERUSER_ID
from openerp.osv import osv, orm, fields
from openerp.addons.web.http import request
from openerp.tools.translate import _
from datetime import datetime, timedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

class sale_order(osv.Model):
    _inherit = "sale.order"
    
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
                'amount_ecom_tax': 0.0,
            }
            val = val1 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val1 += line.price_subtotal
                val += self._amount_line_tax(cr, uid, line, context=context)
            res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
            res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
            print res[order.id]['amount_untaxed'], 'amount untaxesd'
            print order.amount_ecom_tax, 'ecom tax amount'
            print res[order.id]['amount_ecom_tax'], 'ecom tax 2222222222222'
            res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + order.amount_ecom_tax
        return res

    _columns = {
        'amount_ecom_tax': fields.float(string='Ecom Tax Amount'),
        'sale_total_amount': fields.float(string='Total'),
        'wallet_amount': fields.float(string='Wallet Amount'),
    }

class website(orm.Model):
    _inherit = 'website'
    
    def modify_wallet_amount(self, cr, uid, ids, acquirer_id, context=None):
        sale_obj = self.pool.get('sale.order')
        so_id = request.session.get('sale_last_order_id')
        if so_id:
            order = sale_obj.browse(cr, SUPERUSER_ID, so_id, context=context)
            if order.order_line and acquirer_id:
                acquirer_obj = self.pool.get('payment.acquirer')          
                acquire = acquirer_obj.browse(cr, uid, acquirer_id, context=context)
                if acquire.provider == 'wallet':
                    for line in order.order_line:
                        if line.product_id:
                            if line.product_id.wallet == True:
                                raise osv.except_osv(_('Error!'), _('You can not pay amount through wallet for this product!'))
                else:
                    for line in order.order_line:
                        if line.product_id:
                            if line.product_id.wallet == True:
                                partner = self.pool.get('res.partner').browse(cr, uid, line.order_id.partner_id.id)
                                total = partner.wallet_credit_amt + line.price_subtotal
                                self.pool.get('res.partner').write(cr, SUPERUSER_ID, partner.id, {'wallet_credit_amt':total})
        return True
    
    def wallet_recharge_alert(self, cr, uid, ids, acquirer_id, context=None):
        sale_obj = self.pool.get('sale.order')
        so_id = request.session.get('sale_last_order_id')
        order = sale_obj.browse(cr, SUPERUSER_ID, so_id, context=context)
        if acquirer_id:
            acquirer_obj = self.pool.get('payment.acquirer')          
            acquire = acquirer_obj.browse(cr, uid, acquirer_id, context=context)
            if acquire.provider == 'wallet':
                if order.partner_id:
                     if order.sale_total_amount > order.partner_id.wallet_credit_amt:
                         raise osv.except_osv(_('Error!'), _('You can not pay amount through wallet. Because wallet amount is less than order amount!'))
        return True
   
    def sale_get_order(self, cr, uid, ids, force_create=False, code=None, update_pricelist=None, context=None):
        sale_order_obj = self.pool['sale.order']
        sale_order_id = request.session.get('sale_order_id')
        sale_order = None
        # Test validity of the sale_order_id
        if sale_order_id and sale_order_obj.exists(cr, SUPERUSER_ID, sale_order_id, context=context):
            sale_order = sale_order_obj.browse(cr, SUPERUSER_ID, sale_order_id, context=context)
        else:
            sale_order_id = None

        # create so if needed
        if not sale_order_id and (force_create or code):
            # TODO cache partner_id session
            partner = self.pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id
            
            for w in self.browse(cr, uid, ids):
                values = {
                    'user_id': w.user_id.id,
                    'partner_id': partner.id,
                    'pricelist_id': partner.property_product_pricelist.id,
                    'section_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'website', 'salesteam_website_sales')[1],
                    'wallet_amount':partner.wallet_credit_amt
                }
                sale_order_id = sale_order_obj.create(cr, SUPERUSER_ID, values, context=context)
                values = sale_order_obj.onchange_partner_id(cr, SUPERUSER_ID, [], partner.id, context=context)['value']
                sale_order_obj.write(cr, SUPERUSER_ID, [sale_order_id], values, context=context)
                request.session['sale_order_id'] = sale_order_id
                sale_order = sale_order_obj.browse(cr, SUPERUSER_ID, sale_order_id, context=context)

        if sale_order_id:
            # TODO cache partner_id session
            partner = self.pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id
            
            wallet = False
            if sale_order.order_line:
                for line in sale_order.order_line:
                    if line.product_id:
                        if line.product_id.wallet:
                            wallet = True
                            break
            if not wallet:
                today = datetime.today()
                start_date = str(today.replace(day=1).strftime(DEFAULT_SERVER_DATE_FORMAT))
                end_date = str(today.strftime(DEFAULT_SERVER_DATE_FORMAT))
                order_ids = self.pool['sale.order'].search(cr, SUPERUSER_ID, [('date_order','>=',start_date),('date_order','<=',end_date),('partner_id','=',partner.id)])
                total = 0.0
                if order_ids:
                    for sale in self.pool['sale.order'].browse(cr, SUPERUSER_ID, order_ids):
                        for line in sale.order_line:
                            if line.product_id:
                                if not line.product_id.wallet:
                                    total += sale.amount_untaxed
                sale_total = sale_order.amount_untaxed
                tax = 0.0
                if total:
                    if total > 0.0 and total <= 1000.0:
                        tax = float(sale_total) * (6.0/100.0)
                    elif total > 1000.0 and total <= 5000.0:
                        tax += 1000.0 * (6.0/100.0)
                        total -= 1000.0
                        tax += float(sale_total) * (8.0/100.0)
                    elif total > 5000 and total <= 10000:
                        tax = 1000.0 * (6.0/100.0)
                        tax += 4000.0 * (8.0/100.0)
                        total -= 5000.0
                        tax += float(sale_total) * (11.0/100.0)
                    else:
                        tax = 1000.0 * (6.0/100.0)
                        tax += 4000.0 * (8.0/100.0)
                        tax += 5000.0 * (11.0/100.0)
                        total -= 10000.0
                        tax += float(sale_total) * (15.0/100.0)
                sale_total_amount = float(sale_total) + float(tax)
                wallet_amount = 0.0
                #~ wallet_amount = self.pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id.wallet_credit_amt
                sale_order.write({'amount_ecom_tax':float(tax), 'sale_total_amount':float(sale_total_amount)})
            else:
                sale_order.write({'amount_ecom_tax':0.0, 'sale_total_amount':sale_order.amount_untaxed})
            #~ # check for change of pricelist with a coupon
            if code and code != sale_order.pricelist_id.code:
                pricelist_ids = self.pool['product.pricelist'].search(cr, SUPERUSER_ID, [('code', '=', code)], context=context)
                if pricelist_ids:
                    pricelist_id = pricelist_ids[0]
                    request.session['sale_order_code_pricelist_id'] = pricelist_id
                    update_pricelist = True

            pricelist_id = request.session.get('sale_order_code_pricelist_id') or partner.property_product_pricelist.id

            # check for change of partner_id ie after signup
            if sale_order.partner_id.id != partner.id and request.website.partner_id.id != partner.id:
                flag_pricelist = False
                if pricelist_id != sale_order.pricelist_id.id:
                    flag_pricelist = True
                fiscal_position = sale_order.fiscal_position and sale_order.fiscal_position.id or False

                values = sale_order_obj.onchange_partner_id(cr, SUPERUSER_ID, [sale_order_id], partner.id, context=context)['value']
                if values.get('fiscal_position'):
                    order_lines = map(int,sale_order.order_line)
                    values.update(sale_order_obj.onchange_fiscal_position(cr, SUPERUSER_ID, [],
                        values['fiscal_position'], [[6, 0, order_lines]], context=context)['value'])

                values['partner_id'] = partner.id
                sale_order_obj.write(cr, SUPERUSER_ID, [sale_order_id], values, context=context)

                if flag_pricelist or values.get('fiscal_position', False) != fiscal_position:
                    update_pricelist = True

            # update the pricelist
            if update_pricelist:
                values = {'pricelist_id': pricelist_id}
                values.update(sale_order.onchange_pricelist_id(pricelist_id, None)['value'])
                sale_order.write(values)
                for line in sale_order.order_line:
                    if line.exists():
                        sale_order._cart_update(product_id=line.product_id.id, line_id=line.id, add_qty=0)

            # update browse record
            if (code and code != sale_order.pricelist_id.code) or sale_order.partner_id.id !=  partner.id:
                sale_order = sale_order_obj.browse(cr, SUPERUSER_ID, sale_order.id, context=context)

        else:
            request.session['sale_order_id'] = None
            return None

        return sale_order
