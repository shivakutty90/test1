# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime, timedelta
import random
from urlparse import urljoin
import werkzeug

from openerp.addons.base.ir.ir_mail_server import MailDeliveryException
from openerp.osv import osv, fields
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT, ustr
from ast import literal_eval
from openerp.tools.translate import _
from openerp.addons.auth_signup.res_users import SignupError
from openerp import SUPERUSER_ID

class res_users(osv.Model):
    _inherit = 'res.users'

    def _get_change(self, cr, uid, ids, context=None):
        return self.pool.get('res.users').search(cr, uid, [('partner_id', 'in', ids)], context=context)

    _columns = {
        'check_privileges': fields.related('partner_id', 'check_privileges', type='boolean', store={
                                                                        'res.partner': (_get_change, ['check_privileges'], 20),
                                                                        'res.users': (lambda self, cr, uid, ids, c={}: ids, ['partner_id'], 20),}, readonly=True, string='Privileges'),
        'check_pensioner': fields.related('partner_id', 'check_pensioner', type='boolean', store={
                                                                        'res.partner': (_get_change, ['check_pensioner'], 20),
                                                                        'res.users': (lambda self, cr, uid, ids, c={}: ids, ['partner_id'], 20),}, readonly=True, string='Pensioner'),
        'check_regular_user': fields.related('partner_id', 'check_regular_user', type='boolean', store={
                                                                        'res.partner': (_get_change, ['check_regular_user'], 20),
                                                                        'res.users': (lambda self, cr, uid, ids, c={}: ids, ['partner_id'], 20),}, readonly=True, string='Regular User'),
        'check_legal_entity': fields.related('partner_id', 'check_legal_entity', type='boolean', store={
                                                                        'res.partner': (_get_change, ['check_legal_entity'], 20),
                                                                        'res.users': (lambda self, cr, uid, ids, c={}: ids, ['partner_id'], 20),}, readonly=True, string='Legal Entity'),
    }

    def _signup_create_user(self, cr, uid, values, context=None):
        """ create a new user from the template user """
        ir_config_parameter = self.pool.get('ir.config_parameter')
        res_country_obj = self.pool.get('res.country')
        res_partner_obj = self.pool.get('res.partner')
        res_groups_obj = self.pool.get('res.groups')
        template_user_id = literal_eval(ir_config_parameter.get_param(cr, uid, 'auth_signup.template_user_id', 'False'))
        assert template_user_id and self.exists(cr, uid, template_user_id, context=context), 'Signup: invalid template user'
        # check that uninvited users may sign up
        if 'partner_id' not in values:
            if not literal_eval(ir_config_parameter.get_param(cr, uid, 'auth_signup.allow_uninvited', 'False')):
                raise SignupError('Signup is not allowed for uninvited users')

        assert values.get('login'), "Signup: no login given for new user"
        assert values.get('partner_id') or values.get('name'), "Signup: no name or partner given for new user"

        # create a copy of the template user (attached to a specific partner_id if given)
        values['active'] = True
        context = dict(context or {}, no_reset_password=True)
        check_privileges = False
        check_pensioner = False
        check_regular_user = False
        check_legal_entity = False
        try:
            with cr.savepoint():
                vals = dict((key, values.get(key)) for key in ('login', 'name', 'password', 'check_privileges', 'check_pensioner', 'check_regular_user', 'check_legal_entity', 'active'))
                print vals, '---------------------------------'
                if 'check_privileges' in vals:
                    if vals['check_privileges'] == 'on':
                        vals['check_privileges'] = True
                        check_privileges = True
                    else:
                        vals['check_privileges'] = False
                else:
                    vals['check_privileges'] = False
                if 'check_pensioner' in vals:
                    if vals['check_pensioner'] == 'on':
                        vals['check_pensioner'] = True
                        check_pensioner = True
                    else:
                        vals['check_pensioner'] = False
                else:
                    vals['check_pensioner'] = False

                if 'check_regular_user' in vals:
                    if vals['check_regular_user'] == 'on':
                        vals['check_regular_user'] = True
                        check_regular_user = True
                    else:
                        vals['check_regular_user'] = False
                else:
                    vals['check_regular_user'] = False
                if 'check_legal_entity' in vals:
                    if vals['check_legal_entity'] == 'on':
                        vals['check_legal_entity'] = True
                        check_legal_entity = True
                    else:
                        vals['check_legal_entity'] = False
                else:
                    vals['check_legal_entity'] = False
                vals['tz'] = 'Europe/Brussels'
                user_id = self.copy(cr, uid, template_user_id, vals, context=context)
                partner_id = self.browse(cr, uid, user_id).partner_id
                self.pool.get('res.partner').message_subscribe_users(cr, uid, [partner_id.id], user_ids=[user_id], context=context)
                parent_values = dict((key, values.get(key)) for key in ('login', 'name', 'second_name', 'mail', 'third_name', 'birthday', 'street', 'street2', 'city', 'postal_code', 'country_id', 'check_privileges', 'check_pensioner', 'check_regular_user', 'check_legal_entity'))
                second_name = parent_values['second_name']
                third_name = parent_values['third_name']
                birthday = parent_values['birthday']                
                if 'mail' in parent_values:
                    parent_values['mail_id'] = parent_values['mail']
                    mail_id = parent_values['mail']
                    del parent_values['mail']
                if 'login' in parent_values:
                    parent_values['email'] = parent_values['login']
                    mail = parent_values['login']
                    del parent_values['login']
                if 'postal_code' in parent_values:
                    parent_values['zip'] = parent_values['postal_code']
                    del parent_values['postal_code']
                if 'country_id' in parent_values and parent_values['country_id']:
                    res_country_sr = res_country_obj.search(cr, uid, [('name','=',parent_values['country_id'])])
                    if res_country_sr:
                        parent_values['country_id'] = res_country_sr[0]
                    else:
                        parent_values['country_id'] = None
                else:
                    parent_values['country_id'] = None
                if 'check_legal_entity' in parent_values and parent_values.get('check_legal_entity'):
                    parent_values['is_company'] = True
                    parent_id = res_partner_obj.create(cr, uid, parent_values, context=context)
                    self.pool.get('res.partner').message_subscribe_users(cr, uid, [parent_id], user_ids=[user_id], context=context)
                    res_partner_obj.write(cr, uid, partner_id.id, {'parent_id':parent_id, 'use_parent_address':True, 'user_id':user_id, 'check_privileges':check_privileges, 'check_pensioner':check_pensioner, 'check_regular_user':check_regular_user, 'check_legal_entity':check_legal_entity, 'email': mail, 'second_name': second_name, 'third_name': third_name, 'birthday': birthday})
                else:
                    parent_values['customer'] = True
                    res_partner_obj.write(cr, uid, partner_id.id, parent_values)
                return user_id
        except Exception, e:
            # copy may failed if asked login is not available.
            raise SignupError(ustr(e))


res_users()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
