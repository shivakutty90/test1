# -*- coding: utf-8 -*-

{
    'name': "Website Sale Event",
    'author' : 'Shiva & Ayan',
    'website' : 'http://www.odoo.com',
    'license' : 'AGPL-3',
    'category': 'web',
    'summary': "website event sale",    
    'version': '1.0',
    'description': """
        """,
    'depends': ['web','website_sale','payment'],
    'data': [
         
        'views/res_partner.xml',
        'views/template.xml',
        #~ 'views/event_view.xml',
        'views/menu_quest.xml',
    ],
    #~ 'qweb': ['static/src/xml/pos.xml'],
    'installable': True,
    'auto_install': True
}
