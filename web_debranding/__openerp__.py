{
    'name': "Backend debranding",
    'version': '1.0.0',
    'author': 'Siva & Ayan',
    'license': 'GPL-3',
    'category': 'Debranding',
    'website': 'www.odoo.com',
    'price': 90.00,
    'currency': 'EUR',
    'depends': ['web'],
    'data': [
        'security/web_debranding_security.xml',
        'data.xml',
        'views.xml',
        'js.xml',
        'pre_install.yml',
        ],
    'qweb': [
        'static/src/xml/database_manager.xml',
    ],
    'auto_install': False,
    'installable': True
}
