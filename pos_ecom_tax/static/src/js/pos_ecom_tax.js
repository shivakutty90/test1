openerp.pos_ecom_tax = function(instance){
    var module   = instance.point_of_sale;
    var round_pr = instance.web.round_precision
    //~ var QWeb = instance.web.qweb;
//~ 
    //~ QWeb.add_template('/pos_discount/static/src/xml/discount.xml');    
    
    var PosModelSuper = module.PosModel;
    module.PosModel = module.PosModel.extend({
        initialize: function(session, attributes) {
            var partner_model = _.find(this.models,function(model){ return model.model === 'res.partner'; });
            partner_model.fields.push('wallet_credit_amt');
            return PosModelSuper.prototype.initialize.call(this, session, attributes);
        },
    });
    
    module.PaymentScreenWidget.include({
		
		init: function(parent, options) {
            var self = this;
            this._super(parent,options);
            this.line_change_handler = function(event){
                var node = this;
                while(node && !node.classList.contains('paymentline')){
                    node = node.parentNode;
                }
                if(node){
                    var amount;
                    try{
                        amount = instance.web.parse_value(this.value, {type: "float"});
                    }
                    catch(e){
                        amount = 0;
                    }
                    partner = self.pos.get_order().get_client();
                    if (partner.wallet_credit_amt < amount)  {    						
						$(".paymentline-input").val(this.value.slice(0,-1));
						}
						
					else{
						node.line.set_amount(amount);}
                }
            };

        },
                
        render_paymentline: function(line){
            el_node = this._super(line);
            var self = this;
            if (line.cashregister.journal.wallet){
                el_node.querySelector('.pay-full-wallet')
                    .addEventListener('click', function(){self.pay_full_debt(line)});
                
                }
            return el_node;
        },

        pay_full_debt: function(line){
            var due_amt = parseFloat($('.payment-due-total').html());
                        
            partner = this.pos.get_order().get_client();
            // Now I write in the amount the debt x -1
            
            //~ line.set_amount(partner.wallet_credit_amt * -1);
            if (partner.wallet_credit_amt >= due_amt)				
				line.set_amount(due_amt);
			else
				line.set_amount(partner.wallet_credit_amt);
            // refresh the display of the payment line
            this.rerender_paymentline(line);
            },
                
        update_payment_summary: function() {
            var self = this;
            var currentOrder = this.pos.get('selectedOrder');
            var paidTotal = currentOrder.getPaidTotal();
            var dueTotal = currentOrder.getTotalTaxExcluded();
            currentOrder.getTotalEcomTax(dueTotal).then(function(result){
                    view_tax(result);
                });
            function view_tax(tax){
                dueTotal = dueTotal ? dueTotal + tax : 0;
                var remaining = dueTotal > paidTotal ? dueTotal - paidTotal : 0;
                var change = paidTotal > dueTotal ? paidTotal - dueTotal : 0;
                self.$('.payment-due-total').html(self.format_currency(dueTotal));
                self.$('.payment-paid-total').html(self.format_currency(paidTotal));
                self.$('.payment-remaining').html(self.format_currency(remaining));
                self.$('.payment-change').html(self.format_currency(change));
                var partner_id = currentOrder.get_client() ? currentOrder.get_client().id : false
                if(partner_id){
					var wallet_amt = currentOrder.get_client().wallet_credit_amt;
					var input_val = currentOrder.getWalletInput();
					if (wallet_amt == input_val) {
						self.$('.wallet-total').html(self.format_currency(0.00));}
					else if (wallet_amt < input_val) {
						self.$('.wallet-total').html(self.format_currency(wallet_amt));}					
					else{
						self.$('.wallet-total').html(self.format_currency(wallet_amt - input_val));}
                    
                    }
            };            
            
            
            if(currentOrder.selected_orderline === undefined){
                remaining = 1;  // What is this ? 
            }
                
            if(this.pos_widget.action_bar){
                this.pos_widget.action_bar.set_button_disabled('validation', !this.is_paid());
                this.pos_widget.action_bar.set_button_disabled('invoice', !this.is_paid());
            }
        },      
        
        add_paymentline: function(line) {
            var list_container = this.el.querySelector('.payment-lines');
                list_container.appendChild(this.render_paymentline(line));
            
            if(this.numpad_state){
                this.numpad_state.reset();
            }            
        },
        
        
        validate_order: function(options) {
            var self = this;
            this._super(parent,options);
            var currentOrder = this.pos.get('selectedOrder');
            var partner_id = currentOrder.get_client() ? currentOrder.get_client().id : false
            if (partner_id) {				
				var plines_new = currentOrder.get('paymentLines').models;
				for (var i = 0; i < plines_new.length; i++) {
					if (plines_new[i].cashregister.journal.wallet) {
						var write_values = {};
						write_values['wallet_credit_amt'] = currentOrder.get_client().wallet_credit_amt - plines_new[i].get_amount();
						
						new instance.web.Model('res.partner').call(
							'write', [
								[partner_id],
								write_values								
							]).done(function () {});
						return;
					}
				}   
			}         
        },        
    })
    

    module.OrderWidget.include({
        
        update_summary: function(){
            var self = this;
            var order = this.pos.get('selectedOrder');
            var total     = order ? order.getTotalTaxExcluded() : 0;
            var taxes     = order ? total - order.getTotalTaxExcluded() : 0;
            var tax = 0;
            order.getTotalEcomTax(total).then(function(result){
                    view_tax(result);
                });
            function view_tax(tax){
                var ecom_taxes = order ? tax : 0;
                self.el.querySelector('.summary .total .ecom_tax .value').textContent = self.format_currency(ecom_taxes);
            };
            
            this.el.querySelector('.summary .total > .value').textContent = this.format_currency(total);
            
        },
        
    });
    
    module.Paymentline = module.Paymentline.extend({        
		getWalletValue: function() {
            return this.cashregister.journal.wallet
        },

    });
    
    module.Order = module.Order.extend({
		
		getWalletInput: function() {
            return round_pr((this.get('paymentLines')).reduce((function(sum, orderLine) {
				if (orderLine.getWalletValue()){
					return sum + orderLine.get_amount();}
            }), 0), this.pos.currency.rounding);
        },
        
		getChangeNew: function() {
            return this.getPaidTotal() - parseFloat($('.payment-due-total').html());
        },
        
        getTotalEcomTax: function(total) {
            var self = this;
            var tax = 0;
            partner_id = this.get_client() ? this.get_client().id : false
            
            tax = new instance.web.Model('pos.order')
                .call('get_ecom_tax_amt',[total, partner_id])
                .then(function(action){             
                    return action;
                });
            return tax;
        },
        
        addPaymentline: function(cashregister) {
            var self = this;
            var journal = cashregister.journal;
            if (journal.debt && ! this.get_client()){
                setTimeout(function(){
                    var ss = self.pos.pos_widget.screen_selector;
                    ss.set_current_screen('clientlist');
                }, 30);
            }

            var paymentLines = this.get('paymentLines');
            var newPaymentline = new module.Paymentline({},{cashregister:cashregister, pos:this.pos});

            if(journal.type !== 'cash' || journal.debt){
                var val;
                var due_total = this.getChangeNew;
                if (journal.debt)
                    val = -this.getChangeNew() || 0
                else
                    val = this.getDueLeft()
                newPaymentline.set_amount( val );
            }
            paymentLines.add(newPaymentline);
            this.selectPaymentline(newPaymentline);
        },

    });
    
    
    
    

};

