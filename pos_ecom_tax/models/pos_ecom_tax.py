#~ from openerp.osv import osv
from openerp.osv import fields, osv
from datetime import datetime, timedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import SUPERUSER_ID

class pos_order(osv.osv):
    _inherit = "pos.order"

    def get_ecom_tax_amt(self, cr, uid, pos_total, partner_id, context=None):   
        tax = 0.0
        if partner_id:    
            today = datetime.today()
            start_date = str(today.replace(day=1).strftime(DEFAULT_SERVER_DATE_FORMAT))
            end_date = str(today.strftime(DEFAULT_SERVER_DATE_FORMAT))
            order_ids = self.pool['pos.order'].search(cr, SUPERUSER_ID, [('date_order','>=',start_date),('date_order','<=',end_date),('partner_id','=',partner_id)])
            total = 0.0
            if order_ids:
                for pos in self.pool['pos.order'].browse(cr, SUPERUSER_ID, order_ids):
                    total += pos.amount_untaxed
            total += pos_total          
            if total:
                if total > 0.0 and total <= 1000.0:
                    tax = float(total) * (6.0/100.0)
                elif total > 1000.0 and total <= 5000.0:
                    tax += 1000.0 * (6.0/100.0)
                    total -= 1000.0
                    tax += float(total) * (8.0/100.0)
                elif total > 5000 and total <= 10000:
                    tax = 1000.0 * (6.0/100.0)
                    tax += 4000.0 * (8.0/100.0)
                    total -= 5000.0
                    tax += float(total) * (11.0/100.0)
                else:
                    tax = 1000.0 * (6.0/100.0)
                    tax += 4000.0 * (8.0/100.0)
                    tax += 5000.0 * (11.0/100.0)
                    total -= 10000.0
                    tax += float(total) * (15.0/100.0)
        return tax

class account_journal(osv.osv):
    _inherit = 'account.journal'
    
    _columns = {
		'wallet': fields.boolean(string='Wallet Payment Method'),
	}
