# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#~ 
import requests
import datetime
from openerp.http import request
from openerp.addons.web import http
from openerp import http
from openerp import models, fields, api, _
from openerp.exceptions import Warning
from datetime import datetime
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF
import re

class project_inherit(models.Model):
    _inherit = 'project.project'
    _description = 'Tracopedia Odoo Link'

    @api.one
    def get_tracopedia_details(self):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        project_obj = pool['project.project']
        project_task_obj = pool['project.task']
        project_work_obj = pool['project.task.work']
        project_name = self.name
        response = requests.get('http://103.249.204.195:2323/Tracopedia/WorkitemReport?Projectname=' + project_name,
                             auth=('user', 'password'))
        data = response.json()
        #values get from tracopedia
        first_name= last_name= user_name= log_date= task_name = comments = working_hour =' '
        for info in data:
            for k, v in info.items():
                if k == 'F_FIRSTNAME':
                    first_name = v
                if k == 'F_LASTNAME':
                    last_name = v
                user_name = first_name+ '.'+ last_name
                if k == 'F_WORKITEM_LOGDATE':
                    log_date = v                    
                    log_date = re.sub(r'[A-Za-z]+', ' ', str(log_date))
                    print  
                if k == 'F_SUBJECT':
                    task_name= v
                if k == 'F_WORKITEM_COMMENTS':
                    comments = v
                if k == 'F_WORKITEM_LOGHOURS':
                    working_hour = v
                #browse and update the data to odoo
            project_task_id = project_task_sr = existing_project_task_sr = None
            project_sr = project_obj.search(cr, uid, [('name','=', project_name)])
            user_id = pool['res.users'].search(cr, uid, [('name','=',user_name)], context=context)
            if project_sr:
                existing_project_task_sr = project_task_obj.search(cr, uid, [('name','=', task_name), ('project_id','=', project_sr[0])])
                if existing_project_task_sr:
                    existing_project_work_sr = project_work_obj.search(cr, uid, [('date','=', log_date), ('hours','=', working_hour),('name','=', comments)])
                    if not existing_project_work_sr:
                        if user_id:
                            project_work_ids = project_work_obj.create(cr, uid, {'name' :comments, 'date':log_date, 'task_id':existing_project_task_sr[0], 'hours':working_hour, 'user_id': user_id[0]})
                            project_work_br = project_work_obj.browse(cr, uid, project_work_ids)
                else:
                    project_task_id = project_task_obj.create(cr, uid, {'name' :task_name, 'project_id':project_sr[0]})
                    if project_task_id and user_id:
                        project_work_id = project_work_obj.create(cr, uid, {'name' :comments, 'date':log_date, 'task_id':project_task_id, 'hours':working_hour, 'user_id': user_id[0]})
        return True
        
