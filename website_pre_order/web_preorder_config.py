# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import models, fields, api, _


class web_preorder_config(models.Model):
    _name = 'web.preorder.config'
   
    _description = 'Preorder Configuration'   
      
    
    name = fields.Char('Name')
    preorder_code =  fields.Char('Code')
    partner_id =  fields.Many2one('res.partner', string='Customer', ondelete="cascade")
    logo =  fields.Boolean(string='Product')
    start_date =  fields.Date('Start Date')
    expire_date =  fields.Date('Expire Date')
    product_ids =  fields.Many2many('product.product', string='Product')
    supplier_expire_date =  fields.Date('Supplier Expire Date')
    delivery_date =fields.Date('Delivery Date')
    #~ webshop_ids = fields.Many2many('webshop.webshop','product_webshop_rel','product_id','webshop_id',string='Webshop')
    multiorder_selection =  fields.Selection([('yes','Yes'), ('no','No')], string='Multiple Pre-Order' )
    view_type =  fields.Selection([('listview','List View'), ('matrixview','Matrix View')], string='Type of View')
    description =  fields.Text('Description')
    previous_sales =  fields.Boolean('Show Previous Sales')
    responsible_manager =  fields.Boolean('Responsible Manager')
    rule_name =  fields.Char('Rule Name')
    discount =  fields.Char('Discount')
    amount =  fields.Char('% Amount')
    parent_id =  fields.Many2one('product.category','Parent Category', select=True, ondelete='cascade')
    #~ 'product_ids' : fields.One2many('product.template', 'product_brand_id', string='Brand'),
      
        
