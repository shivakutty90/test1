# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
import logging
import werkzeug
import werkzeug.utils
from datetime import datetime
from math import ceil

from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF
from openerp.tools.safe_eval import safe_eval

from openerp.osv import fields
from openerp import http
from openerp.addons.website_sale.controllers.main import table_compute
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
from openerp.addons.web.controllers.main import login_redirect

from openerp.addons.website_sale.controllers.main import website_sale as Home
from openerp.addons.website_sale.controllers.main import QueryURL as QueryURL
import openerp.addons.website_sale.controllers.main as inherited_sale

PPG = 20 # Products Per Page
PPR = 4  # Products Per Row



_logger = logging.getLogger(__name__)

class website_preorder(http.Controller):
    
    def _get_search_domain(self, search, category, webshop, attrib_values):
        domain = request.website.sale_product_domain()

        if search:
            for srch in search.split(" "):
                domain += [
                    '|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]

        if category:
            domain += [('publish_webshop_line.public_categ_ids', 'child_of', int(category))]
        
        if webshop:
            domain += [('publish_webshop_line.webshop_id', '=', int(webshop)),('publish_webshop_line.website_published', '=', True)]

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        return domain
    
   ### creating new url
    @http.route('/page/preoder', type='http', auth='public', website=True)
    def preorder(self, page=0, preorder=None, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])
        
        domain = self._get_search_domain(search, category, preorder, attrib_values)
        keep = QueryURL('/page/preoredr', category=category and int(category), preorder=preorder and int(preorder), search=search, attrib=attrib_list)
        url = "/page/preorder"
        if search:
            post["search"] = search 
        if preorder:
            preorder = pool['web.preorder.config'].browse(cr, uid, int(preorder), context=context)
            url = "/page/preorder/%s" % slug(preorder)
        if attrib_list:
            post['attrib'] = attrib_list
        product_obj = pool.get('product.product')
        product_count = product_obj.search_count(cr, uid, domain, context=context)
        pager = request.website.pager(url=url, total=product_count, page=page, step=PPG, scope=7, url_args=post)
        product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        products = product_obj.browse(cr, uid, product_ids, context=context)
        
        preorder_obj = pool['web.preorder.config']
        preorder_ids = preorder_obj.search(cr, uid, [], context=context)
        preorders = preorder_obj.browse(cr, uid, preorder_ids, context=context)
        values = {
            'search': search,
            'preorder': preorder,
            'pager': pager,
            'products': products,
            'bins': table_compute().process(products),
            'rows': PPR,
            
            
            'preorders':preorders,
            'keep': keep,
        }
        return request.website.render("website_pre_order.preorder_init_user", values)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        cr, uid, context = request.cr, request.uid, request.context
        order_obj = http.request.env['preorder.management'].search([])
        user_input_obj = http.request.env['web.preorder.config'].search([])
        if preorder:
            request.context.setdefault('preorder', str(preorder))
        result = super(website_preorder, self).preorder(page=page, 
                                               preorder=preorder, search=search,
                                               **post)
        result.qcontext['preorder'] = preorder
        return http.request.render('website_pre_order.preorder_init_user', {
            'preorder_name' : order_obj, 'preorder_config' : user_input_obj

            }, result)
            
            
    @http.route(
        ['/page/preorder'],
        type='http',
        auth='public',
        website=True)
    def preorder_filter(self, **post):
        cr, context, pool = (request.cr,
                             request.context,
                             request.registry)
        b_obj = pool['web.preorder.config']
        domain = []
        if post.get('search'):
            domain += [('name', 'ilike', post.get('search'))]
        preorder_ids = b_obj.search(cr, SUPERUSER_ID, domain)
        preorder_rec = b_obj.browse(cr, SUPERUSER_ID, preorder_ids, context=context)

        keep = QueryURL('/page/preorder', preorder_id=[])
        values = {'preorder_rec': preorder_rec,
                  'keep': keep}
        if post.get('search'):
            values.update({'search': post.get('search')})
        return request.website.render(
            'website_pre_order.preorder_init_user',
            values)
