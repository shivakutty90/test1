# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution   
#    Copyright (C) 2004-2008 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name" : "Margin Scheme Report",
    "version" : "1.0",
    "author" : 'Magilan Shiv',
    "description": """
       Margin Scheme Report
         """,
        
    "category" : "Accounting",
    "website" : "http://skygst.com/",
    "depends" : ["base", "account", "account_voucher"],
    'data' : [
        "views/report_supplier_overdue.xml",
        
         ],
    'certificate': '',
    "active": True,
    "installable": True,
}
