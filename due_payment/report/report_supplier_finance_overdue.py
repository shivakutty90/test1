# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.report import report_sxw
from openerp.osv import osv
from datetime import datetime,timedelta,date
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

class Supplier_finance_Overdue(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Supplier_finance_Overdue, self).__init__(cr, uid, name, context=context)
        ids = context.get('active_ids')
        partner_obj = self.pool['res.partner']
        docs = partner_obj.browse(cr, uid, ids, context)

        due = {}
        paid = {}
        mat = {}

        for partner in docs:
            due[partner.id] = reduce(lambda x, y: x + ((y['account_id']['type'] == 'payable' and y['credit'] * -1 or 0)), self._lines_get(partner), 0)
            paid[partner.id] = reduce(lambda x, y: x + ((y['account_id']['type'] == 'payable' and y['debit'] * -1 or 0)), self._lines_get(partner), 0)
            mat[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']), filter(lambda x: x['date_maturity'] < time.strftime('%Y-%m-%d'), self._lines_get(partner)), 0)
            
        addresses = self.pool['res.partner']._address_display(cr, uid, ids, None, None)
        self.localcontext.update({
            'docs': docs,
            'time': time,
            'getLines': self._lines_get,
            'getLinesbyDays': self._lines_get_bydays,
            'tel_get': self._tel_get,
            'message': self._message,
            'due': due,
            'paid': paid,
            'mat': mat,
            'addresses': addresses
        })
        self.context = context

    def _tel_get(self,partner):
        if not partner:
            return False
        res_partner = self.pool['res.partner']
        addresses = res_partner.address_get(self.cr, self.uid, [partner.id], ['invoice'])
        adr_id = addresses and addresses['invoice'] or False
        if adr_id:
            adr=res_partner.read(self.cr, self.uid, [adr_id])[0]
            return adr['phone']
        else:
            return partner.phone or False
        return False

    def _lines_get(self, partner):
        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),
                    ('account_id.type', '=', 'payable'),
                    ('state', '<>', 'paid')])
        movelines = moveline_obj.browse(self.cr, self.uid, movelines)
        return movelines
    
    def last_n_months(self, n=12, ending=None):
        """Return a list of tuples of the first/last day of the month
        for the last N months    
        """
        from datetime import date
        from dateutil.rrule import rrule, MONTHLY
        from dateutil.relativedelta import relativedelta
        if not ending:
            ending = date.today()
        ending = ending + relativedelta(months=+1, days=-ending.day)
        # starting is the first day of the month N months ago
        starting = ending - relativedelta(months=n, day=1)

        months = list(rrule(MONTHLY, bymonthday=(1, -1), dtstart=starting,
                            until=ending))
        if n == 12:
            return months
        else:
            return months[:2]
    
    def _get_move_line_amount(self, partner, due_date_from, due_date_to):
        moveline_obj = self.pool['account.move.line']
        self.cr.execute(
            "SELECT a.id " \
            "FROM account_account a " \
            "LEFT JOIN account_account_type t " \
                "ON (a.type=t.code) " \
                'WHERE a.type = %s' \
                "AND a.active", ('payable', ))
        account_ids = [a for (a,) in self.cr.fetchall()]
        movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),('account_id','in',account_ids),
                    ('state', '<>', 'draft'), ('reconcile_id', '=', False),('date','>=',due_date_from),('date','<=',due_date_to),('credit','!=', 0.00)])
        movelines = moveline_obj.browse(self.cr, self.uid, movelines)
        total = reduce(lambda x, y: x + (y['credit']), movelines, 0)
        deduction_movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),('account_id','in',account_ids),
                    ('state', '<>', 'draft'), ('reconcile_id', '=', False)])
        deduction_movelines = moveline_obj.browse(self.cr, self.uid, deduction_movelines)
        d_total = 0.0
        reconcile_partial_ids = [m.reconcile_partial_id.id for m in movelines if m.reconcile_partial_id]
        for d_line in deduction_movelines:
            if d_line.reconcile_partial_id:
                if d_line.reconcile_partial_id.id in reconcile_partial_ids:
                    d_total += d_line.debit
        return total - d_total   
    
    def _lines_get_bydays(self, partner):        
        val = {}
        payable_amt = 0
        for num in range(0,8):
            if num == 7:
                data = self.last_n_months()[::11]
                s_date = data[0]
                e_date = data[1]
            else:
                s_date, e_date = self.last_n_months(num)
            due_date_from = str(s_date.strftime(DEFAULT_SERVER_DATE_FORMAT))
            due_date_to = str(e_date.strftime(DEFAULT_SERVER_DATE_FORMAT))
            val[str(num)] = self._get_move_line_amount(partner, due_date_from, due_date_to)
        return [val]

    def _message(self, obj, company):
        company_pool = self.pool['res.company']
        message = company_pool.browse(self.cr, self.uid, company.id, {'lang':obj.lang}).overdue_msg
        return message.split('\n')

class report_supplier_finance_overdue(osv.AbstractModel):
    _name = 'report.due_payment.report_supplier_finance_overdue'
    _inherit = 'report.abstract_report'
    _template = 'due_payment.report_supplier_finance_overdue'
    _wrapped_report_class = Supplier_finance_Overdue

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
