# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime, timedelta
import random
from urlparse import urljoin
import werkzeug

from openerp.addons.base.ir.ir_mail_server import MailDeliveryException
from openerp.osv import osv, fields
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT, ustr
from ast import literal_eval
from openerp.tools.translate import _
from openerp.addons.auth_signup.res_users import SignupError
from openerp import SUPERUSER_ID

class res_users(osv.Model):
    _inherit = 'res.users'

    def _get_change(self, cr, uid, ids, context=None):
        return self.pool.get('res.users').search(cr, uid, [('partner_id', 'in', ids)], context=context)

    _columns = {

    }

    def _signup_create_user(self, cr, uid, values, context=None):
        """ create a new user from the template user """
        ir_config_parameter = self.pool.get('ir.config_parameter')
        res_city_obj = self.pool.get('res.cities')
        res_partner_obj = self.pool.get('res.partner')
        res_groups_obj = self.pool.get('res.groups')
        template_user_id = literal_eval(ir_config_parameter.get_param(cr, uid, 'auth_signup.event_template_user_id', 'False'))
        assert template_user_id and self.exists(cr, uid, template_user_id, context=context), 'Signup: invalid template user'
        # check that uninvited users may sign up
        if 'partner_id' not in values:
            if not literal_eval(ir_config_parameter.get_param(cr, uid, 'auth_signup.allow_uninvited', 'False')):
                raise SignupError('Signup is not allowed for uninvited users')

        assert values.get('login'), "Signup: no login given for new user"
        assert values.get('partner_id') or values.get('name'), "Signup: no name or partner given for new user"

        # create a copy of the template user (attached to a specific partner_id if given)
        values['active'] = True
        context = dict(context or {}, no_reset_password=True)
        try:
            with cr.savepoint():
                vals = dict((key, values.get(key)) for key in ('login', 'name', 'password', 'active'))
                #~ print vals, '---------------------------------'
                #~ vals['tz'] = 'Europe/Brussels'
                user_id = self.copy(cr, uid, template_user_id, vals, context=context)
                #~ user_id = self.create(cr, uid, vals, context=context)
                partner_id = self.browse(cr, uid, user_id).partner_id
                self.pool.get('res.partner').message_subscribe_users(cr, uid, [partner_id.id], user_ids=[user_id], context=context)
                parent_values = dict((key, values.get(key)) for key in ('login', 'name', 'password', 'parent_id', 'mail_id', 'tag', 'city_id'))
                mail_id = parent_values['mail_id']
                parent_name = parent_values['parent_id']
                #~ if 'team' in parent_values:
                    #~ parent_values['parent_id'] = parent_values['team']
                    #~ parent_id = parent_values['team']
                    #~ del parent_values['team']
                if 'login' in parent_values:
                    parent_values['email'] = parent_values['login']
                    mail = parent_values['login']
                    del parent_values['login']
                if 'tag' in parent_values:
                    del parent_values['tag']
                if 'password' in parent_values:
                    del parent_values['password']
                if parent_name:
                    parent_values['name'] = parent_name
                    del parent_values['parent_id']
                if 'city_id' in parent_values and parent_values['city_id']:
                    res_city_sr = res_city_obj.search(cr, uid, [('name','=',parent_values['city_id'])])
                    if res_city_sr:
                        parent_values['city'] = res_city_sr[0]
                    else:
                        parent_values['city'] = None
                else:
                    parent_values['city'] = None
                parent_values['is_company'] = True
                parent_id = res_partner_obj.create(cr, uid, parent_values, context=context)
                self.pool.get('res.partner').message_subscribe_users(cr, uid, [parent_id], user_ids=[user_id], context=context)
                res_partner_obj.write(cr, uid, partner_id.id, {'parent_id':parent_id, 'use_parent_address':True, 'user_id':user_id, 'mail_id': mail_id})
                return user_id
        except Exception, e:
            # copy may failed if asked login is not available.
            raise SignupError(ustr(e))


res_users()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
