# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
import logging
import werkzeug
import werkzeug.urls
from werkzeug.exceptions import NotFound

from math import ceil

import itertools
from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF
from openerp.tools.safe_eval import safe_eval

from openerp.osv import fields
from openerp import http
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
from openerp.addons.web.controllers.main import login_redirect


import babel.dates
import time
from openerp import tools
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from openerp.addons.auth_signup.controllers.main import AuthSignupHome
from openerp.addons.auth_signup.res_users import SignupError
import openerp

import logging
import pprint

_logger = logging.getLogger(__name__)

class AuthSignupHome(AuthSignupHome):

    #~ @http.route('/web/signup', type='http', auth='public', website=True)
    #~ def web_auth_signup(self, *args, **kw):
        #~ qcontext = self.get_auth_signup_qcontext()
        #~ res_country = request.registry.get('res.country')
        #~ country_ids = res_country.search(request.cr, openerp.SUPERUSER_ID, [])
        #~ country_names = []
        #~ for country_id in country_ids:
            #~ country_names.append(res_country.browse(request.cr, openerp.SUPERUSER_ID, country_id).name)
        #~ qcontext['country_names'] = country_names
        #~ if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            #~ raise werkzeug.exceptions.NotFound()
#~ 
        #~ if 'error' not in qcontext and request.httprequest.method == 'POST':
            #~ try:
                #~ self.do_signup(qcontext)
                #~ return super(AuthSignupHome, self).web_login(*args, **kw)
            #~ except (SignupError, AssertionError), e:
                #~ qcontext['error'] = _(e.message)
        #~ return request.render('auth_signup.signup', qcontext)
    
    @http.route('/web/signup', type='http', auth='public', website=True)
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()
        res_cities = request.registry.get('res.cities')
        city_ids = res_cities.search(request.cr, openerp.SUPERUSER_ID, [])
        city_names = []
        for city_id in city_ids:
            city_names.append(res_cities.browse(request.cr, openerp.SUPERUSER_ID, city_id).name)
        qcontext['city_names'] = city_names
        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                self.do_signup(qcontext)
                return super(AuthSignupHome, self).web_login(*args, **kw)
            except (SignupError, AssertionError), e:
                qcontext['error'] = _(e.message)
        return request.render('auth_signup.signup', qcontext)

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = dict((key, qcontext.get(key)) for key in ('login', 'name', 'password', 'parent_id', 'mail_id', 'tag', 'city_id'))
        assert any([k for k in values.values()]), "The form was not properly filled in."
        assert values.get('password') == qcontext.get('confirm_password'), "Passwords do not match; please retype them."
        self._signup_with_values(qcontext.get('token'), values)
        request.cr.commit()
        

class QueryURL(object):
    def __init__(self, path='', **args):
        self.path = path
        self.args = args

    def __call__(self, path=None, **kw):
        if not path:
            path = self.path
        for k,v in self.args.items():
            kw.setdefault(k,v)
        l = []
        for k,v in kw.items():
            if v:
                if isinstance(v, list) or isinstance(v, set):
                    l.append(werkzeug.url_encode([(k,i) for i in v]))
                else:
                    l.append(werkzeug.url_encode([(k,v)]))
        if l:
            path += '?' + '&'.join(l)
        return path

class event_registration(http.Controller):
    
    @http.route(['/event_booking', '/event_booking/page/<int:page>', '/event_booking/<model("event.event"):event>'], type='http', auth='public', website=True)
    def event_booking(self, page=0, event=None, **searches):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        event_ticket_obj = http.request.env['event.event.ticket']
        event_obj = http.request.env['event.event']
        type_obj = request.registry['event.type']
        city_obj = request.registry['res.cities']
        searches.setdefault('city', 'All')
        searches.setdefault('type', 'all')
        domain = []
        current_type = None
        event_ticket_ids = []
        keep = QueryURL('/event_booking', event=event and int(event))        
        if event:
            domain += [('id','=',int(event))]
            event = pool['event.event'].browse(cr, uid, int(event), context=context)
            url = "/event_booking/%s" % slug(event)

        request.session.context['city'] = searches['city']
        if searches["city"] != 'All':
            cities = city_obj.search(cr, SUPERUSER_ID, [("name", "=",searches["city"])])    
            if cities:        
                domain += [("city", "=",cities[0])]
        if searches["type"] != 'all':
            current_type = type_obj.browse(cr, uid, int(searches['type']), context=context)
            domain += [("type", "=", int(searches["type"]))]
        domain += [('state', "in", ['draft','confirm','done']), ('parent_id','=',None)]   
        
        print domain, '33333333333333333333333333333'
        types = request.registry['event.event'].read_group(
            request.cr, request.uid, domain, ["id", "type"], groupby="type",
            orderby="type", context=request.context)
        print types, '55555555555555555555'
        type_count = request.registry['event.event'].search(request.cr, request.uid, domain,
                                      count=True, context=request.context)
        types.insert(0, {
            'type_count': type_count,
            'type': ("all", _("All Categories"))
        })
        print types, '33333333333333333'
             
        
        step = 10 # Number of events per page
        event_count = request.registry['event.event'].search(
            request.cr, request.uid, domain, count=True,
            context=request.context)
        pager = request.website.pager(
            url="/event_booking",
            url_args={'city': searches.get('city'), 'type': searches.get('type')},
            total=event_count,
            page=page,
            step=step,
            scope=5)
            
        order = 'date_begin desc'
        event_event = http.request.env['event.event'].search(domain,limit=step,
            offset=pager['offset'], order=order)
        print event_event, "HHHHHHHHHHHHHHHHh"
        for e in event_event:
            if isinstance(e, int):
                event_br = pool['event.event'].browse(cr, uid, e, context=context)
            else:
                event_br = e
            event_ticket_sr = event_ticket_obj.search([('event_id', '=', event_br.id)])
            if event_ticket_sr:
                for et in event_ticket_sr:
                    event_ticket_ids.append(et.id)
        filter_events = http.request.env['event.event'].search([])
        event_tickets = event_ticket_obj.search([('id', 'in', event_ticket_ids)])
        return http.request.render('website_event_registration.event_item_list', {
                                            'events': event_event,
                                            'filter_events': filter_events,
                                            'types': types,
                                            'searches': searches,
                                            'search_path': "?%s" % werkzeug.url_encode(searches),
                                            'event_tickets':event_tickets,
                                            'pager': pager,
                                            'keep':keep,
                                            'event':event})
    
    @http.route(['/event_registration/<model("event.event"):event>', '/event_registration/<model("event.event"):event>/page/<int:page>'], type='http', auth='public', website=True)
    def event_registration(self, page=0, event=None, **searches):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        event_ticket_obj = http.request.env['event.event.ticket']
        #~ searches.setdefault('city', 'All') 
        domain = []
        domain_search = {}
        event_ticket_ids = []
        #~ if searches['city'] and searches['city'] != 'All':
            #~ domain_search["city"] = [("city", "=", searches['city'])]
        
        keep = QueryURL('/page/event_registration', event=event and int(event))                
        if event:
            domain = ['|',('id','=',int(event)),('parent_id','=',int(event))]
            event = pool['event.event'].browse(cr, uid, int(event), context=context)
            url = "/event_registration/%s" % slug(event)
        
        domain += [('state', "in", ['draft','confirm','done'])]
        def sd(date):
            return date.strftime('%Y-%m-%d 00:00:00')
            
        today = datetime.today()
        domain += [("date_begin", ">=", sd(today))]
                #~ ("date_begin", "<", (today.replace(day=1)  + relativedelta(months=2)).strftime('%Y-%m-%d 00:00:00'))],]
        
        step = 7  # Number of events per page
        event_count = request.registry['event.event'].search(
            request.cr, request.uid, domain, count=True,
            context=request.context)
        pager = request.website.pager(
            url=url,            
            total=event_count,
            page=page,
            step=step,
            scope=5)
            
        order = 'date_begin'
        event_event = http.request.env['event.event'].search(domain,limit=step,
            offset=pager['offset'], order=order)
        
        for e in event_event:
            if isinstance(e, int):
                event_br = pool['event.event'].browse(cr, uid, e, context=context)
            else:
                event_br = e
            event_ticket_sr = event_ticket_obj.search([('event_id', '=', event_br.id)])
            if event_ticket_sr:
                for et in event_ticket_sr:
                    event_ticket_ids.append(et.id)
        filter_events = http.request.env['event.event'].search([])
        event_tickets = event_ticket_obj.search([('id', 'in', event_ticket_ids)])
        return http.request.render('website_event_registration.event_registration_init', {
                                            'events': event_event,
                                            'filter_events': filter_events,
                                            'event_tickets':event_tickets,
                                            'searches': searches,
                                            'pager': pager,
                                            'event':event,
                                            'keep': keep})
    
    @http.route(['/event_registration/cart/update'], type='http', auth="user", methods=['POST'], website=True)
    def cart_update(self, event_id, **post):
        cr, uid, context = request.cr, request.uid, request.context
        ticket_obj = request.registry.get('event.event.ticket')
        event_obj = request.registry['event.event']
        sale = False            
        quantity = int(1)
        ticket_id = post.get('tid') or None
        if ticket_id:
            
            ticket = ticket_obj.browse(cr, SUPERUSER_ID, int(ticket_id), context=context)
            lines = [(0,0,{
                'email': str(post.get('email')) or partner.email,
                'name': str(post.get('usrname')) or partner.name,
                'user_id': uid,
                'nb_register': 1,
                'event_ticket_id':ticket.id})]            
            event_obj.write(cr, SUPERUSER_ID, [ticket.event_id.id], {'registration_ids':lines})            
            
            order = request.website.sale_get_order(force_create=1)
            order.with_context(event_ticket_id=ticket.id)._cart_update(product_id=ticket.product_id.id, add_qty=quantity)
            sale = True

        if not sale:
            return request.redirect("/event_registration/%s" % event_id)
            
        request.session['sale_last_order_id'] = order.id
        request.website.sale_get_order(update_pricelist=True, context=context)
        return request.redirect("/shop/payment")
                                           
    
    @http.route(['/event_registration/order_now'], type='http', auth="user", website=True)
    def order_now(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        partner = pool['res.users'].browse(cr, SUPERUSER_ID, context=context).partner_id
        event_ticket_obj = request.registry['event.event.ticket']
        event_obj = request.registry['event.event']
        if post.get('tid'):
            event_ticket_br = event_ticket_obj.browse(cr, SUPERUSER_ID, int(post.get('tid')))            
            lines = [(0,0,{
                'email': partner.email,
                'name':partner.name,
                'user_id': uid,
                'nb_register': 1,
                'event_ticket_id':event_ticket_br.id})]            
            event_obj.write(cr, uid, [event_ticket_br.event_id.id], {'registration_ids':lines})
            return request.redirect('/event_registration/%s' % event_ticket_br.event_id.id)   
    
    @http.route(['/shop/payment/transaction/<int:acquirer_id>'], type='json', auth="public", website=True)
    def payment_transaction(self, acquirer_id):
        """ Json method that creates a payment.transaction, used to create a
        transaction when the user clicks on 'pay now' button. After having
        created the transaction, the event continues and the user is redirected
        to the acquirer website.

        :param int acquirer_id: id of a payment.acquirer record. If not set the
                                user is redirected to the checkout page
        """
        cr, uid, context = request.cr, request.uid, request.context
        transaction_obj = request.registry.get('payment.transaction')
        order = request.website.sale_get_order(context=context)

        if not order or not order.order_line or acquirer_id is None:
            return request.redirect("/shop/checkout")

        assert order.partner_id.id != request.website.partner_id.id
        
        country_id = request.registry.get('res.country').search(cr, SUPERUSER_ID, [('code', '=', 'RU')])
        if country_id:
            request.registry.get('res.partner').write(cr, SUPERUSER_ID, order.partner_id.id, {'country_id':country_id[0]})

        # find an already existing transaction
        tx = request.website.sale_get_transaction()
        if tx:
            if tx.state == 'draft':  # button cliked but no more info -> rewrite on tx or create a new one ?
                tx.write({
                    'acquirer_id': acquirer_id,
                    'amount': order.amount_total,
                })
            tx_id = tx.id
        else:
            tx_id = transaction_obj.create(cr, SUPERUSER_ID, {
                'acquirer_id': acquirer_id,
                'type': 'form',
                'amount': order.amount_total,
                'currency_id': order.pricelist_id.currency_id.id,
                'partner_id': order.partner_id.id,
                'partner_country_id': order.partner_id.country_id.id,
                'reference': order.name,
                'sale_order_id': order.id,
            }, context=context)
            request.session['sale_transaction_id'] = tx_id

        # update quotation
        request.registry['sale.order'].write(
            cr, SUPERUSER_ID, [order.id], {
                'payment_acquirer_id': acquirer_id,
                'payment_tx_id': request.session['sale_transaction_id']
            }, context=context)

        return tx_id

    
    def checkout_values(self, data=None):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        orm_partner = registry.get('res.partner')
        orm_user = registry.get('res.users')
        orm_country = registry.get('res.country')
        state_orm = registry.get('res.country.state')

        country_ids = orm_country.search(cr, SUPERUSER_ID, [], context=context)
        countries = orm_country.browse(cr, SUPERUSER_ID, country_ids, context)
        states_ids = state_orm.search(cr, SUPERUSER_ID, [], context=context)
        states = state_orm.browse(cr, SUPERUSER_ID, states_ids, context)
        partner = orm_user.browse(cr, SUPERUSER_ID, request.uid, context).partner_id

        order = None

        shipping_id = None
        shipping_ids = []
        checkout = {}
        if not data:
            if request.uid != request.website.user_id.id:
                checkout.update( self.checkout_parse("billing", partner) )
                shipping_ids = orm_partner.search(cr, SUPERUSER_ID, [("parent_id", "=", partner.id), ('type', "=", 'delivery')], context=context)
            else:
                order = request.website.sale_get_order(force_create=1, context=context)
                if order.partner_id:
                    domain = [("partner_id", "=", order.partner_id.id)]
                    user_ids = request.registry['res.users'].search(cr, SUPERUSER_ID, domain, context=dict(context or {}, active_test=False))
                    if not user_ids or request.website.user_id.id not in user_ids:
                        checkout.update( self.checkout_parse("billing", order.partner_id) )
        else:
            checkout = self.checkout_parse('billing', data)
            try: 
                shipping_id = int(data["shipping_id"])
            except ValueError:
                pass
            if shipping_id == -1:
                checkout.update(self.checkout_parse('shipping', data))

        if shipping_id is None:
            if not order:
                order = request.website.sale_get_order(context=context)
            if order and order.partner_shipping_id:
                shipping_id = order.partner_shipping_id.id

        shipping_ids = list(set(shipping_ids) - set([partner.id]))

        if shipping_id == partner.id:
            shipping_id = 0
        elif shipping_id > 0 and shipping_id not in shipping_ids:
            shipping_ids.append(shipping_id)
        elif shipping_id is None and shipping_ids:
            shipping_id = shipping_ids[0]

        ctx = dict(context, show_address=1)
        shippings = []
        if shipping_ids:
            shippings = shipping_ids and orm_partner.browse(cr, SUPERUSER_ID, list(shipping_ids), ctx) or []
        if shipping_id > 0:
            shipping = orm_partner.browse(cr, SUPERUSER_ID, shipping_id, ctx)
            checkout.update( self.checkout_parse("shipping", shipping) )

        checkout['shipping_id'] = shipping_id

        # Default search by user country
        if not checkout.get('country_id'):
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                country_ids = request.registry.get('res.country').search(cr, uid, [('code', '=', country_code)], context=context)
                if country_ids:
                    checkout['country_id'] = country_ids[0]

        values = {
            'countries': countries,
            'states': states,
            'checkout': checkout,
            'shipping_id': partner.id != shipping_id and shipping_id or 0,
            'shippings': shippings,
            'error': {},
            'has_check_vat': hasattr(registry['res.partner'], 'check_vat')
        }

        return values
