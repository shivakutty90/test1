
from openerp import models, fields, api, _

class res_cities(models.Model):
    _name = 'res.cities'
      
    name = fields.Char(string='City Name')
