
from openerp.osv import fields,osv

class res_partner(osv.osv):
    """ Inherits partner and adds event information in the partner form """
    _inherit = 'res.partner'   
    def _count_events(self, cursor, user, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cursor, user, ids, context=context):            
            event_sr = self.pool.get('event.registration').search(cursor, user, [('partner_id','=',partner.id)])                        
            if event_sr:
                res[partner.id] = len(event_sr)
            else:
                res[partner.id] = 0            
        return res       

    _columns = {            
        'event_count': fields.function(_count_events, string='Events', type="integer"),
        'age' : fields.char('Age'),
        'tag': fields.char('Tag'),
        'contact_ref' : fields.char('Contact Reference'),
        'contact_ref1' : fields.char('Contact Reference1'),
        'mail_id' : fields.char('Email'),
        'email' : fields.char('Phone'),       
        'city' : fields.many2one('res.cities', 'City'),
    }
    
    def get_user_events(self,cr,uid,ids,context=None):
        if context is None:
            context = {}        
        registration_ids = self.pool.get('event.registration').search(cr, uid, [('partner_id','=',ids[0])]) 
        
        return {
            'type': 'ir.actions.act_window',
            'name': 'Events Registration',
            'view_type': 'form',
            'view_mode': 'tree,form',            
            'domain' : [('id','in',registration_ids)],
            'res_model': 'event.registration',
            'nodestroy': True,
        }
    
res_partner()
   
