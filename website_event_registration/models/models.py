# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import osv, models, fields, api
from openerp.http import request
from openerp import SUPERUSER_ID

class crm_case_section(models.Model):
    _inherit = "crm.case.section"

    company_id = fields.Integer()

    def create(self, cr, uid, vals, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=None)
        vals['company_id'] = user.company_id.id

        return super(crm_case_section, self).create(cr, uid, vals, context)
        
class ecom_hr_employee(models.Model):
    _inherit = "hr.employee"

class event_event(models.Model):
    _inherit = "event.event"

    city = fields.Many2one('res.cities', 'City')
    event_time = fields.Char('Event Time')
    no_of_lock = fields.Char('No Of Lock')
    no_of_user = fields.Char('No Of User')
    
class website(models.Model):
    _inherit = "website"

    def _get_events_citys(self):
        cr, uid, context= request.cr, request.uid, request.context
        citys = []
        citiy_ids = self.pool['res.cities'].search(cr, SUPERUSER_ID, [])
        cities = self.pool['res.cities'].browse(cr, SUPERUSER_ID, citiy_ids)
        for x in cities:
            if x['name'] not in citys:
                citys.append(x['name'])
        citys.insert(0, 'All')        
        return citys

    def _get_events(self, city=None):
        cr, uid, context= request.cr, request.uid, request.context
        if city and city != 'All':
            cities = self.pool['res.cities'].search(cr, SUPERUSER_ID, [('name', '=', city)])
            if cities:
                return self.env['event.event'].search([('city', '=', cities[0]),('parent_id', '=', None)])
        if not city or city == 'All':
            return self.env['event.event'].search([('parent_id', '=', None)])    
