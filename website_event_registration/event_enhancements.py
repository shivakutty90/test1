# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import pytz
import re
import time
from datetime import datetime, timedelta
from operator import itemgetter

import openerp
from openerp import api
import openerp.service.report
from openerp import tools, SUPERUSER_ID
from dateutil import parser
from dateutil import rrule
from dateutil.relativedelta import relativedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

import string
import smtplib
from email.message import Message
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
import logging
_logger = logging.getLogger(__name__)   

#from openerp import models, fields, api, _

class event_enhancements(osv.osv):
    _inherit = "event.event"    
    _description = "Event Enhancements"
    
    def get_recurrent_date_by_event(self, cr, uid, event, context=None):
        
        def todate(date):
            val = parser.parse(''.join((re.compile('\d')).findall(date)))
            ## Dates are localized to saved timezone if any, else current timezone.
            if not val.tzinfo:
                val = pytz.UTC.localize(val)
            return val.astimezone(timezone)

        timezone = pytz.timezone(context.get('tz') or 'UTC')
        startdate = pytz.UTC.localize(datetime.strptime(event.start, DEFAULT_SERVER_DATETIME_FORMAT))  # Add "+hh:mm" timezone
        if not startdate:
            startdate = datetime.now()

        ## Convert the start date to saved timezone (or context tz) as it'll
        ## define the correct hour/day asked by the user to repeat for recurrence.
        startdate = startdate.astimezone(timezone)  # transform "+hh:mm" timezone
        rset1 = rrule.rrulestr(str(event.rrule), dtstart=startdate, forceset=True)
        
        ids_depending = self.search(cr, uid, [('recurrent_id', '=', event.id)], context=context)
        all_events = self.browse(cr, uid, ids_depending, context=context)
        for ev in all_events:
            rset1._exdate.append(todate(ev.recurrent_id_date))
        return [d.astimezone(pytz.UTC) for d in rset1]
    
    def _compute(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        if not isinstance(fields, list):
            fields = [fields]
        for event in self.browse(cr, uid, ids, context=context):
            event_data = {}
            res[event.id] = event_data            
            for field in fields:                
                if field == 'start':                    
                    event_data[field] = event.date_begin
                elif field == 'stop':                    
                    event_data[field] = event.date_end
        return res
    
    def _get_rulestring(self, cr, uid, ids, name, arg, context=None):
        """
        Gets Recurrence rule string according to value type RECUR of iCalendar from the values given.
        @return: dictionary of rrule value.
        """
        result = {}
        if not isinstance(ids, list):
            ids = [ids]

        #read these fields as SUPERUSER because if the record is private a normal search could raise an error
        events = self.read(cr, SUPERUSER_ID, ids,
                           ['id', 'byday', 'recurrency', 'final_date', 'rrule_type', 'month_by',
                            'interval', 'count', 'end_type', 'mo', 'tu', 'we', 'th', 'fr', 'sa',
                            'su', 'day', 'week_list'], context=context)
        for event in events:
            if event['recurrency']:
                result[event['id']] = self.compute_rule_string(event)
            else:
                result[event['id']] = ''

        return result
    
    def _set_rulestring(self, cr, uid, ids, field_name, field_value, args, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        data = self._get_empty_rrule_data()
        if field_value:
            data['recurrency'] = True
            for event in self.browse(cr, uid, ids, context=context):
                rdate = event.start
                update_data = self._parse_rrule(field_value, dict(data), rdate)
                data.update(update_data)
                self.write(cr, uid, ids, data, context=context)
        return True
    
    def _get_recurrency_end_date(self, cr, uid, id, context=None):
        data = self.read(cr, uid, id, ['final_date', 'recurrency', 'rrule_type', 'count', 'end_type', 'stop'], context=context)

        if not data.get('recurrency'):
            return False

        end_type = data.get('end_type')
        final_date = data.get('final_date')
        if end_type == 'count' and all(data.get(key) for key in ['count', 'rrule_type', 'stop']):
            count = data['count'] + 1
            delay, mult = {
                'daily': ('days', 1),
                'weekly': ('days', 7),
                'monthly': ('months', 1),
                'yearly': ('years', 1),
            }[data['rrule_type']]

            deadline = datetime.strptime(data['stop'], tools.DEFAULT_SERVER_DATETIME_FORMAT)
            return deadline + relativedelta(**{delay: count * mult})
        return final_date
    
    def _set_date(self, cr, uid, values, id=False, context=None):

        if context is None:
            context = {}

        if values.get('date_begin') or values.get('start') \
                or values.get('date_end') or values.get('stop'):        
            
            if values.get('date_end') or values.get('stop'):                
                values['date_end'] = values.get('date_end') or values.get('stop')               
                if 'stop' not in values.keys():
                    values['stop'] = values['date_end']
            if values.get('date_begin') or values.get('start'):             
                values['date_begin'] = values.get('date_begin') or values.get('start')              
                if 'start' not in values.keys():
                    values['start'] = values['date_begin']

            diff = False
            
            if values.get('date_end') and values.get('date_begin'):
                diff = datetime.strptime(values['date_end'].split('.')[0], DEFAULT_SERVER_DATETIME_FORMAT) - datetime.strptime(values['date_begin'].split('.')[0], DEFAULT_SERVER_DATETIME_FORMAT)
            if diff:
                duration = float(diff.days) * 24 + (float(diff.seconds) / 3600)
                values['duration'] = round(duration, 2)
    
    def _count_recurrent_events(self, cursor, user, ids, name, arg, context=None):
        res = {}
        for event in self.browse(cursor, user, ids, context=context):
            if event.link_id:
                event_sr = self.search(cursor, user, [('link_id','=',event.link_id.id)])            
                if event_sr:
                    res[event.id] = len(event_sr)
                else:
                    res[event.id] = 0
            else:
                res[event.id] = 0
        return res
        
      
    
    _columns = {
        'logo':fields.binary(string='Logo'),
        'rrule': fields.function(_get_rulestring, type='char', fnct_inv=_set_rulestring, store=True, string='Recurrent Rule'),
        'recurrency': fields.boolean('Recurrent', help="Recurrent Meeting"), 
        'recurrent_id': fields.integer('Recurrent ID'),
        'recurrent_id_date': fields.datetime('Recurrent ID date'),
        'interval': fields.integer('Repeat Every', help="Repeat every (Days/Week/Month/Year)"),
        'rrule_type': fields.selection([('daily', 'Day(s)'), ('weekly', 'Week(s)'), ('monthly', 'Month(s)'), ('yearly', 'Year(s)')], 'Recurrency', states={'done': [('readonly', True)]}, help="Let the event automatically repeat at that interval"),
        'end_type': fields.selection([('count', 'Number of repetitions'), ('end_date', 'End date')], 'Recurrence Termination'),
        'count': fields.integer('Repeat', help="Repeat x times"),
        'final_date': fields.date('Repeat Until'),  # The last event of a recurrence
        ## Weeks
        'mo': fields.boolean('Mon'),
        'tu': fields.boolean('Tue'),
        'we': fields.boolean('Wed'),
        'th': fields.boolean('Thu'),
        'fr': fields.boolean('Fri'),
        'sa': fields.boolean('Sat'),
        'su': fields.boolean('Sun'),
        ## Month
        'month_by': fields.selection([('date', 'Date of month'), ('day', 'Day of month')], 'Option', oldname='select1'),
        'day': fields.integer('Date of month'),
        'week_list': fields.selection([('MO', 'Monday'), ('TU', 'Tuesday'), ('WE', 'Wednesday'), ('TH', 'Thursday'), ('FR', 'Friday'), ('SA', 'Saturday'), ('SU', 'Sunday')], 'Weekday'),
        'byday': fields.selection([('1', 'First'), ('2', 'Second'), ('3', 'Third'), ('4', 'Fourth'), ('5', 'Fifth'), ('-1', 'Last')], 'By day'),
        
        'start': fields.function(_compute, string='Calculated start', type="datetime", multi='event', store=True, required=True),
        'stop': fields.function(_compute, string='Calculated stop', type="datetime", multi='event', store=True, required=True),
        'duration': fields.float('Duration'),         
        'parent_id': fields.many2one('event.event', "Ref"),
        'link_id': fields.many2one('event.event', "Current Ref"),
        'recurrent_count': fields.function(_count_recurrent_events, string='count', type="integer"), 
        'email_process': fields.boolean(string="Send email to registered users?"),        
        }
    _defaults = {
        'end_type': 'count',
        'count': 1,
        'rrule_type': False,
        'interval': 1,      
        'month_by': 'date', 
        'email_process':True, 
        #'date_begin': fields.datetime.now,
        #'date_end': fields.datetime.now,
    }
    
    
    
    
    def onchange_dates(self, cr, uid, ids, fromtype, start=False, end=False, context=None):

        """Returns duration and end date based on values passed
        @param ids: List of calendar event's IDs.
        """
        value = {}
        
        if fromtype == 'start' and start:
            start = datetime.strptime(start, DEFAULT_SERVER_DATETIME_FORMAT)
            value['date_end'] = start
            value['start'] = datetime.strftime(start, DEFAULT_SERVER_DATETIME_FORMAT)
        if fromtype == 'stop' and end:
            end = datetime.strptime(end, DEFAULT_SERVER_DATETIME_FORMAT)
            #value['date_end'] = datetime.strftime(end, DEFAULT_SERVER_DATE_FORMAT)
            value['stop'] = datetime.strftime(end, DEFAULT_SERVER_DATETIME_FORMAT)

        return {'value': value}
    
    def get_search_fields(self, browse_event, order_fields, r_date=None):
        sort_fields = {}
        for ord in order_fields:
            if ord == 'id' and r_date:
                sort_fields[ord] = '%s-%s' % (browse_event[ord], r_date.strftime("%Y%m%d%H%M%S"))
            else:
                sort_fields[ord] = browse_event[ord]
                if type(browse_event[ord]) is openerp.osv.orm.browse_record:
                    name_get = browse_event[ord].name_get()
                    if len(name_get) and len(name_get[0]) >= 2:
                        sort_fields[ord] = name_get[0][1]
        return sort_fields
    
    def modified_end_date(self, cr, uid, ev, fnl_date, context=None):        
        dt = datetime.strptime(fnl_date, DEFAULT_SERVER_DATETIME_FORMAT)
        day_string = dt.strftime('%Y-%m-%d')
        time_string = ev.date_end.split()[1]
        dt_string = day_string + ' ' + time_string
        f_dt_string = datetime.strptime(dt_string, DEFAULT_SERVER_DATETIME_FORMAT)
        return datetime.strftime(f_dt_string, DEFAULT_SERVER_DATETIME_FORMAT)
    
    def create(self, cr, uid, vals, context=None):        
        if context is None:
            context = {}   
        cpy_ids = []
        self._set_date(cr, uid, vals, id=False, context=context)
        if not 'user_id' in vals:  # Else bug with quick_create when we are filter on an other user
            vals['user_id'] = uid
        res = super(event_enhancements, self).create(cr, uid, vals, context=context)
        final_date = self._get_recurrency_end_date(cr, uid, res, context=context)
        ctx = context
        ctx['count'] = 0
        """if 'seats_max' in vals.keys():
            self.write(cr, uid, [res], {'final_date': final_date, 'seats_max': vals['seats_max']}, context=ctx)
        else:
            self.write(cr, uid, [res], {'final_date': final_date}, context=ctx)"""
        self.write(cr, uid, [res], {'final_date': final_date}, context=ctx)
        ev = self.browse(cr, uid, res, context=context)     
        if ev.recurrency and not ev.parent_id:
            r_date = self.get_recurrent_date_by_event(cr, uid, ev, context=context)           
            count = 0
            for d in r_date:
                dt = datetime.strftime(d, DEFAULT_SERVER_DATETIME_FORMAT)
                f_dt = self.modified_end_date(cr, uid, ev, dt, context=context)
                if count == 0:              
                    self.pool.get('event.event').write(cr, uid, [res], {'date_begin': dt, 'date_end':f_dt}, context=context)
                    count += 1
                else:
                    id = self.pool.get('event.event').copy(cr, uid, ev.id, {'parent_id':ev.id, 'date_begin': dt, 'date_end':f_dt, 'event_ticket_ids':[], 'link_id':ev.id, 'registration_ids':[]}, context=context)                                        
                    
                    self.pool.get('event.event').write(cr, uid, [ev.id], {'link_id': ev.id}, context=context)             
                    if ev.event_ticket_ids:
                        for t in ev.event_ticket_ids:
                            self.pool.get('event.event.ticket').copy(cr, uid, t.id,{'event_id':id})
                    if ev.registration_ids:
                        for reg in ev.registration_ids:
                            self.pool.get('event.registration').copy(cr, uid, reg.id,{'event_id':id})
                    #self.pool.get('event.event').write(cr, uid, [id], {'date_begin': dt, 'date_end':dt, 'parent_id':ev.id}, context=context)
        return res
    
    def write(self, cr, uid, ids, vals, context=None):        
        context = context or {}
        self._set_date(cr, uid, vals, id=ids[0], context=context)  
        res = super(event_enhancements, self).write(cr, uid, ids, vals, context=context)        
        if vals.get('recurrency'):
            if vals['recurrency']:
                ev_sr = self.search(cr, uid, [('parent_id', '=', ids[0])], context=context)
                if not ev_sr:
                    ev = self.browse(cr, uid, ids[0], context=context)
                    if ev.recurrency and not ev.parent_id:
                        r_date = self.get_recurrent_date_by_event(cr, uid, ev, context=context)            
                        count = 0
                        for d in r_date:  
                            dt = datetime.strftime(d, DEFAULT_SERVER_DATETIME_FORMAT)
                            f_dt = self.modified_end_date(cr, uid, ev, dt, context=context)
                            if count == 0:              
                                self.pool.get('event.event').write(cr, uid, ids, {'date_begin': dt, 'date_end':f_dt}, context=context)
                                count += 1
                            else:
                                id = self.pool.get('event.event').copy(cr, uid, ev.id, {'parent_id':ev.id, 'date_begin': dt, 'date_end':f_dt, 'event_ticket_ids':[], 'link_id':ev.id}, context=context)                  
                                self.pool.get('event.event').write(cr, uid, [ev.id], {'link_id': ev.id}, context=context)                    
            else:
                ev_sr = self.search(cr, uid, [('parent_id', '=', ids[0])], context=context)     
                if ev_sr:
                    for ev_id in ev_sr:
                        ev_br1 = self.pool.get('event.event').browse(cr, uid, ev_id, context=context)
                        if ev_br1.registration_ids:
                            raise osv.except_osv(_('Error!'),
                            _('User already registered for this recurrent events.'))  
                    self.pool.get('event.event').unlink(cr, uid, ev_sr, context=context)
                ev_br = self.browse(cr, uid, ids[0], context=context)
                if ev_br.parent_id:
                    raise osv.except_osv(_('Error!'),
                        _('This event is related to other event. Please choose recurrent boolean should be seleted'))
                
        else:            
            if 'interval' in vals.keys() or 'rrule_type' in vals.keys() or 'end_type' in vals.keys() or 'count' in vals.keys() or \
            'final_date' in vals.keys() or 'month_by' in vals.keys() or 'day' in vals.keys() or 'week_list' in vals.keys() or \
            'mo' in vals.keys() or 'tu' in vals.keys() or 'we' in vals.keys() or 'th' in vals.keys() or \
            'fr' in vals.keys() or 'sa' in vals.keys() or 'su' in vals.keys():
                if 'count' not in context.keys():
                    ctx = context
                    ctx['count'] = 0
                    ev_br = self.pool.get('event.event').browse(cr, uid, ids[0], context=context)
                    ev_sr = self.search(cr, uid, [('parent_id', '=', ev_br.id)], context=context)
                    if ev_sr:
                        for ev_id in ev_sr:
                            ev_br_cld = self.pool.get('event.event').browse(cr, uid, ev_id, context=context)
                            if ev_br_cld.registration_ids:
                                raise osv.except_osv(_('Error!'),
                                    _("You can not change the days! Because some user register for the related event '%s(%s)'!") % (ev_br_cld.name,ev_br_cld.date_begin.split(' ')[0]))
                        self.pool.get('event.event').unlink(cr, uid, ev_sr, context=context)
                    r_date = self.get_recurrent_date_by_event(cr, uid, ev_br, context=context)
                    count = 0
                    for d in r_date:
                        dt = datetime.strftime(d, DEFAULT_SERVER_DATETIME_FORMAT)
                        f_dt = self.modified_end_date(cr, uid, ev_br, dt, context=context)
                        if count == 0:              
                            self.pool.get('event.event').write(cr, uid, [ev_br.id], {'date_begin': dt, 'date_end':f_dt}, context=ctx)
                            count += 1
                        else:
                            id = self.pool.get('event.event').copy(cr, uid, ev_br.id, {'parent_id':ev_br.id, 'date_begin': dt, 'date_end':f_dt, 'event_ticket_ids':[], 'link_id':ev_br.id}, context=context)                    
                            self.pool.get('event.event').write(cr, uid, [ev_br.id], {'link_id': ev_br.id}, context=ctx)                    
            if 'recurrency' in vals.keys():                
                ev_sr = self.search(cr, uid, [('parent_id', '=', ids[0])], context=context)     
                if ev_sr:
                    for ev_id in ev_sr:
                        ev_br1 = self.pool.get('event.event').browse(cr, uid, ev_id, context=context)
                        if ev_br1.registration_ids:
                            raise osv.except_osv(_('Error!'),
                            _('User already registered for this recurrent events.'))  
                    self.pool.get('event.event').unlink(cr, uid, ev_sr, context=context)
                    
                ev_br = self.browse(cr, uid, ids[0], context=context)
                if ev_br.parent_id:
                    raise osv.except_osv(_('Error!'),
                        _('This event is related to other events. Please choose recurrent boolean should be seleted'))
            """if 'date_begin' in vals.keys() or 'date_end' in vals.keys():
                if 'count' not in context:
                    ev_br = self.browse(cr, uid, ids[0], context=context)
                    if ev_br.registration_ids:
                        raise osv.except_osv(_('Error!'),
                            _('User already registered for this event. Date change not allowed'))    """
            if 'count' not in context and 'state' not in vals.keys():                
                event_br = self.pool.get('event.event').browse(cr, uid, ids, context=context)[0]       
                if not event_br.parent_id:
                    vals_new = vals.copy()
                    event_ids = []
                    event_sr = self.search(cr, uid, [('parent_id', '=', event_br.id)], context=context)
                    if event_sr:                    
                        for event_id in event_sr:
                            event_ids.append(event_id)
                            if 'registration_ids' in vals_new:
                                vals_new.pop('registration_ids',False)
                            elif 'sponsor_ids' in vals_new:
                                vals_new.pop('sponsor_ids',False)
                            elif 'date_begin' in vals_new:
                                vals_new.pop('date_begin',False)
                                vals_new.pop('start',False)
                            elif 'date_end' in vals_new:
                                vals_new.pop('date_end',False)
                                vals_new.pop('stop',False)
                            elif 'event_ticket_ids' in vals_new:
                                vals_new.pop('event_ticket_ids',False)
                        #~ if event_ids:                            
                            #~ for ev_id in  event_ids:
                                #~ ct = 0
                                #~ if event_br.event_ticket_ids:                                                     
                                    #~ for t in event_br.event_ticket_ids:                                        
                                        #~ if ct == 0:
                                            #~ cld_ev_tkt_sr = self.pool.get('event.event.ticket').search(cr, uid, [('event_id','=',ev_id)], context=context)                                    
                                            #~ if cld_ev_tkt_sr:                                        
                                                #~ self.pool.get('event.event.ticket').unlink(cr, uid, cld_ev_tkt_sr, context=context)
                                                #~ ct += 1                                    
                                        #~ self.pool.get('event.event.ticket').copy(cr, uid, t.id,{'event_id':ev_id})
                    if event_ids:                        
                        self.write(cr, uid, event_ids, vals_new, context=context)
        
        return res or True and False      
    
    def compute_rule_string(self, data):
        """
        Compute rule string according to value type RECUR of iCalendar from the values given.
        @param self: the object pointer
        @param data: dictionary of freq and interval value
        @return: string containing recurring rule (empty if no rule)
        """
        if data['interval'] and data['interval'] < 0:
            raise osv.except_osv(_('warning!'), _('interval cannot be negative.'))
        if data['count'] and data['count'] <= 0:
            raise osv.except_osv(_('warning!'), _('count cannot be negative or 0.'))

        def get_week_string(freq, data):
            weekdays = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']
            if freq == 'weekly':
                byday = map(lambda x: x.upper(), filter(lambda x: data.get(x) and x in weekdays, data))
                if byday:
                    return ';BYDAY=' + ','.join(byday)
            return ''

        def get_month_string(freq, data):
            if freq == 'monthly':
                if data.get('month_by') == 'date' and (data.get('day') < 1 or data.get('day') > 31):
                    raise osv.except_osv(_('Error!'), ("Please select a proper day of the month."))

                if data.get('month_by') == 'day':  # Eg : Second Monday of the month
                    return ';BYDAY=' + data.get('byday') + data.get('week_list')
                elif data.get('month_by') == 'date':  # Eg : 16th of the month
                    return ';BYMONTHDAY=' + str(data.get('day'))
            return ''

        def get_end_date(data):
            if data.get('final_date'):
                data['end_date_new'] = ''.join((re.compile('\d')).findall(data.get('final_date'))) + 'T235959Z'

            return (data.get('end_type') == 'count' and (';COUNT=' + str(data.get('count'))) or '') +\
                ((data.get('end_date_new') and data.get('end_type') == 'end_date' and (';UNTIL=' + data.get('end_date_new'))) or '')

        freq = data.get('rrule_type', False)  # day/week/month/year
        res = ''
        if freq:
            interval_srting = data.get('interval') and (';INTERVAL=' + str(data.get('interval'))) or ''
            res = 'FREQ=' + freq.upper() + get_week_string(freq, data) + interval_srting + get_end_date(data) + get_month_string(freq, data)

        return res
    
    def _get_empty_rrule_data(self):
        return {
            'byday': False,
            'recurrency': False,
            'final_date': False,
            'rrule_type': False,
            'month_by': False,
            'interval': 0,
            'count': False,
            'end_type': False,
            'mo': False,
            'tu': False,
            'we': False,
            'th': False,
            'fr': False,
            'sa': False,
            'su': False,
            'day': False,
            'week_list': False
        }
    
    def _parse_rrule(self, rule, data, date_start):
        day_list = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']
        rrule_type = ['yearly', 'monthly', 'weekly', 'daily']
        r = rrule.rrulestr(rule, dtstart=datetime.strptime(date_start, DEFAULT_SERVER_DATETIME_FORMAT))

        if r._freq > 0 and r._freq < 4:
            data['rrule_type'] = rrule_type[r._freq]
        data['count'] = r._count
        data['interval'] = r._interval
        data['final_date'] = r._until and r._until.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        #repeat weekly
        if r._byweekday:
            for i in xrange(0, 7):
                if i in r._byweekday:
                    data[day_list[i]] = True
            data['rrule_type'] = 'weekly'
        #repeat monthly by nweekday ((weekday, weeknumber), )
        if r._bynweekday:
            data['week_list'] = day_list[r._bynweekday[0][0]].upper()
            data['byday'] = str(r._bynweekday[0][1])
            data['month_by'] = 'day'
            data['rrule_type'] = 'monthly'

        if r._bymonthday:
            data['day'] = r._bymonthday[0]
            data['month_by'] = 'date'
            data['rrule_type'] = 'monthly'

        #repeat yearly but for openerp it's monthly, take same information as monthly but interval is 12 times
        if r._bymonth:
            data['interval'] = data['interval'] * 12

        #FIXEME handle forever case
        #end of recurrence
        #in case of repeat for ever that we do not support right now
        if not (data.get('count') or data.get('final_date')):
            data['count'] = 100
        if data.get('count'):
            data['end_type'] = 'count'
        else:
            data['end_type'] = 'end_date'
        return data
    
    def open_link_lines(self,cr,uid,ids,context=None):
        if context is None:
            context = {}
        link_ids = []
        event_ids = self.pool.get('event.event').search(cr,uid,['|',('id','in',context.get('search_default_event_id',False)), ('link_id','=',context.get('default_link_id',False))])
        
        return {
            'type': 'ir.actions.act_window',
            'name': 'Recurrent Events',
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',            
            'domain' : [('id','in',event_ids)],
            'res_model': 'event.event',
            'nodestroy': True,
        }

    def update_one_ticket_ids(self, cr, uid, ids, context=None):
        cr.execute("SELECT id FROM event_event_ticket WHERE event_id = 447;")
        correct_tickets = cr.fetchall()
        print correct_tickets
        cr.execute("SELECT id FROM event_event_ticket WHERE event_id= 437;")
        wrong_tickets = cr.fetchall()
        print wrong_tickets
        for correct, wrong in zip(correct_tickets, wrong_tickets):
            print correct
            print wrong
            cr.execute("UPDATE event_registration SET event_ticket_id = %s WHERE "
                       "event_ticket_id = %s AND event_id = 447;" % (correct[0], wrong[0]))

    def update_recurrent_ticket_ids(self, cr, uid, ids, context=None):
        cr.execute("SELECT id FROM event_event WHERE parent_id IS NULL AND recurrency = 't';")
        parent_ids = cr.fetchall()
        for parent in parent_ids: # eg 437
            cr.execute("SELECT id FROM event_event WHERE parent_id = %s;" % parent[0])
            events_to_adjust = cr.fetchall()
            for event in events_to_adjust:  # eg 447
                cr.execute("SELECT id FROM event_event_ticket WHERE event_id=%s;" % event[0])
                correct_tickets = cr.fetchall()
                cr.execute("SELECT id FROM event_event_ticket WHERE event_id=%s;" % parent[0])
                wrong_tickets = cr.fetchall()
                for correct, wrong in zip(correct_tickets, wrong_tickets):
                    print "UPDATE event_registration SET event_ticket_id = %s " \
                          "WHERE event_ticket_id = %s AND event_id = %s;" % (correct[0], wrong[0], event[0])
                    cr.execute("UPDATE event_registration SET event_ticket_id = %s "
                               "WHERE event_ticket_id = %s AND event_id = %s;" % (correct[0], wrong[0], event[0]))

    
event_enhancements()

class event_event_ticket(osv.osv):
    _inherit = 'event.event.ticket'
    _description = 'Event Tickets'
    
    _columns = {        
        'seats_max' : fields.integer(string='Maximum Available Seats', oldname='register_max',
        help="You can for each event define a maximum registration level. If you have too much registrations you are not able to confirm your event. (put 0 to ignore this rule )"),         
        }
    _defaults = {        
        'seats_max': 1,
    }
    

class event_enhancements_registration(osv.osv):
    _inherit = 'event.registration'
    _description = 'Event Registration'
    
    _columns = {        
        'recurrent_check': fields.boolean('Recurrent', help="Recurrent"),
        'not_recurrent_check': fields.boolean('Not Recurrent', help="Recurrent"),
        'check': fields.boolean('check'),         
        }
    _defaults = {        
        'recurrent_check': False,        
        'not_recurrent_check': True,
        'check': True
    }
    
    
    def on_change_recurrent_check(self, cr, uid, ids, recurrent_check, context=None):              
        if recurrent_check:
            return {'value':{'recurrent_check':True,'not_recurrent_check':False}}            
    
    def on_change_not_recurrent_check(self, cr, uid, ids, not_recurrent_check, context=None):        
        if not_recurrent_check:
            return {'value':{'not_recurrent_check':True,'recurrent_check':False}}           
    
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}        
        event_obj = self.pool.get('event.event')    
        res = super(event_enhancements_registration, self).create(cr, uid, vals, context=context)
        if 'recurrent_check' in vals.keys() and 'not_recurrent_check' in vals.keys():
            if not vals['recurrent_check'] and not vals['not_recurrent_check']:
                raise osv.except_osv(_('Error!'),
                        _('If you want to register for all recurrent event, check the Recurrent Option.'))
            if vals['recurrent_check'] and vals['check']:
                event_br = event_obj.browse(cr, uid, vals['event_id'], context=context)
                ev_sr = event_obj.search(cr, uid, [('parent_id', '=', event_br.id)], context=context)
                if ev_sr:
                    for ev_id in ev_sr:
                        event_regis_br = self.pool.get('event.registration').browse(cr, uid, res, context=context)                        
                        vals_new = vals
                        if event_regis_br.partner_id:
                            vals_new['mobile'] = event_regis_br.partner_id.mobile
                            vals_new['email'] = event_regis_br.partner_id.email                            
                        #vals_new['not_recurrent_check'] = True
                        vals_new['event_id'] = ev_id
                        vals_new['check'] = False                        
                        #~ vals_new['event_ticket_id'] = None       
                        parent_ticket_name= self.pool.get('event.event.ticket').browse(cr, uid, vals['event_ticket_id'], context=context).name
                        event_br = event_obj.browse(cr, uid, ev_id, context=context)            
                        ticket_sr = self.pool.get('event.event.ticket').search(cr, uid, [('event_id','=',event_br.id)], context=context)
                        if ticket_sr:
                            for t in ticket_sr:
                                ticket_name = self.pool.get('event.event.ticket').browse(cr, uid, t, context=context).name
                                if ticket_name == parent_ticket_name:                
                                    vals_new['event_ticket_id'] = t
                                    break
                                else:              
                                    vals_new['event_ticket_id'] = None
                        else:
                            vals_new['event_ticket_id'] = None                 
                        id = self.pool.get('event.registration').create(cr, uid, vals_new, context=context)
                else:
                    if event_br.parent_id:
                        ev_sr = event_obj.search(cr, uid, [('parent_id', '=', event_br.parent_id.id),('date_begin', '>', event_br.date_begin)], context=context)
                        if ev_sr:
                            for ev_id in ev_sr:
                                vals_new = vals
                                if 'partner_id' in vals and vals['partner_id']:
                                    partner_br = self.pool.get('res.partner').browse(cr, uid, vals['partner_id'])
                                    vals_new['mobile'] = partner_br.mobile
                                    vals_new['email'] = partner_br.email                                    
                                #vals_new['not_recurrent_check'] = True
                                vals_new['event_id'] = ev_id
                                vals_new['check'] = False
                                #~ vals_new['event_ticket_id'] = None            
                                parent_ticket_name= self.pool.get('event.event.ticket').browse(cr, uid, vals['event_ticket_id'], context=context).name
                                event_br = event_obj.browse(cr, uid, ev_id, context=context)            
                                ticket_sr = self.pool.get('event.event.ticket').search(cr, uid, [('event_id','=',event_br.id)], context=context)
                                if ticket_sr:
                                    for t in ticket_sr:
                                        ticket_name = self.pool.get('event.event.ticket').browse(cr, uid, t, context=context).name
                                        if ticket_name == parent_ticket_name:                
                                            vals_new['event_ticket_id'] = t
                                            break
                                        else:              
                                            vals_new['event_ticket_id'] = None
                                else:
                                    vals_new['event_ticket_id'] = None                    
                                id = self.pool.get('event.registration').create(cr, uid, vals_new, context=context) 
        return res
        
        
    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}        
        event_obj = self.pool.get('event.event')        
        if 'event_ticket_id' in vals and vals['event_ticket_id']:
            vals_new = vals
            current_ticket_name= self.pool.get('event.event.ticket').browse(cr, uid, vals['event_ticket_id'], context=context).name
            event_res_br = self.pool.get('event.registration').browse(cr, uid, ids, context=context)            
            ticket_sr = self.pool.get('event.event.ticket').search(cr, uid, [('event_id','=',event_res_br.event_id.id)], context=context)
            if ticket_sr:
                for t in ticket_sr:
                    ticket_name = self.pool.get('event.event.ticket').browse(cr, uid, t, context=context).name
                    if ticket_name == current_ticket_name:                      
                        vals_new['event_ticket_id'] = t            
            return super(event_enhancements_registration, self).write(cr, uid, ids, vals_new, context=context)  
        return super(event_enhancements_registration, self).write(cr, uid, ids, vals, context=context)    
        
    def mailsend(self, message, subject, host, port, from_addr, pwd, to_addr):      
        themsg = MIMEMultipart()
        themsg['Subject'] = subject
        themsg['To'] = to_addr
        themsg['From'] = from_addr
        themsg.preamble = 'I am not using a MIME-aware mail reader.\n'
                
        #themsg.attach(MIMEText(msg, 'plain'))
        themsg.attach(MIMEText(message, 'html'))
        themsg = themsg.as_string()        
        # send the message
        smtp_host_post = str(host) + ':' + str(port)
        try:
            smtp = smtplib.SMTP(smtp_host_post)
            smtp.starttls()
            user = tools.ustr(from_addr).encode('utf-8')
            password = tools.ustr(pwd).encode('utf-8')
            try:
                smtp.login(user, password)
                smtp.sendmail(from_addr, to_addr, themsg)
            except Exception, e:
                log_msg = _("Connection Failed!  Here is what we got instead: %s") % tools.ustr(e)
                _logger.warning(log_msg)
            finally:
                try:
                    # Close Connection of SMTP Server
                    smtp.quit()
                except Exception:
                    # ignored, just a consequence of the previous exception
                    pass
        except Exception, e:
            log_msg = _("Mail delivery failed via SMTP server '%s'.%s: %s") % (tools.ustr(host),
                                                                     e.__class__.__name__,
                                                                     tools.ustr(e))
            _logger.warning(log_msg)
        #smtp.close()
        return True
    
    def mail_process(self, cr, uid, record_name, events, events_registration, to_email, email_registration_id=None, email_confirmation_id=None):
        ir_mail_server = self.pool.get('ir.mail_server')
        mail_server_ids = ir_mail_server.search(cr, uid, [], order = 'sequence', limit = 1)                                
        if mail_server_ids:
            mail_server = ir_mail_server.browse(cr, uid, mail_server_ids[0])
            if events_registration and email_registration_id:
                template_br = self.pool.get('email.template').browse(cr, uid, email_registration_id)
                body_email = template_br.body_html            
                body_email = string.replace(body_email, 'Hello ${object.name}', 'Hello '+ str(record_name))
                body_email = string.replace(body_email, 'event ${object.event_id.name}', 'events ' + '<br> '+'<br>'.join(events_registration) + '<br>')       
                body_email = string.replace(body_email, 'event is', 'events are ')
                #msg = "<p>Hello "+ record_name +",<p> <p>We confirm that your registration to the event <br> %s has been recorded.  You will automatically receive an email providing you more practical information (such as the schedule, the agenda...) as soon as the event confirmed.</p> <p> Thank you for your participation!</p> <p> Best regards</p>"  % '<br>'.join(events)                                                                  
                msg = body_email
                subject = 'Recurrent Event Registration - Confirmation'       
            if events and email_confirmation_id:
                template_br = self.pool.get('email.template').browse(cr, uid, email_confirmation_id)
                body_email = template_br.body_html            
                body_email = string.replace(body_email, 'Hello ${object.name}', 'Hello '+ str(record_name))
                body_email = string.replace(body_email, 'event ${object.event_id.name}', 'events')              
                body_email = string.replace(body_email, "${object.event_id.date_begin_located.strftime('%Y-%m-%d %H:%M:%S (%Z)')} to ${object.event_id.date_end_located.strftime('%Y-%m-%d %H:%M:%S (%Z)')}", '<br> '+'<br>'.join(events) + '<br>')
                #msg = "<p>Hello "+ record_name +",<p> <p>We confirm that your registration to the event <br> %s has been recorded.  You will automatically receive an email providing you more practical information (such as the schedule, the agenda...) as soon as the event confirmed.</p> <p> Thank you for your participation!</p> <p> Best regards</p>"  % '<br>'.join(events)                                                                  
                msg = body_email
                subject = 'Recurrent Event - Confirmation'      
            try:
                self.mailsend(msg, subject, host = mail_server.smtp_host, port = mail_server.smtp_port, from_addr = mail_server.smtp_user, pwd = mail_server.smtp_pass, to_addr = to_email)  #call to send mail 
            except Exception, e:
                _logger.warning(e)
    
    @api.cr_uid_id_context
    def recurrent_mail(self, cr, uid, template_id, res_id, from_event=False, context=None):
        if context is None:
            context = {}
        events = []
        events_registration = []
        email = ''
        event_obj = self.pool.get('event.event')
        registration_br = self.browse(cr, uid, template_id)
        if not from_event:
            events_registration.append(registration_br.event_id.name + '(' + registration_br.event_id.date_begin + ')')
        else:
            t_date_begin = datetime.strptime(registration_br.event_id.date_begin_located, '%Y-%m-%d %H:%M:%S')
            t_date_end = datetime.strptime(registration_br.event_id.date_end_located, '%Y-%m-%d %H:%M:%S')
            s_date_begin = t_date_begin.strftime('%Y-%m-%d %H:%M:%S %Z')
            s_date_end = t_date_end.strftime('%Y-%m-%d %H:%M:%S %Z')
            events.append(registration_br.event_id.name + ': (' + s_date_begin + ' to ' + s_date_end + ')')
        if registration_br.event_id:
            if registration_br.event_id.recurrency:
                event_sr = event_obj.search(cr, uid, [('parent_id','=',registration_br.event_id.id)])
                if event_sr:
                    for e in event_sr:
                        event_br = event_obj.browse(cr, uid, e)
                        for r in event_br.registration_ids:
                            if r.email == registration_br.email:
                                if not from_event and registration_br.event_id.email_registration_id:
                                    events_registration.append(r.event_id.name + '(' + r.event_id.date_begin + ')')
                                if from_event and registration_br.event_id.email_confirmation_id:
                                    t_date_begin = datetime.strptime(r.event_id.date_begin_located, '%Y-%m-%d %H:%M:%S')
                                    t_date_end = datetime.strptime(r.event_id.date_end_located, '%Y-%m-%d %H:%M:%S')
                                    s_date_begin = t_date_begin.strftime('%Y-%m-%d %H:%M:%S %Z')
                                    s_date_end = t_date_end.strftime('%Y-%m-%d %H:%M:%S %Z')
                                    events.append(r.event_id.name + ': (' + s_date_begin + ' to ' +s_date_end + ')')
        if registration_br.partner_id:
            if registration_br.email:
                email = registration_br.email
            else:
                partner_br = self.pool.get('res.partner').browse(cr, uid, registration_br.partner_id.id, context=context)                                    
                email = partner_br.email
        else:
            email = registration_br.email                           
        if events_registration and registration_br.event_id.email_registration_id and not from_event:
            if registration_br.event_id.email_process == True:             
                self.mail_process(cr, uid, record_name=registration_br.name, events=[], events_registration=events_registration, to_email=email, email_registration_id=registration_br.event_id.email_registration_id.id, email_confirmation_id=None)
        else:
            if events and registration_br.event_id.email_confirmation_id and from_event:
                events[-1] = events[-1] + '.'
                if registration_br.event_id.email_process == True:
                    self.mail_process(cr, uid, record_name=registration_br.name, events=events, events_registration=[], to_email=email, email_registration_id=None, email_confirmation_id=registration_br.event_id.email_confirmation_id.id)
        return True

    @api.one
    def mail_user(self):
        """Send email to user with email_template when registration is done """
        if self.event_id.email_process == True:
            if self.event_id.state == 'confirm' and self.event_id.email_confirmation_id:
                self.mail_user_confirm()
            else:
                template = self.event_id.email_registration_id
                if template:
                    if not self.recurrent_check:
                        mail_message = template.send_mail(self.id)
                    else:
                        self.recurrent_mail(self.id, from_event = False)
                    
    @api.one
    def mail_user_confirm(self):
        """Send email to user when the event is confirmed """
        if self.event_id.email_process == True:
            template = self.event_id.email_confirmation_id
            if template:
                if not self.event_id.recurrency:
                    mail_message = template.send_mail(self.id)
                else:
                    self.recurrent_mail(self.id, from_event = True)
event_enhancements_registration()

class event_ticket(osv.osv):
    _inherit = 'event.event.ticket'
    _columns = {
        'product_id': fields.many2one('product.product', 'Product', required=True, domain=[("event_type_id", "!=", False)]),
        'publish': fields.boolean(string="Publish on website?"), 
        }
        
    def create(self, cr, uid, vals, context=None):        
        if context is None:
            context = {}        
        res = super(event_ticket, self).create(cr, uid, vals, context=context)
        ticket_br = self.browse(cr, uid, res, context=context)
        if ticket_br.event_id.recurrency:
            event_obj = self.pool.get('event.event')
            event_sr = event_obj.search(cr, uid, [('parent_id','=',ticket_br.event_id.id)])
            if event_sr:
                for e in event_sr:
                    vals_new = vals.copy()
                    vals_new['event_id'] = e
                    self.pool.get('event.event.ticket').create(cr, uid, vals_new)
        return res
    
    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if self.pool.get('res.users').has_group(cr, uid, 'base.group_event_manager'):        
            event_obj = self.pool.get('event.event')
            ticket_br = self.browse(cr, uid, ids, context=context)
            if ticket_br.event_id.recurrency:
                event_obj = self.pool.get('event.event')
                event_sr = event_obj.search(cr, uid, [('parent_id','=',ticket_br.event_id.id)])
                if event_sr:
                    for e in event_sr:
                        event_br = event_obj.browse(cr, uid, e)
                        if event_br.event_ticket_ids:
                            for t in event_br.event_ticket_ids:
                                if t.product_id.id == ticket_br.product_id.id:
                                    vals_new = vals.copy()
                                    vals_new['event_id'] = e
                                    self.pool.get('event.event.ticket').write(cr, uid, t.id, vals_new)
        return super(event_ticket, self).write(cr, uid, ids, vals, context=context) 
event_ticket()


class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    
    def button_confirm(self, cr, uid, ids, context=None):
        '''
        create registration with sales order
        '''        
        
        context = dict(context or {})
        registration_obj = self.pool.get('event.registration')        
        for order_line in self.browse(cr, uid, ids, context=context):
            if order_line.event_id:
                if order_line.order_id.registration_contact_ids:                    
                    for contact_line in order_line.order_id.registration_contact_ids:
                        dic = {
                        'name': contact_line.partner_id.name,
                        'partner_id': contact_line.partner_id.id,
                        'nb_register': int(1),
                        'email': contact_line.partner_id.email,
                        'phone': contact_line.partner_id.phone,
                        'origin': order_line.order_id.name,
                        'event_id': order_line.event_id.id,
                        'event_ticket_id': order_line.event_ticket_id and order_line.event_ticket_id.id or None,
                        }
                        if order_line.event_ticket_id:
                            message = _("The registration has been created for event <i>%s</i> with the ticket <i>%s</i> from the Sale Order %s. ") % (order_line.event_id.name, order_line.event_ticket_id.name, order_line.order_id.name)
                        else:
                            message = _("The registration has been created for event <i>%s</i> from the Sale Order %s. ") % (order_line.event_id.name, order_line.order_id.name)
                    
                        context.update({'mail_create_nolog': True})
                        registration_id = registration_obj.create(cr, uid, dic, context=context)                              

                else:                    
                    dic = {
                        'name': order_line.order_id.partner_invoice_id.name,
                        'partner_id': order_line.order_id.partner_id.id,
                        'nb_register': int(order_line.product_uom_qty),
                        'email': order_line.order_id.partner_id.email,
                        'phone': order_line.order_id.partner_id.phone,
                        'origin': order_line.order_id.name,
                        'event_id': order_line.event_id.id,
                        'event_ticket_id': order_line.event_ticket_id and order_line.event_ticket_id.id or None,
                    }

                    if order_line.event_ticket_id:
                        message = _("The registration has been created for event <i>%s</i> with the ticket <i>%s</i> from the Sale Order %s. ") % (order_line.event_id.name, order_line.event_ticket_id.name, order_line.order_id.name)
                    else:
                        message = _("The registration has been created for event <i>%s</i> from the Sale Order %s. ") % (order_line.event_id.name, order_line.order_id.name)
                    
                    context.update({'mail_create_nolog': True})
                    registration_id = registration_obj.create(cr, uid, dic, context=context)                              
                if order_line.event_id.email_process == True:
                    registration_obj.message_post(cr, uid, [registration_id], body=message, context=context)
        return self.write(cr, uid, ids, {'state': 'confirmed'})
        
sale_order_line()


class sale_order(osv.osv):
    _inherit = 'sale.order'
    
    _columns = {
        'registration_contact_ids': fields.one2many('order.event.registration.contact', 'sale_id', 'Registration Contacts'),
        }
sale_order()


class order_event_registration_contact(osv.osv):
    _name = 'order.event.registration.contact'
    
    _columns = {
        'sale_id': fields.many2one('sale.order', 'Sale Ref'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        }
order_event_registration_contact()
