# -*- coding: utf-8 -*-

{
    'name': "Website Sale Event",
    'author' : 'Shiva & Ayan',
    'website' : 'http://www.odoo.com',
    'license' : 'AGPL-3',
    'category': 'web',
    'summary': "website event sale",    
    'version': '1.0',
    'description': """
        """,
    'depends': ['web','website_sale','payment','point_of_sale'],
    'data': [
         
        'res_partner.xml',
        'template.xml'
        
    ],
    #~ 'qweb': ['static/src/xml/pos.xml'],
    'installable': True,
    'auto_install': True
}
