# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

import werkzeug.urls
from werkzeug.exceptions import NotFound

from openerp import http
from openerp import tools
from openerp.http import request
from openerp.addons.website.models.website import slug


from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
from openerp.addons.website_event_sale.controllers.main import website_event
from openerp.addons.website_sale.controllers.main import get_pricelist
from openerp.addons.website_sale.controllers.main import website_sale
from openerp.addons.website_sale.controllers.main import QueryURL
from openerp.addons.website_sale.controllers.main import table_compute
from openerp.addons.website_sale.controllers.main import PPG, PPR

from openerp.tools.translate import _
from openerp.addons.auth_signup.controllers.main import AuthSignupHome
from openerp.addons.auth_signup.res_users import SignupError
import openerp

import logging
import pprint

_logger = logging.getLogger(__name__)

#~ class WalletController(http.Controller):
    #~ _accept_url = '/payment/wallet/feedback'
    #~ _return_url = '/shop/payment/validate'
#~ 
    #~ @http.route([
        #~ '/payment/wallet/feedback',
    #~ ], type='http', auth='none')
    #~ def wallet_form_feedback(self, **post):
        #~ cr, uid, context = request.cr, SUPERUSER_ID, request.context
        #~ _logger.info('Beginning form_feedback with post data %s', pprint.pformat(post))  # debug
        #~ request.registry['payment.transaction'].form_feedback(cr, uid, post, 'wallet', context)
        #~ post.pop('return_url', '/')
        #~ return werkzeug.utils.redirect('/shop/payment/validate')

class AuthSignupHome(AuthSignupHome):

    @http.route('/web/signup', type='http', auth='public', website=True)
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()
        res_country = request.registry.get('res.country')
        country_ids = res_country.search(request.cr, openerp.SUPERUSER_ID, [])
        country_names = []
        for country_id in country_ids:
            country_names.append(res_country.browse(request.cr, openerp.SUPERUSER_ID, country_id).name)
        qcontext['country_names'] = country_names
        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                self.do_signup(qcontext)
                return super(AuthSignupHome, self).web_login(*args, **kw)
            except (SignupError, AssertionError), e:
                qcontext['error'] = _(e.message)
        return request.render('auth_signup.signup', qcontext)

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = dict((key, qcontext.get(key)) for key in ('login', 'name', 'password', 'parent_id', 'mail_id', 'tag', 'country_id'))
        assert any([k for k in values.values()]), "The form was not properly filled in."
        assert values.get('password') == qcontext.get('confirm_password'), "Passwords do not match; please retype them."
        self._signup_with_values(qcontext.get('token'), values)
        request.cr.commit()
        
#~ class website_sale(website_sale):
    #~ 
    #~ @http.route(['/shop/payment/transaction/<int:acquirer_id>'], type='json', auth="public", website=True)
    #~ def payment_transaction(self, acquirer_id):
        #~ """ Json method that creates a payment.transaction, used to create a
        #~ transaction when the user clicks on 'pay now' button. After having
        #~ created the transaction, the event continues and the user is redirected
        #~ to the acquirer website.
#~ 
        #~ :param int acquirer_id: id of a payment.acquirer record. If not set the
                                #~ user is redirected to the checkout page
        #~ """
        #~ cr, uid, context = request.cr, request.uid, request.context
        #~ transaction_obj = request.registry.get('payment.transaction')
        #~ order = request.website.sale_get_order(context=context)
#~ 
        #~ if not order or not order.order_line or acquirer_id is None:
            #~ return request.redirect("/shop/checkout")
#~ 
        #~ assert order.partner_id.id != request.website.partner_id.id
        #~ 
        #~ wa = request.website.modify_wallet_amount(acquirer_id=acquirer_id)
        #~ wa1 = request.website.wallet_recharge_alert(acquirer_id=acquirer_id)
        #~ # find an already existing transaction
        #~ tx = request.website.sale_get_transaction()
        #~ if tx:
            #~ tx_id = tx.id
            #~ if tx.reference != order.name:
                #~ tx = False
                #~ tx_id = False
            #~ elif tx.state == 'draft':  # button cliked but no more info -> rewrite on tx or create a new one ?
                #~ tx.write({
                    #~ 'acquirer_id': acquirer_id,
                    #~ 'amount': order.amount_total,
                #~ })
        #~ if not tx:
            #~ tx_id = transaction_obj.create(cr, SUPERUSER_ID, {
                #~ 'acquirer_id': acquirer_id,
                #~ 'type': 'form',
                #~ 'amount': order.amount_total,
                #~ 'currency_id': order.pricelist_id.currency_id.id,
                #~ 'partner_id': order.partner_id.id,
                #~ 'partner_country_id': order.partner_id.country_id.id,
                #~ 'reference': order.name,
                #~ 'sale_order_id': order.id,
            #~ }, context=context)
            #~ request.session['sale_transaction_id'] = tx_id                        
#~ 
        #~ # update quotation
        #~ request.registry['sale.order'].write(
            #~ cr, SUPERUSER_ID, [order.id], {
                #~ 'payment_acquirer_id': acquirer_id,
                #~ 'payment_tx_id': request.session['sale_transaction_id']
            #~ }, context=context)
#~ 
        #~ return tx_id
#~ 
#~ 
    #~ @http.route([
        #~ '/shop',
        #~ '/shop/page/<int:page>',
        #~ '/shop/category/<model("product.public.category"):category>',
        #~ '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    #~ ], type='http', auth="public", website=True)
    #~ def shop(self, page=0, category=None, search='', **post):
        #~ cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
#~ 
        #~ attrib_list = request.httprequest.args.getlist('attrib')
        #~ attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        #~ attrib_set = set([v[1] for v in attrib_values])
#~ 
        #~ domain = self._get_search_domain(search, category, attrib_values)
#~ 
        #~ keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list)
#~ 
        #~ if not context.get('pricelist'):
            #~ pricelist = self.get_pricelist()
            #~ context['pricelist'] = int(pricelist)
        #~ else:
            #~ pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)
#~ 
        #~ product_obj = pool.get('product.template')
#~ 
        #~ url = "/shop"
        #~ product_count = product_obj.search_count(cr, uid, domain, context=context)
        #~ if search:
            #~ post["search"] = search
        #~ if category:
            #~ category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
            #~ url = "/shop/category/%s" % slug(category)
        #~ if attrib_list:
            #~ post['attrib'] = attrib_list
        #~ pager = request.website.pager(url=url, total=product_count, page=page, step=PPG, scope=7, url_args=post)
        #~ product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        #~ products = product_obj.browse(cr, uid, product_ids, context=context)
#~ 
        #~ style_obj = pool['product.style']
        #~ style_ids = style_obj.search(cr, uid, [], context=context)
        #~ styles = style_obj.browse(cr, uid, style_ids, context=context)
#~ 
        #~ category_obj = pool['product.public.category']
        #~ category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        #~ categs = category_obj.browse(cr, uid, category_ids, context=context)
#~ 
        #~ attributes_obj = request.registry['product.attribute']
        #~ attributes_ids = attributes_obj.search(cr, uid, [], context=context)
        #~ attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)
#~ 
        #~ from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        #~ to_currency = pricelist.currency_id
        #~ compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)
#~ 
        #~ values = {
            #~ 'search': search,
            #~ 'category': category,
            #~ 'attrib_values': attrib_values,
            #~ 'attrib_set': attrib_set,
            #~ 'pager': pager,
            #~ 'pricelist': pricelist,
            #~ 'products': products,
            #~ 'bins': table_compute().process(products),
            #~ 'rows': PPR,
            #~ 'styles': styles,
            #~ 'categories': categs,
            #~ 'attributes': attributes,
            #~ 'compute_currency': compute_currency,
            #~ 'keep': keep,
            #~ 'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            #~ 'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib',i) for i in attribs]),
        #~ }
        #~ return request.website.render("website_sale.products", values)
#~ 
            #~ 
    #~ @http.route(['/shop/payment'], type='http', auth="user", website=True)
    #~ def payment(self, **post):
        #~ """ Payment step. This page proposes several payment means based on available
        #~ payment.acquirer. State at this point :
#~ 
         #~ - a draft sale order with lines; otherwise, clean context / session and
           #~ back to the shop
         #~ - no transaction in context / session, or only a draft one, if the customer
           #~ did go to a payment.acquirer website but closed the tab without
           #~ paying / canceling
        #~ """
        #~ 
        #~ cr, uid, context = request.cr, request.uid, request.context
        #~ payment_obj = request.registry.get('payment.acquirer')
        #~ sale_order_obj = request.registry.get('sale.order')
#~ 
        #~ order = request.website.sale_get_order(context=context)
#~ 
        #~ redirection = self.checkout_redirection(order)
        #~ if redirection:
            #~ return redirection
#~ 
        #~ shipping_partner_id = False
        #~ if order:
            #~ if order.partner_shipping_id.id:
                #~ shipping_partner_id = order.partner_shipping_id.id
            #~ else:
                #~ shipping_partner_id = order.partner_invoice_id.id
#~ 
        #~ values = {
            #~ 'order': request.registry['sale.order'].browse(cr, SUPERUSER_ID, order.id, context=context)
        #~ }
        #~ values['errors'] = sale_order_obj._get_errors(cr, uid, order, context=context)
        #~ values.update(sale_order_obj._get_website_data(cr, uid, order, context))
#~ 
        #~ # fetch all registered payment means
        #~ # if tx:
        #~ #     acquirer_ids = [tx.acquirer_id.id]
        #~ # else:
        #~ if not values['errors']:
            #~ acquirer_ids = payment_obj.search(cr, SUPERUSER_ID, [('website_published', '=', True), ('company_id', '=', order.company_id.id)], context=context)
            #~ values['acquirers'] = list(payment_obj.browse(cr, uid, acquirer_ids, context=context))
            #~ render_ctx = dict(context, submit_class='btn btn-primary', submit_txt=_('Pay Now'))
            #~ for acquirer in values['acquirers']:
                #~ acquirer.button = payment_obj.render(
                    #~ cr, SUPERUSER_ID, acquirer.id,
                    #~ order.name,
                    #~ order.amount_total,
                    #~ order.pricelist_id.currency_id.id,
                    #~ partner_id=shipping_partner_id,
                    #~ tx_values={
                        #~ 'return_url': '/shop/payment/validate',
                    #~ },
                    #~ context=render_ctx)
        #~ return request.website.render("website_sale.payment", values)
        #~ 
    #~ @http.route(['/shop/cart'], type='http', auth="user", website=True)
    #~ def cart(self, **post):
        #~ cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        #~ order = request.website.sale_get_order()
        #~ if order:
            #~ from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
            #~ to_currency = order.pricelist_id.currency_id
            #~ compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)
        #~ else:
            #~ compute_currency = lambda price: price
#~ 
        #~ values = {
            #~ 'order': order,
            #~ 'compute_currency': compute_currency,
            #~ 'suggested_products': [],
        #~ }
        #~ if order:
            #~ _order = order
            #~ if not context.get('pricelist'):
                #~ _order = order.with_context(pricelist=order.pricelist_id.id)
            #~ values['suggested_products'] = _order._cart_accessories()
#~ 
        #~ return request.website.render("website_sale.cart", values)
    #~ 
    #~ @http.route(['/shop/checkout'], type='http', auth="user", website=True)
    #~ def checkout(self, **post):
        #~ cr, uid, context = request.cr, request.uid, request.context
#~ 
        #~ order = request.website.sale_get_order(force_create=1, context=context)
#~ 
        #~ redirection = self.checkout_redirection(order)
        #~ if redirection:
            #~ return redirection
#~ 
        #~ values = self.checkout_values()
#~ 
        #~ return request.website.render("website_sale.checkout", values)
#~ 
    #~ @http.route(['/shop/confirm_order'], type='http', auth="user", website=True)
    #~ def confirm_order(self, **post):
        #~ cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
#~ 
        #~ order = request.website.sale_get_order(context=context)
        #~ if not order:
            #~ return request.redirect("/shop")
#~ 
        #~ redirection = self.checkout_redirection(order)
        #~ if redirection:
            #~ return redirection
#~ 
        #~ values = self.checkout_values(post)
#~ 
        #~ values["error"] = self.checkout_form_validate(values["checkout"])
        #~ if values["error"]:
            #~ return request.website.render("website_sale.checkout", values)
#~ 
        #~ self.checkout_form_save(values["checkout"])
#~ 
        #~ request.session['sale_last_order_id'] = order.id
#~ 
        #~ request.website.sale_get_order(update_pricelist=True, context=context)
#~ 
        #~ return request.redirect("/shop/payment")
    #~ 
    #~ @http.route(['/shop/confirmation'], type='http', auth="public", website=True)
    #~ def payment_confirmation(self, **post):
        #~ """ End of checkout process controller. Confirmation is basically seing
        #~ the status of a sale.order. State at this point :
#~ 
         #~ - should not have any context / session info: clean them
         #~ - take a sale.order id, because we request a sale.order and are not
           #~ session dependant anymore
        #~ """
        #~ cr, uid, context = request.cr, request.uid, request.context
#~ 
        #~ sale_order_id = request.session.get('sale_last_order_id')
        #~ if sale_order_id:
            #~ order = request.registry['sale.order'].browse(cr, SUPERUSER_ID, sale_order_id, context=context)
            #~ if order.payment_acquirer_id.provider == 'wallet':
                #~ request.registry['res.partner'].write(cr, SUPERUSER_ID, [order.partner_id.id], {'wallet_credit_amt':order.partner_id.wallet_credit_amt - order.sale_total_amount})
            #~ 
        #~ else:
            #~ return request.redirect('/shop')
#~ 
        #~ return request.website.render("website_sale.confirmation", {'order': order})
#~ 
    #~ @http.route(['/shop/cart/update'], type='http', auth="user", methods=['POST'], website=True)
    #~ def cart_update(self, product_id, add_qty=1, set_qty=0, **kw):
        #~ cr, uid, context = request.cr, request.uid, request.context
        #~ request.website.sale_get_order(force_create=1)._cart_update(product_id=int(product_id), add_qty=float(add_qty), set_qty=float(set_qty))
        #~ return request.redirect("/shop/cart")
#~ 
    #~ @http.route(['/shop/cart/update_json'], type='json', auth="user", methods=['POST'], website=True)
    #~ def cart_update_json(self, product_id, line_id, add_qty=None, set_qty=None, display=True):
        #~ order = request.website.sale_get_order(force_create=1)
        #~ if order.state != 'draft':
            #~ request.website.sale_reset()
            #~ return {}
#~ 
        #~ value = order._cart_update(product_id=product_id, line_id=line_id, add_qty=add_qty, set_qty=set_qty)
        #~ if not order.cart_quantity:
            #~ request.website.sale_reset()
            #~ return {}
        #~ if not display:
            #~ return None
        #~ value['cart_quantity'] = order.cart_quantity
        #~ value['website_sale.total'] = request.website._render("website_sale.total", {
                #~ 'website_sale_order': request.website.sale_get_order()
            #~ })
        #~ return value
    #~ #------------------------------------------------------
    #~ # Checkout
    #~ #------------------------------------------------------
#~ 
    #~ def checkout_redirection(self, order):
        #~ cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
#~ 
        #~ # must have a draft sale order with lines at this point, otherwise reset
        #~ if not order or order.state != 'draft':
            #~ request.session['sale_order_id'] = None
            #~ request.session['sale_transaction_id'] = None
            #~ return request.redirect('/shop')
#~ 
        #~ # if transaction pending / done: redirect to confirmation
        #~ tx = context.get('website_sale_transaction')
        #~ if tx and tx.state != 'draft':
            #~ return request.redirect('/shop/payment/confirmation/%s' % order.id)
#~ 
    #~ def checkout_values(self, data=None):
        #~ cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        #~ orm_partner = registry.get('res.partner')
        #~ orm_user = registry.get('res.users')
        #~ orm_country = registry.get('res.country')
        #~ state_orm = registry.get('res.country.state')
#~ 
        #~ country_ids = orm_country.search(cr, SUPERUSER_ID, [], context=context)
        #~ countries = orm_country.browse(cr, SUPERUSER_ID, country_ids, context)
        #~ states_ids = state_orm.search(cr, SUPERUSER_ID, [], context=context)
        #~ states = state_orm.browse(cr, SUPERUSER_ID, states_ids, context)
        #~ partner = orm_user.browse(cr, SUPERUSER_ID, request.uid, context).partner_id
#~ 
        #~ order = None
#~ 
        #~ shipping_id = None
        #~ shipping_ids = []
        #~ checkout = {}
        #~ if not data:
            #~ if request.uid != request.website.user_id.id:
                #~ checkout.update( self.checkout_parse("billing", partner) )
                #~ shipping_ids = orm_partner.search(cr, SUPERUSER_ID, [("parent_id", "=", partner.id), ('type', "=", 'delivery')], context=context)
            #~ else:
                #~ order = request.website.sale_get_order(force_create=1, context=context)
                #~ if order.partner_id:
                    #~ domain = [("partner_id", "=", order.partner_id.id)]
                    #~ user_ids = request.registry['res.users'].search(cr, SUPERUSER_ID, domain, context=dict(context or {}, active_test=False))
                    #~ if not user_ids or request.website.user_id.id not in user_ids:
                        #~ checkout.update( self.checkout_parse("billing", order.partner_id) )
        #~ else:
            #~ checkout = self.checkout_parse('billing', data)
            #~ try: 
                #~ shipping_id = int(data["shipping_id"])
            #~ except ValueError:
                #~ pass
            #~ if shipping_id == -1:
                #~ checkout.update(self.checkout_parse('shipping', data))
#~ 
        #~ if shipping_id is None:
            #~ if not order:
                #~ order = request.website.sale_get_order(context=context)
            #~ if order and order.partner_shipping_id:
                #~ shipping_id = order.partner_shipping_id.id
#~ 
        #~ shipping_ids = list(set(shipping_ids) - set([partner.id]))
#~ 
        #~ if shipping_id == partner.id:
            #~ shipping_id = 0
        #~ elif shipping_id > 0 and shipping_id not in shipping_ids:
            #~ shipping_ids.append(shipping_id)
        #~ elif shipping_id is None and shipping_ids:
            #~ shipping_id = shipping_ids[0]
#~ 
        #~ ctx = dict(context, show_address=1)
        #~ shippings = []
        #~ if shipping_ids:
            #~ shippings = shipping_ids and orm_partner.browse(cr, SUPERUSER_ID, list(shipping_ids), ctx) or []
        #~ if shipping_id > 0:
            #~ shipping = orm_partner.browse(cr, SUPERUSER_ID, shipping_id, ctx)
            #~ checkout.update( self.checkout_parse("shipping", shipping) )
#~ 
        #~ checkout['shipping_id'] = shipping_id
#~ 
        #~ # Default search by user country
        #~ if not checkout.get('country_id'):
            #~ country_code = request.session['geoip'].get('country_code')
            #~ if country_code:
                #~ country_ids = request.registry.get('res.country').search(cr, uid, [('code', '=', country_code)], context=context)
                #~ if country_ids:
                    #~ checkout['country_id'] = country_ids[0]
#~ 
        #~ values = {
            #~ 'countries': countries,
            #~ 'states': states,
            #~ 'checkout': checkout,
            #~ 'shipping_id': partner.id != shipping_id and shipping_id or 0,
            #~ 'shippings': shippings,
            #~ 'error': {},
            #~ 'has_check_vat': hasattr(registry['res.partner'], 'check_vat')
        #~ }
#~ 
        #~ return values
#~ 
    #~ mandatory_billing_fields = ["name", "email", "mail_id", "street", "street2", "city", "country_id"]
    #~ optional_billing_fields = ["street", "state_id", "vat", "vat_subjected", "zip"]
    #~ mandatory_shipping_fields = ["name", "email", "street", "street2", "city", "country_id"]
    #~ optional_shipping_fields = ["state_id", "zip"]
#~ 
    #~ def _get_mandatory_billing_fields(self):
        #~ return self.mandatory_billing_fields
#~ 
    #~ def _get_optional_billing_fields(self):
        #~ return self.optional_billing_fields
#~ 
    #~ def _get_mandatory_shipping_fields(self):
        #~ return self.mandatory_shipping_fields
#~ 
    #~ def _get_optional_shipping_fields(self):
        #~ return self.optional_shipping_fields
#~ 
    #~ def _post_prepare_query(self, query, data, address_type):
        #~ return query
#~ 
    #~ def checkout_parse(self, address_type, data, remove_prefix=False):
        #~ """ data is a dict OR a partner browse record
        #~ """
        #~ # set mandatory and optional fields
        #~ assert address_type in ('billing', 'shipping')
        #~ if address_type == 'billing':
            #~ all_fields = self._get_mandatory_billing_fields() + self._get_optional_billing_fields()
            #~ prefix = ''
        #~ else:
            #~ all_fields = self._get_mandatory_shipping_fields() + self._get_optional_shipping_fields()
            #~ prefix = 'shipping_'
#~ 
        #~ # set data
        #~ if isinstance(data, dict):
            #~ query = dict((prefix + field_name, data[prefix + field_name])
                #~ for field_name in all_fields if prefix + field_name in data)
        #~ else:
            #~ query = dict((prefix + field_name, getattr(data, field_name))
                #~ for field_name in all_fields if getattr(data, field_name))
            #~ if address_type == 'billing' and data.parent_id:
                #~ query[prefix + 'street'] = data.parent_id.name
#~ 
        #~ if query.get(prefix + 'state_id'):
            #~ query[prefix + 'state_id'] = int(query[prefix + 'state_id'])
        #~ if query.get(prefix + 'country_id'):
            #~ query[prefix + 'country_id'] = int(query[prefix + 'country_id'])
#~ 
        #~ if query.get(prefix + 'vat'):
            #~ query[prefix + 'vat_subjected'] = True
#~ 
        #~ query = self._post_prepare_query(query, data, address_type)
#~ 
        #~ if not remove_prefix:
            #~ return query
#~ 
        #~ return dict((field_name, data[prefix + field_name]) for field_name in all_fields if prefix + field_name in data)
#~ 
    #~ def checkout_form_validate(self, data):
        #~ cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
#~ 
        #~ # Validation
        #~ error = dict()
        #~ for field_name in self._get_mandatory_billing_fields():
            #~ if not data.get(field_name):
                #~ error[field_name] = 'missing'
#~ 
        #~ if data.get("vat") and hasattr(registry["res.partner"], "check_vat"):
            #~ if request.website.company_id.vat_check_vies:
                #~ # force full VIES online check
                #~ check_func = registry["res.partner"].vies_vat_check
            #~ else:
                #~ # quick and partial off-line checksum validation
                #~ check_func = registry["res.partner"].simple_vat_check
            #~ vat_country, vat_number = registry["res.partner"]._split_vat(data.get("vat"))
            #~ if not check_func(cr, uid, vat_country, vat_number, context=None): # simple_vat_check
                #~ error["vat"] = 'error'
#~ 
        #~ if data.get("shipping_id") == -1:
            #~ for field_name in self._get_mandatory_shipping_fields():
                #~ field_name = 'shipping_' + field_name
                #~ if not data.get(field_name):
                    #~ error[field_name] = 'missing'
#~ 
        #~ return error
#~ 
    #~ def _get_shipping_info(self, checkout):
        #~ shipping_info = {}
        #~ shipping_info.update(self.checkout_parse('shipping', checkout, True))
        #~ shipping_info['type'] = 'delivery'
        #~ return shipping_info
#~ 
    #~ def checkout_form_save(self, checkout):
        #~ cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
#~ 
        #~ order = request.website.sale_get_order(force_create=1, context=context)
#~ 
        #~ orm_partner = registry.get('res.partner')
        #~ orm_user = registry.get('res.users')
        #~ order_obj = request.registry.get('sale.order')
#~ 
        #~ partner_lang = request.lang if request.lang in [lang.code for lang in request.website.language_ids] else None
#~ 
        #~ billing_info = {'customer': True}
        #~ if partner_lang:
            #~ billing_info['lang'] = partner_lang
        #~ billing_info.update(self.checkout_parse('billing', checkout, True))
#~ 
        #~ # set partner_id
        #~ partner_id = None
        #~ if request.uid != request.website.user_id.id:
            #~ partner_id = orm_user.browse(cr, SUPERUSER_ID, uid, context=context).partner_id.id
        #~ elif order.partner_id:
            #~ user_ids = request.registry['res.users'].search(cr, SUPERUSER_ID,
                #~ [("partner_id", "=", order.partner_id.id)], context=dict(context or {}, active_test=False))
            #~ if not user_ids or request.website.user_id.id not in user_ids:
                #~ partner_id = order.partner_id.id
#~ 
        #~ # save partner informations
        #~ if partner_id and request.website.partner_id.id != partner_id:
            #~ orm_partner.write(cr, SUPERUSER_ID, [partner_id], billing_info, context=context)
        #~ else:
            #~ # create partner
            #~ partner_id = orm_partner.create(cr, SUPERUSER_ID, billing_info, context=context)
#~ 
        #~ # create a new shipping partner
        #~ if checkout.get('shipping_id') == -1:
            #~ shipping_info = self._get_shipping_info(checkout)
            #~ if partner_lang:
                #~ shipping_info['lang'] = partner_lang
            #~ shipping_info['parent_id'] = partner_id
            #~ checkout['shipping_id'] = orm_partner.create(cr, SUPERUSER_ID, shipping_info, context)
#~ 
        #~ order_info = {
            #~ 'partner_id': partner_id,
            #~ 'message_follower_ids': [(4, partner_id), (3, request.website.partner_id.id)],
            #~ 'partner_invoice_id': partner_id,
        #~ }
        #~ order_info.update(order_obj.onchange_partner_id(cr, SUPERUSER_ID, [], partner_id, context=context)['value'])
        #~ address_change = order_obj.onchange_delivery_id(cr, SUPERUSER_ID, [], order.company_id.id, partner_id,
                                                        #~ checkout.get('shipping_id'), None, context=context)['value']
        #~ order_info.update(address_change)
        #~ if address_change.get('fiscal_position'):
            #~ fiscal_update = order_obj.onchange_fiscal_position(cr, SUPERUSER_ID, [], address_change['fiscal_position'],
                                                               #~ [(4, l.id) for l in order.order_line], context=None)['value']
            #~ order_info.update(fiscal_update)
#~ 
        #~ order_info.pop('user_id')
        #~ order_info.update(partner_shipping_id=checkout.get('shipping_id') or partner_id)
#~ 
        #~ order_obj.write(cr, SUPERUSER_ID, [order.id], order_info, context=context)
#~ 
    #~ 
#~ 
