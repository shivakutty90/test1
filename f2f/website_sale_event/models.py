from openerp import api, fields, models
from openerp.osv import osv

class pos_model(models.Model):
    _name = ‘pos.popup’

    name=fields.Char(‘Name’,size=30,required=True)

    def okay_refresh(self,cr,uid,ids,context=None):

    return{

        ‘type’:’ir.actions.client’,

    ‘tag’:’reload’,

    }
