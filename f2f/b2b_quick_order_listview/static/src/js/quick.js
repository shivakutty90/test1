$(document).ready(function () {

    openerp.set_cookie = function(name, value, ttl) {
        ttl = ttl || 24*60*60*365;
        document.cookie = [
            name + '=' + value,
            'path=/shop',
            'max-age=' + ttl,
            'expires=' + new Date(new Date().getTime() + ttl*1000).toGMTString()
        ].join(';');
    };

	var session = openerp.get_cookie('cart_items');
	if (session){
		cart_items = JSON.parse(session);
	}else{cart_items = {}}

	// Hide confirm quantities button in matrix list view
	if (!($("#products_grid").find(".variant_matrix").length > 0)){ 
	  $(this).find(".matrix_confirm_qty").addClass("hidden");
	}

	if (cart_items){
		_.each(cart_items, function(value, key){
			$('.'+key).prop("value", value['qtys']); 
		});
	}

    total_qtys = 0;
	_.each(cart_items, function(value, key){
	        total_qtys += value['qtys'];
	});

    total_amount = 0;
	_.each(cart_items, function(value, key){
	        total_amount += value['price'];
	});

	$('.tqty').find('span').text(total_qtys);	/*set total quantities to matrix list view*/
	$('.total_amount').find('span').text(total_amount);	 /*set total prices to matrix list view*/	


	// Calculate total quantities and total amounts, collect variant ids for creating order
	$(".js_quantity1").change(function() {
	    var qty = parseInt($(this).val(), 10);
	    var variant_id = parseInt($(this).data('varient-id'),10);
	    var variant_price = parseInt($(this).data('varient-price'),10);
        if (isNaN(qty)){
        	delete cart_items[variant_id];
    	}else{
        	cart_items[variant_id] = {'qtys':qty,'price':variant_price*qty};
    	}

	    total_qtys = 0;
		_.each(cart_items, function(value, key){
		        total_qtys += value['qtys'];
		});

	    total_amount = 0;
		_.each(cart_items, function(value, key){
		        total_amount += value['price'];
		});

		$('.tqty').find('span').text(total_qtys);	/*set total quantities to matrix list view*/
		$('.total_amount').find('span').text(total_amount);	 /*set total prices to matrix list view*/			
        openerp.set_cookie('cart_items', JSON.stringify(cart_items), 3600*3600);
	});

    $(this).on('click', '.xx_confirm_qtys', function (ev) {
        ev.preventDefault();
		tmp_list = [];
	        $(".js_quantity1").each(function() {
	        	var value = parseInt($(this).val(), 10);
		        if (value){
		        	var product_id = parseInt($(this).data('varient-id'),10); 
		        	
		        	tmp_list.push([product_id, value]);
		        	$(this).val('');		// Clearing entered quantities while confirming qtys.
		        }
	    });		
		if (tmp_list){
 			    openerp.jsonRpc("/shop/cart/confirm_qty", 'call', {'cart_list': tmp_list}).then(function (result) {
					_.each(result, function(data){
				        var $q = $(".my_cart_quantity");
				        $q.parent().parent().removeClass("hidden", !data.quantity);
				        $q.html(data.cart_quantity).hide().fadeIn(600);
				        $("#cart_total").replaceWith(data['website_sale.total']);
				    });	
				});
		}    
		$('.tqty').find('span').text('');	/*clear total quantities to matrix list view*/
		$('.total_amount').find('span').text('');	 /*clear total prices to matrix list view*/		
		cart_items = {};
        document.cookie = ['cart_items' + '=' + JSON.stringify(cart_items),'path=/shop',].join(';');
		// location.reload();
	}); 

// Fixed table sidebar and header start
    $('.fixedTable-body').scroll(function () {
        $(this).prev().find('table').css('margin-top', -$($(this)).scrollTop());
        $(this).prev().prev().find('table').css('margin-left', -$($(this)).scrollLeft());

		$(this).next().find('table').css('margin-top', -$($(this)).scrollTop());
		$(this).next().next().next().find('table').css('margin-left', -$($(this)).scrollLeft());        
    });
// Fixed table sidebar and header end

// popup matrix table start
    $('.xx_fullscreen').click(function(){
        var product_id = $(this).data('product-id');


        openerp.jsonRpc("/shop/get_matrix_row", 'call', {'product': product_id}).then(function (table) {
		        var thead ='';
		        for(i = 1; i< table[0].length; i++){
		        	thead += "<th>"+table[0][i]+"</th>";
		        }        	
		        var tablerow = '';
		        // table[0].reverse();
		        for(i = 0; i< table[1].length; i++){
		        	var tr = "<td>"+table[1][i][1]+"</td>";
			        for(j = 2; j< table[1][i].length; j++){
			        	tr += "<td><input class='js_quantity1 "+table[1][i][j][0]+"' data-varient-id='"+table[1][i][j][0]+"' data-varient-price='"+table[1][i][j][1]+"' type=text size='3'/>
                                <i class='tiptext fa fa-info-circle' data-varient-id='"+table[1][i][j][0]+"' data-varient-price='"+table[1][i][j][1]+"' style='color:#428BCC;'></i>
                                <div class='hover_price "+table[1][i][j][0]+"'>Sale Price: "+table[1][i][j][1]+"</div>";
			        }
		        		tablerow += "<tr>"+tr+"</tr>";
		        }

		        var dynamic_table = "<table class='matrix'>
		        						<thead><tr>"+thead+"</tr></thead>
		        						<tbody>"+tablerow+"</tbody>
							        </tabe>";

		        $("#cart_matrix .popup_matrix").html(dynamic_table);
            }); 
			var test = this;
	        $('body').on('DOMNodeInserted', '#cart_matrix .popup_matrix table', function () {
			    setTimeout(function(){				
		        	$(test).parents("section").find(".variant_matrix .fixedTable-body table tbody tr").each(function(){
						$(this).find("td input").each(function(){
							var class_name = $(this).data('varient-id');
							$("#cart_matrix ."+class_name).val($(this).val());
							console.log($(this).val());
						});
					});
		        },100);
			});
    });
// popup matrix table start

// Only number enterable in matrix table input field start
	 $(".js_quantity1").keypress(function (e) {
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	       return false;
	    }
		});
// Only number enterable in matrix table input field end

// css for matrix table scroll start
	$(".fixedTable").each(function(){
		var fixedTablebodyWidth = $(this).find(".fixedTable-body table").width();
		
		$(this).children(".fixedTable-body").css("width",fixedTablebodyWidth + 20);
		$(this).children(".fixedTable-header").css("width",fixedTablebodyWidth + 20);

		var fixedTablesidebarWidth = $(this).find('.fixedTable-sidebar').width();
        $(this).children(".first_child").css("width",fixedTablesidebarWidth);        
        $(this).children(".totalSidebar").css("width",fixedTablesidebarWidth);        
		$(this).children(".fixedTable-header").css("margin-left",fixedTablesidebarWidth);
		$(this).children(".bottom_total").css("margin-left",fixedTablesidebarWidth + 20);
	});
// css for matrix table scroll end

// Sale price hover start
	$('.oe_website_sale').each(function () {
	    var oe_website_sale = this;
	    $(oe_website_sale).find(".tiptext").mouseenter(function() {
	        var $input = $(this);
	        var product_id = $input.data('varient-id');
            $(oe_website_sale).find("."+product_id).show();
	    }).mouseleave(function() {
	        $(oe_website_sale).find(".hover_price").hide();
	    });
	    });
// Sale price hover end
// popup Matrix getting input start
	$(this).on("keypress","#cart_matrix .js_quantity1",function(){
		var value = $(this).val();
		var variant_id = $(this).data('varient-id');
		$('.oe_website_sale').find('.'+variant_id).val(value).change();; 
	});
// popup Matrix getting input end

// Matrix text input focus color start
$(this).each(function(){
	$(".js_quantity1").focusin(function () {
	   $(this).css({ 'background': '#a9a9a9' });
	});
	$(".js_quantity1").focusout(function () {
	   $(this).css({ 'background': '' });
	});
	$("section").focusin(function(){
	   $(this).css({ 'background': '#d3d3d3' });
	});
	$("section").focusout(function(){
	   $(this).css({ 'background': '' });
	});
});
// Matrix text input focus color end
});


