openerp.b2b_quick_order_listview  = function(instance) {
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    instance.b2b_quick_order_listview.Matrix = instance.web.form.FormWidget.extend(instance.web.form.ReinitializeWidgetMixin, {
		template: "b2b_quick_order_listview.Matrix",
        init: function() {
            this._super.apply(this, arguments);
            var self = this;
            this.set({
                product_id1: false,
                thead:false,
            });			        
            this.field_manager.on("field_changed:product_id1", this, function() {
                this.set({"product_id1":this.field_manager.get_field_value("product_id1")});
            });	
		},
        initialize_field: function() {
            instance.web.form.ReinitializeWidgetMixin.initialize_field.call(this);
            var self = this;
            self.on("change:product_id1", self, self.initialize_content);
        },
        initialize_content: function() {
            var self = this;
			var product_id = parseInt(this.get("product_id1"));
            if (!product_id)
                return;	
            return new instance.web.Model("product.template").call('read',[product_id, []]).then(function(product){
                if (product.product_variant_ids.length > 1){
					
                    new instance.web.Model('sale.order.line').call('get_variant_matrix', [{'product_id':product_id}]).then(function(result){
                        // console.log(result[1]);
                        self.th = result[0];
                        self.tr = result[1];
                        self.rlength = result[0].slice(1);
                        self.display_data();
                    });
                    }else{
						
                        self.th = [];
                        self.tr = [];
                        self.rlength = undefined;	
                        self.display_data();						
					}
            });  
		},	

        display_data: function() {
            var self = this;
            self.$el.html(QWeb.render("b2b_quick_order_listview.Matrix", {widget: self}));
			self.$(".variant_qty").change(function(event){
				var qtys = 0;				
				$(this).parents('tr').find('td').each(function() {
					var qty = parseInt($(this).find('input').val(),10);
					if (isNaN(qty)) qty = 0;
					if (qty != undefined){
						qtys += qty;
					}
				$(this).parents('tr').find('span').text(qtys);
				});
			});			
        },
    }); 

    instance.web.form.custom_widgets.add('matrix', 'instance.b2b_quick_order_listview.Matrix');
};
