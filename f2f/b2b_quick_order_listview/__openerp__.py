{
	'name': 'B2B Quick Order Listview',
    'description': 'B2B Quick Order Listview',
    'category': 'Ecommerce',
    'version': '1.0',
    'author': 'Insoft',
    'depends': ['product','sale','website_sale','website','payment'],
    'data': [
        'views/templates.xml',
        'views/views.xml'      
    ],
    'js': ['static/src/js/quick.js',
           'static/src/js/back.js'],
	'qweb' : ['static/src/xml/back.xml'],
}
