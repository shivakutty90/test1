{
    'name': 'Pre order',
    'description': 'pre order',
    'category': 'Pre order',
    'version': '1.0',
    'author': 'Insoft',
    'depends': ['website_sale', 'web', 'website', 'base'],
    'data': [
        'views/template.xml',
        'views/menu_preorder.xml',
        #~ 'views/check_data.xml',
        'views/web_preorder_config.xml',
        'views/preorder_view.xml',
        'views/sale_view.xml',  
    ],
    'js': [ ],
    #~ 'qweb' : ['static/src/xml/back.xml'],
}
