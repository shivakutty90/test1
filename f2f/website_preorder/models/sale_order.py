# -*- coding: utf-8 -*-
import random

from openerp import SUPERUSER_ID
from openerp.osv import osv, orm, fields
from openerp.addons.web.http import request
from openerp.tools.translate import _


class sale_order(osv.Model):
    _inherit = "sale.order"
    
    def _generate_so_line(self, cr, uid, ids, product_id=None, set_qty=0, context=None, **kwargs):
        """ Add or set product quantity, add_qty can be negative """
        sol = self.pool.get('sale.order.line')
        quantity = 0
        for so in self.browse(cr, uid, ids, context=context):
            # Create line
            if product_id:
                values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=1, context=context)
                line_id = sol.create(cr, SUPERUSER_ID, values, context=context)
                # compute new quantity
                if set_qty:
                    quantity = set_qty            

                # Remove zero of negative lines
                if quantity <= 0:
                    sol.unlink(cr, SUPERUSER_ID, [line_id], context=context)
                else:
                    # update line
                    values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=quantity, line_id=line_id, context=context)
                    values['product_uom_qty'] = quantity
                    sol.write(cr, SUPERUSER_ID, [line_id], values, context=context)
        return True
        
class website(orm.Model):
    _inherit = 'website'

    def _generate_line(self, cr, uid, ids, force_create=False, context=None):
        sale_order_obj = self.pool['sale.order']
        sale_order = None
                
        # create so if needed
        if force_create:
            # TODO cache partner_id session
            partner = self.pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id

            for w in self.browse(cr, uid, ids):
                values = {
                    'user_id': w.user_id.id,
                    'partner_id': partner.id,
                    'pricelist_id': partner.property_product_pricelist.id,
                    #~ 'section_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'website', 'salesteam_website_sales')[1],
                }
                sale_order_id = sale_order_obj.create(cr, SUPERUSER_ID, values, context=context)
                values = sale_order_obj.onchange_partner_id(cr, SUPERUSER_ID, [], partner.id, context=context)['value']
                sale_order_obj.write(cr, SUPERUSER_ID, [sale_order_id], values, context=context)                
                sale_order = sale_order_obj.browse(cr, SUPERUSER_ID, sale_order_id, context=context)        
        return sale_order

class preorder_config(osv.Model):
    _inherit = 'preorder.config'
    _name = 'preorder.config'

    _columns = {        
        'test': fields.char('Test'),
    }
