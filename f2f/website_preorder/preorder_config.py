# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import models, fields, api, _

class preorder_config(models.Model):
    _name = 'preorder.config'   
    _description = 'Pre-Order Configuration'         
    
    name = fields.Char('Name')
    code = fields.Char('Code')    
    partner_ids = fields.Many2many('res.partner','preorder_partner_rel','preorder_id','partner_id', string='Customer', help='Select customer to show the preorder')
    logo = fields.Binary(string='Logo')
    start_date = fields.Datetime('Start Date')
    expire_date = fields.Datetime('Expire Date')
    product_ids = fields.Many2many('product.template', 'preorder_product_rel','preorder_id','product_id', string='Product')
    supplier_expire_date = fields.Datetime('Supplier Expire Date')
    delivery_date = fields.Date('Delivery Date')    
    multi_preorder = fields.Boolean('Multiple Pre-Order')    
    description = fields.Text('Description')
    show_previous_sales = fields.Boolean('Show Previous Sales')    
    responsible_manager = fields.Many2one('res.users','Responsible Manager')
    
	# Discount Rules
    rule_name =  fields.Char('Rule Name')
    rule_discount =  fields.Float('Discount(%)')
    rule_amount =  fields.Float('Amount')
    exclusion_product_ids =  fields.Many2many('product.template', 'preorder_exclusion_product_rel','preorder_id','product_id', string='Product Exclusion')
    exclusion_category_ids =  fields.Many2many('product.category', 'preorder_exclusion_category_rel','preorder_id','category_id', string='Product Category Exclusion')
    exclusion_brand_ids =  fields.Many2many('product.brand', 'preorder_exclusion_brand_rel','preorder_id','brand_id', string='Product Brand Exclusion')      
        
