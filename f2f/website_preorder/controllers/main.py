# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
import logging
import werkzeug
import werkzeug.utils
from datetime import datetime
from math import ceil

import itertools
from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF
from openerp.tools.safe_eval import safe_eval

from openerp.osv import fields
from openerp import http
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
from openerp.addons.web.controllers.main import login_redirect

from openerp.addons.website_sale.controllers.main import website_sale as Home
from openerp.addons.website_sale.controllers.main import QueryURL as QueryURL
import openerp.addons.website_sale.controllers.main as inherited_sale

PPG = 20 # Products Per Page
PPR = 4  # Products Per Row

_logger = logging.getLogger(__name__)

class website_preorder(http.Controller):
    
    @http.route(['/page/preorder', '/page/preorder/<model("preorder.config"):preorder>'], type='http', auth='user', website=True)
    def preorder(self, preorder=None, token=None):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        domain = [] 
        keep = QueryURL('/page/preorder', preorder=preorder and int(preorder))        
        if preorder:
            domain = [('id','=',int(preorder))]
            preorder = pool['preorder.config'].browse(cr, uid, int(preorder), context=context)
            url = "/page/preorder/%s" % slug(preorder)
        obj_search = http.request.env['preorder.config'].search([])
        print obj_search, '================'
        for i in obj_search:
            print i.start_date, '111111111111111111111111'
            print i.expire_date, '2222222222222222222222222222'
        today = datetime.today()
        filter_preorders = http.request.env['preorder.config'].search([])
        preorder_config_obj = http.request.env['preorder.config'].search(domain)
        return http.request.render('website_preorder.preorder_init', {
                                            'preorders' : preorder_config_obj, 
                                            'keep': keep,
                                            'preorder': preorder,
                                            'today': today,
                                            'filter_preorders': filter_preorders})
    
    def get_attribute_value_ids_matrix(self, product):
        attribute_value_ids = []
        visible_attrs = set(l.attribute_id.id for l in product.attribute_line_ids if len(l.value_ids) >= 1)
        
        for p in product.product_variant_ids:
            attribute_value_ids.append([p.id, [v.id for v in p.attribute_value_ids if v.attribute_id.id in visible_attrs]])
            
        return attribute_value_ids
    
    def _get_search_domain(self, search, product_ids, attrib_values):
        domain = request.website.sale_product_domain()

        if search:
            for srch in search.split(" "):
                domain += [
                    '|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]

        #~ if category:
            #~ domain += [('public_categ_ids', 'child_of', int(category))]
        
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        return domain

    @http.route([
        '/preorder/shop/<model("preorder.config"):preorder>',
        '/preorder/shop/<model("preorder.config"):preorder>/page/<int:page>'        
    ], type='http', auth="public", website=True)
    def shop(self, page=0, preorder=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])
        
        preorder_obj = pool['preorder.config']
        preorder_products = []
        if preorder:
            preorder_br = preorder_obj.browse(cr, uid, int(preorder), context=context)
            if preorder_br.product_ids:
                preorder_products = [p.id for p in preorder_br.product_ids]
        #~ domain = self._get_search_domain(search, preorder, attrib_values)
        domain = [('id', 'in', preorder_products)]
        if not preorder_products:
            return request.redirect('/page/preorder')
        keep = QueryURL('/preorder/shop', preorder=preorder and int(preorder), search=search, attrib=attrib_list)

        if not context.get('pricelist'):
            pricelist = inherited_sale.get_pricelist()
            context['pricelist'] = int(pricelist)
        else:
            pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)

        product_obj = pool.get('product.template')

        url = "/preorder/shop"
        product_count = product_obj.search_count(cr, uid, domain, context=context)
        if search:
            post["search"] = search
        if preorder:
            preorder = pool['preorder.config'].browse(cr, uid, int(preorder), context=context)
            url = "/preorder/shop/%s" % slug(preorder)
        if attrib_list:
            post['attrib'] = attrib_list
        pager = request.website.pager(url=url, total=product_count, page=page, step=PPG, scope=7, url_args=post)
        product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        products = product_obj.browse(cr, uid, product_ids, context=context)
        style_obj = pool['product.style']
        style_ids = style_obj.search(cr, uid, [], context=context)
        styles = style_obj.browse(cr, uid, style_ids, context=context)
        
        preorder_ids = preorder_obj.search(cr, uid, [], context=context)
        preorders = preorder_obj.browse(cr, uid, preorder_ids, context=context)

        attributes_obj = request.registry['product.attribute']
        attributes_ids = attributes_obj.search(cr, uid, [], context=context)
        attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)

        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)

        user_id = pool['res.users'].browse(cr, uid, uid, context=None)
        currency = user_id.partner_id.property_product_pricelist.currency_id.symbol
        thead = []
        trows = []
        non_variant_prices = []
        for product in products:
            if not product.attribute_line_ids:
                price = self.get_sale_price(int(product.product_variant_ids[0].id))
                non_variant_prices.append([product.id, price])

            if product.attribute_line_ids and product.sale_ok:
                result = self.get_matrix_row(product)
                thead.append(result[0])
                for x in result[1]:
                    trows.append(x)
        values = {
            'non_variant_prices': non_variant_prices,
            'currency': currency,
            'thead': thead,
            'trows': trows,
            'search': search,
            'preorder': preorder,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'bins': inherited_sale.table_compute().process(products),
            'rows': PPR,
            'styles': styles,
            'preorders': preorders,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib',i) for i in attribs]),
        }
        return request.website.render("website_preorder.products", values)
    
    @http.route(['/preorder/shop/get_matrix_row'], type='json', auth="public", methods=['POST'], website=True)
    def get_matrix_row(self, product):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        if type(product) == int:
            product = pool['product.template'].browse(cr, uid, product, context=None)
        temp_attribute_value_ids = []

        attribute_value_ids = self.get_attribute_value_ids_matrix(product)

        thlength = len(product.attribute_line_ids[0].value_ids)

        th = [str(x.name) for x in product.attribute_line_ids[0].value_ids]
        th.insert(0, str(product.attribute_line_ids[0].attribute_id.name))    # add attribute name
        th.insert(0, product.id)                                              # add product id in index 0


        product_attribute_line_ids = product.attribute_line_ids
        for x in product_attribute_line_ids:
            temp_attribute_value_ids.append([y.id for y in x.value_ids])
        tmp_compinations_list_of_tuple = list(itertools.product(*temp_attribute_value_ids))
        product_original_compinations = map(list, tmp_compinations_list_of_tuple)
        product_temp_compinations = []
        for x in product_original_compinations:
            for i in xrange(0,len(attribute_value_ids)):
                if set(x) == set(attribute_value_ids[i][1]):
                    tmp_rows = [x]
                    tmp_rows.insert(0,int(attribute_value_ids[i][0]))
                    product_temp_compinations.append(tmp_rows)
        trlength = len(product_temp_compinations)/thlength
        
        product_compinations = list(zip(*[product_temp_compinations[i:i+trlength] for i in range(0, len(product_temp_compinations), trlength)]))
        rows_list = map(list, product_compinations)
        trow = []
        for row in rows_list:
            attribute_value = pool['product.attribute.value'].browse(cr,uid,row[0][1][1:],context=None)
            attribute_value_names = [str(x.name) for x in attribute_value]
            y_axis_compinations = ""
            for i in xrange(0,len(attribute_value_names)):
                if i == 0:
                    y_axis_compinations += str(attribute_value_names[i])
                else:
                    y_axis_compinations += '/'+str(attribute_value_names[i])
            for i in xrange(0, len(row)):
                del row[i][1:]
            tr = list(itertools.chain(*row))
            tr_withprices = []
            for x in tr:
                price = self.get_sale_price(int(x))
                qty_available = self.check_qtys_available(int(x))
                tr_withprices.append([x, price, qty_available])
            tr_withprices.insert(0, y_axis_compinations)
            tr_withprices.insert(0,int(product.id))
            trow.append(tr_withprices)
        return th, trow
    
    @http.route(['/preorder/shop/product/<model("product.template"):product>'], type='http', auth="public", website=True)
    def product(self, product, category='', search='', **kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        category_obj = pool['product.public.category']
        template_obj = pool['product.template']

        context.update(active_id=product.id)

        if category:
            category = category_obj.browse(cr, uid, int(category), context=context)
            category = category if category.exists() else False

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        keep = QueryURL('/preorder/shop', category=category and category.id, search=search, attrib=attrib_list)

        category_ids = category_obj.search(cr, uid, [], context=context)
        category_list = category_obj.name_get(cr, uid, category_ids, context=context)
        category_list = sorted(category_list, key=lambda category: category[1])

        pricelist = self.get_pricelist()

        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)

        if not context.get('pricelist'):
            context['pricelist'] = int(self.get_pricelist())
            product = template_obj.browse(cr, uid, int(product), context=context)

        values = {
            'search': search,
            'category': category,
            'pricelist': pricelist,
            'attrib_values': attrib_values,
            'compute_currency': compute_currency,
            'attrib_set': attrib_set,
            'keep': keep,
            'category_list': category_list,
            'main_object': product,
            'product': product,
            'get_attribute_value_ids': self.get_attribute_value_ids
        }
        return request.website.render("website_preorder.product", values)
    
    @http.route(['/preorder/shop/check_qtys_available'], type='json', auth="public", methods=['POST'], website=True)
    def check_qtys_available(self, product_id, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        product = pool['product.product'].browse(cr, SUPERUSER_ID, product_id, context=context)
        if product.virtual_available > 0:
            return True
        else:
            return False

    @http.route(['/preorder/shop/get_sale_price'], type='json', auth="public", methods=['POST'], website=True)
    def get_sale_price(self, product_id, use_order_pricelist=False, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        product = pool['product.product'].browse(cr, uid, product_id, context=context)
        partner = pool['res.users'].browse(cr, uid, uid, context=context).partner_id
        add_qty = 0
        if use_order_pricelist:
            pricelist_id = request.session.get('sale_order_code_pricelist_id') or partner.property_product_pricelist.id
        else:
            pricelist_id = partner.property_product_pricelist.id
            # TO-DO simplyfy below lines products to-> product
        prices = pool['product.pricelist'].price_rule_get_multi(cr, uid, [], [(product, add_qty, partner)], context=context)
        return prices[product_id][pricelist_id][0]    
        
    @http.route(['/preorder/shop/confirm_qty'], type='json', auth="public", methods=['POST'], website=True)
    def cart_confirm_qty(self, cart_list, display=True, **kw):        
        order = request.website._generate_line(force_create=1)
        print "&&&&&&&&&&&&&&&&&&&&", order
        values = []
        for item in cart_list:
            order._generate_so_line(product_id=item[0], set_qty=item[1])
        #~ return request.redirect('/preorder/shop/get_status/%s' % order.id)    
        return {
                'state': 'draft',
                'message': '<p>%s</p>' % _('Your preorder has been received.'),
            }
    
    @http.route('/preorder/shop/get_status/<int:sale_order_id>', type='json', auth="public", website=True)
    def payment_get_status(self, sale_order_id, **post):
        cr, uid, context = request.cr, request.uid, request.context

        order = request.registry['sale.order'].browse(cr, SUPERUSER_ID, sale_order_id, context=context)
        #~ assert order.id == request.session.get('sale_last_order_id')

        if not order:
            return {
                'state': 'error',
                'message': '<p>%s</p>' % _('There seems to be an error with your request.'),
            }        
        else:
            return {
                'state': 'draft',
                'message': '<p>%s</p>' % _('Your preorder has been received.'),
            }

    @http.route('/preorder/shop/validate', type='http', auth="public", website=True)
    def confirm_validate(self,  sale_order_id=None, **post):
        """ Method that should be called by the server when receiving an update
        for a transaction. State at this point :

         - UDPATE ME
        """
        cr, uid, context = request.cr, request.uid, request.context
        email_act = None
        sale_order_obj = request.registry['sale.order']

        return request.redirect('/preorder/shop/confirmation')
        
        
    @http.route(['/preorder/shop/confirmation'], type='http', auth="public", website=True)
    def preorder_confirmation(self, **post):
        """ End of checkout process controller. Confirmation is basically seing
        the status of a sale.order. State at this point :

         - should not have any context / session info: clean them
         - take a sale.order id, because we request a sale.order and are not
           session dependant anymore
        """
        cr, uid, context = request.cr, request.uid, request.context
        sale_order_id = request.session.get('sale_last_order_id')
        if sale_order_id:
            order = request.registry['sale.order'].browse(cr, SUPERUSER_ID, sale_order_id, context=context)
        else:
            return request.redirect('/preorder/shop')

        return request.website.render("website_preorder.preorder_confirmation", {'order': order})

    
