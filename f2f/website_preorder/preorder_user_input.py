# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import models, fields, api, _

class preorder_user_input(models.Model):
    _name = 'preorder.user.input'   
    _description = 'Pre-Order User Input'      
    
    preorder_id = fields.Many2one('preorder.config','PreOrder Ref')    
    name = fields.Char('Name')     
    created_date = fields.Datetime('Created Date')
    partner_id = fields.Many2one('res.partner','Partner')
    preorder_user_input_product_line = fields.One2many('preorder.user.input.product.line', 'user_input_id', string='Line Ref')
    token = fields.Char("Identification token", readonly=True)
    state = fields.Selection([
            ('draft', 'Unconfirmed'),
            ('confirm', 'Confirmed'),            
        ], string='Status', default='draft', readonly=True, required=True, copy=False,
        )
    
    _defaults = {
        'date_create': fields.datetime.now,
        'state': 'Unconfirmed',
        'token': lambda s, cr, uid, c: uuid.uuid4().__str__(),
    }
    
class preorder_user_input_product_line(models.Model):
    _name = 'preorder.user.input.product.line'   
    _description = 'Pre-Order User Input Product Line'      
    
    user_input_id = fields.Many2one('preorder.user.input','User Input Ref')
    product_id = fields.Many2one('product.product','Product')
    qty = fields.Float('Qty')
