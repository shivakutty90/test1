{
    'name': 'Pre-Order',
    'description': 'Generating Pre-Order',
    'category': 'Sale',
    'version': '1.0',
    'author': 'Cats&Dogs bvba',
    'website' : 'http://www.catsanddogs.com',
    'depends': ['website_sale', 'product_brand', 'sale', 'website'],
    'data': [        
        'views/menu_preorder.xml',
        'views/preorder_config_view.xml',
        'views/template.xml',
        'views/shop_templates.xml',
        'views/preorder_management_view.xml',
        'views/sale_view.xml',  
	'views/preorder_user_input_view.xml',
    ],
    'js': [ ],
    #~ 'qweb' : ['static/src/xml/back.xml'],
    'installable': True
}
