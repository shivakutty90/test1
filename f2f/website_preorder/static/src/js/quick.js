var width = 0;

function update_product_image(event_source, product_id) {
	var $img = event_source.parents('section').prev().find('img');
	$img.attr("src", "/website/image/product.product/" + product_id + "/image");
}
function update_product_templ_image(event_source, product_id) {
	var $img = event_source.parents('section').prev().find('img');
	$img.attr("src", "/website/image/product.template/" + product_id + "/image");
}
$(document).ready(function () {
	var self = this;
    openerp.set_cookie = function(name, value, ttl) {
        ttl = ttl || 24*60*60*365;
        document.cookie = [
            name + '=' + value,
            'path=/preorder/shop',
            'max-age=' + ttl,
            'expires=' + new Date(new Date().getTime() + ttl*1000).toGMTString()
        ].join(';');
    };

	var session = openerp.get_cookie('cart_items');
	if (session){
		cart_items = JSON.parse(session);
	}else{cart_items = {}}

	// Hide confirm quantities button in matrix list view
	if (!($("#products_grid").find(".variant_matrix").length > 0)){ 
	  $(this).find(".matrix_confirm_qty").addClass("hidden");
	}

	if (cart_items){
		_.each(cart_items, function(value, key){
			$('.'+key).val(value['qtys']); 
		});
	}

    total_qtys = 0;
	_.each(cart_items, function(value, key){
	        total_qtys += value['qtys'];
	});

    total_amount = 0.00;
	_.each(cart_items, function(value, key){
	        total_amount += parseFloat(value['price']);
	});

	$('.tqty').find('span').text(total_qtys.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));	/*set total quantities to matrix list view*/

	if (total_amount != 0) $('.total_amount').find('span').text(parseFloat(total_amount).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));	 /*set total prices to matrix list view*/	


	// Calculate total quantities and total amounts, collect variant ids for creating order
	$(".js_quantity1").change(function() {

// ################################### Total row quantities
	    var row_index = $(this).parents('tr').index()+1;

		var total_row_quantities = 0;
		$(this).parents('tr').find('td').each(function() {
		    var qty = parseInt($(this).find('input').val(),10);
		    if (isNaN(qty)) qty = 0;
		    if (qty != undefined){
		        total_row_quantities += qty;
		    }
		    $(this).parents('section').find('.right_total').find("table tr:nth-child("+row_index+")").find('span').text(total_row_quantities.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
		});
// ################################### 
// ################################### Total column quantities, prices
	    var column_index = $(this).parents('td').index()+1;
		var total_column_quantities = 0;
		var total_column_prices = 0.00;
		var total_quantities = 0;
		var total_prices = 0.00;
		var currency = $(this).parents('#products_grid').find('.currency').val();

		$(this).parents('tbody').find('tr').each(function() {
	        $(this).find("td:nth-child("+column_index+") > input").each(function(){
				var price = parseFloat($(this).data('varient-price'), 10);
	            var qty = parseInt($(this).val(), 10);   
	            if(isNaN(qty)) qty = 0;
	            if (qty != undefined){      
	                total_column_prices += price*qty;
	            	total_column_quantities += qty;
	            }
	        });
        $(this).parents('section').find('.bottom_total').find("table tbody tr:nth-child(1) td:nth-child("+column_index+") span").text(total_column_quantities.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
        $(this).parents('section').find('.bottom_total').find("table tbody tr:nth-child(2) td:nth-child("+column_index+") span").text(String(currency + " "+total_column_prices.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
// ################################### 
// ################################### Total Prices, quantities
        $(this).find('td input').each(function(){
			var price = parseFloat($(this).data('varient-price'), 10);
            var qty = parseInt($(this).val(), 10);       
            if(isNaN(qty)) qty = 0;
            if (qty != undefined){      
                total_prices += price*qty;
            	total_quantities += qty;
            }               	
        });

        $(this).parents('section').find('.totalRighttbar').find("table tbody tr:nth-child(1) td span strong").text(total_quantities.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
        $(this).parents('section').find('.totalRighttbar').find("table tbody tr:nth-child(2) td span strong").text(String(currency + " "+total_prices.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
	});
// ##################################
	    var qty = parseInt($(this).val(), 10);
	    var variant_id = parseInt($(this).data('varient-id'),10);
	    var variant_price = parseFloat($(this).data('varient-price'),10);

        if (isNaN(qty)){
        	delete cart_items[variant_id];
    	}else{
        	cart_items[variant_id] = {'qtys':qty,'price':variant_price*qty};
    	}

	    total_qtys = 0;
		_.each(cart_items, function(value, key){
		        total_qtys += value['qtys'];
		});

	    total_amount = 0;
		_.each(cart_items, function(value, key){
		        total_amount += value['price'];
		});
		$('.tqty').find('span').text(total_qtys.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")); /*set total quantities to matrix list view*/
		$('.total_amount').find('span').text(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")); /*set total prices to matrix list view*/
	
        openerp.set_cookie('cart_items', JSON.stringify(cart_items), 3600*3600);
	});

    $(this).on('click', '.xx_confirm_qtys', function (ev) {
        ev.preventDefault();
		tmp_list = [];
		if (cart_items){
			_.each(cart_items, function(value, key){
	        	tmp_list.push([parseInt(key, 10), value['qtys']]);
			});
		}
		if (tmp_list){
	    	  $('#preloader').show();
 			    openerp.jsonRpc("/preorder/shop/confirm_qty", 'call', {'cart_list': tmp_list});
	    	  $('#preloader').hide();
		}

	}); 

// Fixed table sidebar and header start
    $('.fixedTable-body').scroll(function () {
        $(this).prev().find('table').css('margin-top', -$($(this)).scrollTop());
        $(this).parents('.fixedTable').find('.fixedTable-header table').css('margin-left', -$($(this)).scrollLeft());

		$(this).next().find('table').css('margin-top', -$($(this)).scrollTop());
		$(this).parents('.fixedTable').find('.bottom_total table').css('margin-left', -$($(this)).scrollLeft());        
    });

// css for matrix table scroll start
	$(".fixedTable").each(function(){
		var fixedTablebodyWidth = $(this).find(".fixedTable-body table").width();
		// var rightWidth = $(this).find(".right_total").width();

		$(this).find(".fixedTable-body").css("width",fixedTablebodyWidth + 20);
		$(this).find(".fixedTable-header").css("width",fixedTablebodyWidth + 20);
		$(this).find(".bottom_total").css("width",fixedTablebodyWidth + 20);

		var fixedTablesidebarWidth = $(this).find('.fixedTable-sidebar').width();
        $(this).find(".first_child").css("width",fixedTablesidebarWidth);        
        $(this).find(".totalSidebar").css("width",fixedTablesidebarWidth);        
        // $(this).find(".totalRighttbar").css("width",rightWidth);        
        // $(this).find(".header_qty_label").css("width",rightWidth);        
	});
// css for matrix table scroll end

// Preorder price hover start
	$('.oe_website_preorder').each(function () {
	    var oe_website_preorder = this;
	    $(oe_website_preorder).find(".tiptext").mouseenter(function() {
	        var $input = $(this);
	        var product_id = $input.data('varient-id');
            $(oe_website_preorder).find("."+product_id).show();
	    }).mouseleave(function() {
	        $(oe_website_preorder).find(".hover_price").hide();
	    });    
	    });

// Matrix text input focus color start
$(this).each(function(){
	$(".js_quantity1").focusin(function () {
	   $(this).css({ 'background': '#a9a9a9' });
	   update_product_image($(this), +$(this).data('varient-id')); 
	});
	$(".js_quantity1").focusout(function () {
	   $(this).css({ 'background': '' });
	   update_product_templ_image($(this), +$(this).data('product-id')); 
	});
});

// Matrix text input focus color end
$('.reset_qtys').click(function(){
	$('.js_quantity1').each(function(){
		$(this).val('');
	});
});
	$('.tqty').find('span').text('0 Unit(s)'); /*set total quantities to matrix list view*/
	$('.total_amount').find('span').text('0.00'); /*set total prices to matrix list view*/
	cart_items = {};
    document.cookie = ['cart_items' + '=' + JSON.stringify(cart_items),'path=/preorder/shop',].join(';');
});
