# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import math
import re
import time

from openerp import api, tools, SUPERUSER_ID
from openerp.osv import osv, fields, expression
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import psycopg2

import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round, float_compare

class product_template(osv.osv):
    _name = _inherit = "product.template"
           
    _columns = {
    'name_sku': fields.char('Code', size=3,required=True),
    'default_code': fields.related('product_variant_ids', 'default_code', type='char', string='SKU',readonly=True),
    }
    _sql_constraints = [
        ('value_company_uniq', 'unique (name_sku)', 'This Name SKU value already exists !')
    ]

class product_product(osv.osv):
    _inherit = "product.product"
    
    def create(self, cr, uid, vals, context=None):
        res = super(product_product, self).create(cr, uid, vals, context=context)
        product_br = self.pool.get('product.product').browse(cr, uid, res)
        name = ''
        categ_name = ''
        product_name = ''
        value_name = ''
        att_name = ''
        if product_br.seller_ids:
            for s in product_br.seller_ids:
                if s == product_br.seller_ids[0]:
                    name = s.product_code[0:2]
        if product_br.categ_id.category_sku:
            categ_name = product_br.categ_id.category_sku[0:1]
        if product_br.name_sku:
            product_name = product_br.name_sku[0:3]
        for l in product_br.attribute_value_ids:
            if l.attribute_id.name == 'Color':
                if l.attr_sku_id:
                    value_name = l.attr_sku_id[0:2]
            if l.attribute_id.name == 'Size':
                if l.attr_sku_id:
                    att_name = l.attr_sku_id[0:2]
        sku_title = str(name)+str(categ_name)+str(product_name)+' '+str(value_name)+str(att_name)
        self.pool.get('product.product').write(cr, uid, [res], {'default_code' :sku_title})
        return res
    
class product_category(osv.osv):
    _name = _inherit = "product.category"
    _columns = {
    'category_sku': fields.char('Code', size=3,required=True),
    }
    _sql_constraints = [
        ('value_company_uniq', 'unique (category_sku)', 'This Name SKU value already exists !')
    ]
    
class product_attribute_value(osv.osv):
    _name = "product.attribute.value"
    _order = 'sequence'
    def _get_price_extra(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, 0)
        if not context.get('active_id'):
            return result

        for obj in self.browse(cr, uid, ids, context=context):
            for price_id in obj.price_ids:
                if price_id.product_tmpl_id.id == context.get('active_id'):
                    result[obj.id] = price_id.price_extra
                    break
        return result

    def _set_price_extra(self, cr, uid, id, name, value, args, context=None):
        if context is None:
            context = {}
        if 'active_id' not in context:
            return None
        p_obj = self.pool['product.attribute.price']
        p_ids = p_obj.search(cr, uid, [('value_id', '=', id), ('product_tmpl_id', '=', context['active_id'])], context=context)
        if p_ids:
            p_obj.write(cr, uid, p_ids, {'price_extra': value}, context=context)
        else:
            p_obj.create(cr, uid, {
                    'product_tmpl_id': context['active_id'],
                    'value_id': id,
                    'price_extra': value,
                }, context=context)

    def name_get(self, cr, uid, ids, context=None):
        if context and not context.get('show_attribute', True):
            return super(product_attribute_value, self).name_get(cr, uid, ids, context=context)
        res = []
        for value in self.browse(cr, uid, ids, context=context):
            res.append([value.id, "%s: %s" % (value.attribute_id.name, value.name)])
        return res

    _columns = {
        'sequence': fields.integer('Sequence', help="Determine the display order"),
        'name': fields.char('Value', translate=True, required=True),
        'attr_sku_id': fields.char('Attr SKU', size=2,required=True),
        'attribute_id': fields.many2one('product.attribute', 'Attribute', required=True, ondelete='cascade'),
        'product_ids': fields.many2many('product.product', id1='att_id', id2='prod_id', string='Variants', readonly=True),
        'price_extra': fields.function(_get_price_extra, type='float', string='Attribute Price Extra',
            fnct_inv=_set_price_extra,
            digits_compute=dp.get_precision('Product Price'),
            help="Price Extra: Extra price for the variant with this attribute value on sale price. eg. 200 price extra, 1000 + 200 = 1200."),
        'price_ids': fields.one2many('product.attribute.price', 'value_id', string='Attribute Prices', readonly=True),
    }
    _sql_constraints = [
        ('value_company_uniq', 'unique (name,attribute_id,attr_sku_id)', 'This attribute value already exists !')
    ]
    _defaults = {
        'price_extra': 0.0,
    }
    def unlink(self, cr, uid, ids, context=None):
        ctx = dict(context or {}, active_test=False)
        product_ids = self.pool['product.product'].search(cr, uid, [('attribute_value_ids', 'in', ids)], context=ctx)
        if product_ids:
            raise osv.except_osv(_('Integrity Error!'), _('The operation cannot be completed:\nYou trying to delete an attribute value with a reference on a product variant.'))
        return super(product_attribute_value, self).unlink(cr, uid, ids, context=context)





# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
