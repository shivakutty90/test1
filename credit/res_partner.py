# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields,osv

class res_partner(osv.osv):
    _inherit = 'res.partner'

    def _show_credit_amount1(self, cr, uid, ids, field_name, arg, context=None):
        print '111111111111111111111111111'
        res = {}
        credit_amount_obj = self.pool.get('credit.updates')
        #~ update_amount = credit_amount_obj.self.browse(cr, uid, ids[0], context=context).update_amount
        #~ print update_amount, 
        for record in self.browse(cr, uid, ids, context=context):
            print '-------------------------------------------'
            credit_amount_obj1 = credit_amount_obj.search(cr, uid, [('name','=',record.id)])
            total = 0
            for rec in credit_amount_obj.browse(cr, uid, credit_amount_obj1, context=None):
                if rec.credit_amount:
                    total += int(rec.credit_amount)
            res[record.id] = total
        return res

    _columns = {
    
        'show_credit_amount': fields.function(_show_credit_amount1, string='# of Sales Order', type='integer')
        #~ 'sale_order_ids': fields.one2many('sale.order','partner_id','Sales Order')
    }
