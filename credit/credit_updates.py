# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _

class credit_updates(osv.osv):
    _name = 'credit.updates'
    _description = 'update the credit balance'

    _columns = {
    'name': fields.many2one('res.partner', string = 'Name'),
    'credit_amount': fields.char('Credit'),
    'update_amount': fields.char('Update'),
    }
    
    def credit_update(self, cr, uid, ids, context=None):
        update_amount = self.browse(cr, uid, ids[0], context=context).update_amount
        print update_amount, '22222222222222222222222222222222222222222222222222'
        return self.write(cr, uid, ids, {'credit_amount': update_amount}, context=context)
    
    
