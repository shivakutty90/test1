
import werkzeug
import itertools
from openerp import SUPERUSER_ID
from openerp import http
from openerp.http import request
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
from openerp.addons.web.controllers.main import login_redirect

from openerp.addons.website_sale.controllers.main import website_sale as Home
from openerp.addons.website_sale.controllers.main import QueryURL as QueryURL
import openerp.addons.website_sale.controllers.main as inherited_sale

PPG = 20 # Products Per Page
PPR = 4  # Products Per Row

class xx_website_sale(Home):

    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        domain = self._get_search_domain(search, category, attrib_values)

        keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list)

        if not context.get('pricelist'):
            pricelist = self.get_pricelist()
            context['pricelist'] = int(pricelist)
        else:
            pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)

        product_obj = pool.get('product.template')

        url = "/shop"
        product_count = product_obj.search_count(cr, uid, domain, context=context)
        if search:
            post["search"] = search
        if category:
            category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
            url = "/shop/category/%s" % slug(category)
        if attrib_list:
            post['attrib'] = attrib_list
        pager = request.website.pager(url=url, total=product_count, page=page, step=PPG, scope=7, url_args=post)
        product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        products = product_obj.browse(cr, uid, product_ids, context=context)
        style_obj = pool['product.style']
        style_ids = style_obj.search(cr, uid, [], context=context)
        styles = style_obj.browse(cr, uid, style_ids, context=context)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        attributes_obj = request.registry['product.attribute']
        attributes_ids = attributes_obj.search(cr, uid, [], context=context)
        attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)

        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)
        thead = []
        rows = []
        for product in products:
            p_value_ids = []
            p_value_ids_variant_id = []
            if product.attribute_line_ids and product.sale_ok:
                # table HEAD start
                thlength = len(product.attribute_line_ids[0].value_ids)
                first_att_values = [str(x.name) for x in product.attribute_line_ids[0].value_ids]   # add atribute value names for relevant product
                first_att_values.insert(0, str(product.attribute_line_ids[0].attribute_id.name))    # add attribute name
                first_att_values.insert(0, product.id)                                              # add product id in index 0
                thead.append(first_att_values)
                # table HEAD stop
                # table rows start

                attribute_value_ids = self.get_attribute_value_ids(product)
                p_att_line_ids = product.attribute_line_ids                
                for x in p_att_line_ids:
                    p_value_ids.append([y.id for y in x.value_ids])
                tmp_compinations_list_of_tuple = list(itertools.product(*p_value_ids))
                p_att_value_compinations = map(list, tmp_compinations_list_of_tuple)
                tmp_compination_list = []
                for x in p_att_value_compinations:
                    for i in xrange(0,len(attribute_value_ids)):
                        if set(x) == set(attribute_value_ids[i][1]):
                            tmp_rows = [x]
                            tmp_rows.insert(0,int(attribute_value_ids[i][0]))
                            tmp_compination_list.append(tmp_rows)
                trlength = len(tmp_compination_list)/thlength
                final_tmp_compination_list = list(zip(*[tmp_compination_list[i:i+trlength] for i in range(0, len(tmp_compination_list), trlength)]))                                          
                rows_list = map(list, final_tmp_compination_list)
                for row in rows_list:
                    at_value = pool['product.attribute.value'].browse(cr,uid,row[0][1][1:],context=None)
                    at_value_names = [str(x.name) for x in at_value]
                    y_axis_compinations = ""
                    for i in xrange(0,len(at_value_names)):
                        if i == 0:
                            y_axis_compinations += str(at_value_names[i])
                        else:
                            y_axis_compinations += '/'+str(at_value_names[i])

                    for i in xrange(0, len(row)):
                        del row[i][1:]
                    tr = list(itertools.chain(*row))
                    tr.insert(0,y_axis_compinations)
                    tr.insert(0,int(product.id))
                    rows.append(tr)           
        values = {
            'thead': thead,
            'trows': rows,
            'search': search,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'bins': inherited_sale.table_compute().process(products),
            'rows': PPR,
            'styles': styles,
            'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib',i) for i in attribs]),
        }
        return request.website.render("website_sale.products", values)

    @http.route(['/shop/get_sale_price'], type='json', auth="public", methods=['POST'], website=True)
    def get_sale_price(self, product_id, use_order_pricelist=False, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        product = pool['product.product'].browse(cr, uid, product_id, context=context)
        partner = pool['res.users'].browse(cr, uid, uid, context=context).partner_id
        add_qty = 0
        if use_order_pricelist:
            pricelist_id = request.session.get('sale_order_code_pricelist_id') or partner.property_product_pricelist.id
        else:
            pricelist_id = partner.property_product_pricelist.id
            # TO-DO simplyfy below lines products to-> product
        prices = pool['product.pricelist'].price_rule_get_multi(cr, uid, [], [(product, add_qty, partner)], context=context)
        return prices[product_id][pricelist_id][0]          

    @http.route(['/shop/cart/confirm_qty'], type='json', auth="public", methods=['POST'], website=True)
    def cart_confirm_qty(self, cart_list, display=True):        
        order = request.website.sale_get_order(force_create=1)
        values = []
        for item in cart_list:
            value = order._cart_update(product_id=item[0], line_id='', add_qty=False, set_qty=item[1])
            value['cart_quantity'] = order.cart_quantity
            value['website_sale.total'] = request.website._render("website_sale.total", {
                    'website_sale_order': request.website.sale_get_order()
                })            
            values.append(value)
        return values