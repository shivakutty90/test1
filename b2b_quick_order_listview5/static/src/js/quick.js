$(document).ready(function () {

    openerp.set_cookie = function(name, value, ttl) {
        ttl = ttl || 24*60*60*365;
        document.cookie = [
            name + '=' + value,
            'path=/shop',
            'max-age=' + ttl,
            'expires=' + new Date(new Date().getTime() + ttl*1000).toGMTString()
        ].join(';');
    };
    
    openerp.delete_cookie = function(name) {
        document.cookie = [
            name + '=' + '',
            'path=/shop',
        ].join(';');
    };

    var session = openerp.get_cookie('cart_items');
    if (session){
        cart_items = JSON.parse(session);
        console.log(cart_items);
    }else{cart_items = {}}

    // Hide confirm quantities button in matrix list view
    if (!($("#products_grid").find(".variant_matrix").length > 0)){ 
      $(this).find(".matrix_confirm_qty").addClass("hidden");
    }

    if (cart_items){
        _.each(cart_items, function(value, key){
            $('.'+key).prop("value", value['qtys']); 
        });
    }

    total_qtys = 0;
    _.each(cart_items, function(value, key){
            total_qtys += value['qtys'];
    });

    total_amount = 0;
    _.each(cart_items, function(value, key){
            total_amount += value['price'];
    });

    $('.tqty').find('span').text(total_qtys);   /*set total quantities to matrix list view*/
    $('.total_amount').find('span').text(total_amount);  /*set total prices to matrix list view*/   



    // Calculate total quantities and total amounts, collect variant ids for creating order
    $(".js_quantity1").change(function() {
        var qty = parseInt($(this).val(), 10);
        var variant_id = parseInt($(this).data('varient-id'),10);
        openerp.jsonRpc("/shop/get_sale_price", 'call', {'product_id': variant_id}).then(function (price) {
                if (isNaN(qty)){
                    delete cart_items[variant_id];
                    }else{
                            cart_items[variant_id] = {'qtys':qty,'price':price*qty};
                    }

            total_qtys = 0;
            _.each(cart_items, function(value, key){
                    total_qtys += value['qtys'];
            });

            total_amount = 0;
            _.each(cart_items, function(value, key){
                    total_amount += value['price'];
            });

            $('.tqty').find('span').text(total_qtys);   /*set total quantities to matrix list view*/
            $('.total_amount').find('span').text(total_amount);  /*set total prices to matrix list view*/           
            openerp.set_cookie('cart_items', JSON.stringify(cart_items), 3600*3600);
        });

    });

    $(this).on('click', '.xx_confirm_qtys', function (ev) {
        ev.preventDefault();
        tmp_list = [];
            $(".js_quantity1").each(function() {
                var value = parseInt($(this).val(), 10);
                if (value){
                    var product_id = parseInt($(this).data('varient-id'),10); 
                    
                    tmp_list.push([product_id, value]);
                    $(this).val('');        // Clearing entered quantities while confirming qtys.
                }
        });     
        if (tmp_list){
                openerp.jsonRpc("/shop/cart/confirm_qty", 'call', {'cart_list': tmp_list}).then(function (result) {
                    _.each(result, function(data){
                        var $q = $(".my_cart_quantity");
                        $q.parent().parent().removeClass("hidden", !data.quantity);
                        $q.html(data.cart_quantity).hide().fadeIn(600);
                        $("#cart_total").replaceWith(data['website_sale.total']);
                    }); 
                });
        }    
        $('.tqty').find('span').text('');   /*clear total quantities to matrix list view*/
        $('.total_amount').find('span').text('');    /*clear total prices to matrix list view*/     
        openerp.delete_cookie('cart_items');    // Delete cart_items in cookies
        // location.reload();
    }); 
// Fixed table sidebar and header start

    $('.fixedTable-body').scroll(function () {
        $(this).prev().find('table').css('margin-top', -$($(this)).scrollTop());
        $(this).prev().prev().find('table').css('margin-left', -$($(this)).scrollLeft());
    });

// Fixed table sidebar and header end

// popup matrix table start

// popup matrix table start

// Only number enterable in matrix table input field start
     $(".js_quantity1").keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           return false;
        }
        });
// Only number enterable in matrix table input field end

// css for matrix table scroll start
    $(".fixedTable").each(function(){
        var fixedTablebodyWidth = $(this).find(".fixedTable-body table").width();
        
        $(this).children(".fixedTable-body").css("width",fixedTablebodyWidth + 20);
        $(this).children(".fixedTable-header").css("width",fixedTablebodyWidth + 20);

        var fixedTablesidebarWidth = $(this).find('.fixedTable-sidebar').width();
        $(this).children(".first_child").css("width",fixedTablesidebarWidth);        
        $(this).children(".fixedTable-header").css("margin-left",fixedTablesidebarWidth);

    });
// css for matrix table scroll end

// Sale price hover start
$('.oe_website_sale').each(function () {
    var oe_website_sale = this;
    $(oe_website_sale).find(".tiptext").mouseenter(function() {
        var $input = $(this);
        var product_id = $input.data('varient-id');

        openerp.jsonRpc("/shop/get_sale_price", 'call', {'product_id': product_id})
        .then(function (price) {
            $("."+product_id).html("Sale Price: "+price);
            $(oe_website_sale).find("."+product_id).show();
            });
    }).mouseleave(function() {
        $(oe_website_sale).find(".hover_price").hide();
    });
    });

});
// Sale price hover end
    $('.xx_fullscreen').click(function(){

        var test = $(this).parents("section").children(".variant_matrix").html();
        var arr = ['Jothi', 'Mani']
        var dynamic_table = "<table class='testtable'>
                                <thead><tr></tr></thead>
                                <tbody><tr></tr></tbody>

        </tabe>"
        $("#cart_matrix .popup_matrix").html(dynamic_table);

        for(i = 0; i< arr.length; i++){
            $(".testtable thead tr").append("<th>"+arr[i]+"</th>")
            $(".testtable tbody tr").append("<td>"+arr[i]+"</td>")
        }

    });

$(document).ready(function(){
    $("input").each(function() {
        $(this).keyup(function(){
            newSum.call(this);
        });
    });
});

function newSum() {
    var sum = 0;
    var thisRow = $(this).closest('tr');
    //iterate through each input and add to sum
    $(thisRow).find("td:not(.total) input").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                    $(this).css("background-color", "#FEFFB0");
                }
                else if (this.value.length != 0){
                $(this).css("background-color", "red");
               } 
    });

    //change value of total
    $(thisRow).find(".total").html(sum);
    
}

$(document).ready(function(){
    $("input").each(function() {
        $(this).keyup(function(){
            newSum1.call(this);
        });
    });
});

function newSum1() {
    var sum = 0;
    var thisRow = $(this).closest('td');
    //iterate through each input and add to sum
    $(thisRow).find("tr:not(.total1) input").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                    $(this).css("background-color", "#FEFFB0");
                }
                else if (this.value.length != 0){
                $(this).css("background-color", "red");
               } 
    });

    //change value of total
    $(thisRow).find(".total1").html(sum);
    
}
