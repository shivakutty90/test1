# -*- coding: utf-8 -*-
from openerp.osv import fields, osv

class product_uom(osv.osv):
	_inherit = 'product.uom'
	_columns = {
		'approved_supplier': fields.boolean('Link to Approved Supplier'),
		'mode_type': fields.selection(
                [('bottle', 'Bottle'),('case', 'Case'),('pack', 'Pack')],
                'Mode Type')
	}

