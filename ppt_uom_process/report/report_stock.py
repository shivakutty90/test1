# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.sql import drop_view_if_exists

class report_stock_move_ppt(osv.osv):
    _name = "report.stock.move.ppt"
    _auto = False
    _order = "date_of_inward"
    _columns = {
        'id': fields.integer('Move Id', readonly=True),
        'date_of_inward': fields.datetime('Date of Inward', readonly=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', readonly=True, select=True),
        'location_id': fields.many2one('stock.location', 'Location', readonly=True, select=True),
        'partner_id': fields.many2one('res.partner', 'Supplier Name', readonly=True, select=True),
        'product_id': fields.many2one('product.product', 'Product', readonly=True, select=True),
        'no_of_bottles': fields.float('No.of Each'),
        'price_per_bottle': fields.float('PRICE / Each'),
        #~ 'no_of_pack': fields.float('No.of.Pack'),
        #~ 'price_per_pack': fields.float('Price Per Pack'),
        'no_of_case_or_pack': fields.float('No.of CASE / PACK'),
        'price_per_case_or_pack': fields.float('PRICE / CASE / PACK'),
        'package_reference': fields.char('Package Reference'),
        'inventry_value': fields.float('Total Inventry Value'),
        'move_id': fields.many2one('stock.move', 'Move Ref', readonly=True, select=True),
        'po_mode': fields.many2one('product.uom', 'PO Mode'),
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'report_stock_move_ppt')
        cr.execute("""
            CREATE OR REPLACE VIEW report_stock_move_ppt AS (
              SELECT id as id,
                move_id,
                location_id,
                warehouse_id,
                product_id,
                partner_id,
                date_of_inward,
                no_of_bottles,
                price_per_bottle,                
                no_of_case_or_pack,
                price_per_case_or_pack,
                inventry_value,
                po_mode
                FROM
                ((SELECT
                    stock_move.id AS id,
                    stock_move.id AS move_id,
                    dest_location.id AS location_id,
                    stock_move.warehouse_id AS warehouse_id,
                    stock_move.product_id AS product_id,
                    picking.partner_id As partner_id,
                    quant.qty AS quantity,
                    stock_move.date AS date_of_inward,
                    CASE WHEN uom.mode_type = 'bottle' THEN stock_move.uom_qty 
                        ELSE po_line.product_qty
                    END AS no_of_bottles,
                    CASE WHEN uom.mode_type = 'bottle' THEN stock_move.uom_price 
                        ELSE po_line.price_unit
                    END AS price_per_bottle,                    
                    CASE WHEN uom.mode_type in ('case', 'pack') THEN stock_move.uom_qty 
                        ELSE '0'
                    END AS no_of_case_or_pack,
                    CASE WHEN uom.mode_type in ('case', 'pack') THEN stock_move.uom_price 
                        ELSE '0'
                    END AS price_per_case_or_pack,
                    stock_move.total_uom_price AS inventry_value,
                    po_line.uom_mode AS po_mode
                FROM
                    stock_move
                JOIN
                    product_uom as uom on uom.id = stock_move.uom_mode
                JOIN
                    stock_picking as picking on picking.id = stock_move.picking_id
                JOIN
                    stock_picking_type as picking_type on picking_type.id = picking.picking_type_id
                JOIN
                    stock_quant_move_rel on stock_quant_move_rel.move_id = stock_move.id
                JOIN
                    stock_quant as quant on stock_quant_move_rel.quant_id = quant.id
                JOIN
                   stock_location dest_location ON stock_move.location_dest_id = dest_location.id
                JOIN
                    stock_location source_location ON stock_move.location_id = source_location.id
                JOIN
                    product_product ON product_product.id = stock_move.product_id
                JOIN
                    product_template ON product_template.id = product_product.product_tmpl_id
                JOIN
                    purchase_order_line as po_line ON po_line.id = stock_move.purchase_line_id
                WHERE quant.qty>0 AND stock_move.state = 'done' AND dest_location.usage in ('internal', 'transit') AND picking_type.code = 'incoming'
                  AND (
                    (source_location.company_id is null and dest_location.company_id is not null) or
                    (source_location.company_id is not null and dest_location.company_id is null) or
                    source_location.company_id != dest_location.company_id or
                    source_location.usage not in ('internal', 'transit'))
                ))
                AS foo1
                GROUP BY id, move_id, location_id, partner_id, warehouse_id, product_id, date_of_inward, no_of_bottles, price_per_bottle, no_of_case_or_pack, price_per_case_or_pack, inventry_value, po_mode
            )""")


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
