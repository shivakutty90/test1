# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP

class purchase_order(osv.osv):
    _inherit = 'purchase.order'

    def _create_stock_moves(self, cr, uid, order, order_lines, picking_id=False, context=None):
        """Creates appropriate stock moves for given order lines, whose can optionally create a
        picking if none is given or no suitable is found, then confirms the moves, makes them
        available, and confirms the pickings.

        If ``picking_id`` is provided, the stock moves will be added to it, otherwise a standard
        incoming picking will be created to wrap the stock moves (default behavior of the stock.move)

        Modules that wish to customize the procurements or partition the stock moves over
        multiple stock pickings may override this method and call ``super()`` with
        different subsets of ``order_lines`` and/or preset ``picking_id`` values.

        :param browse_record order: purchase order to which the order lines belong
        :param list(browse_record) order_lines: purchase order line records for which picking
                                                and moves should be created.
        :param int picking_id: optional ID of a stock picking to which the created stock moves
                               will be added. A new picking will be created if omitted.
        :return: None
        """
        stock_move = self.pool.get('stock.move')
        todo_moves = []
        new_group = self.pool.get("procurement.group").create(cr, uid, {'name': order.name, 'partner_id': order.partner_id.id}, context=context)

        for order_line in order_lines:
            if order_line.state == 'cancel':
                continue
            if not order_line.product_id:
                continue

            if order_line.product_id.type in ('product', 'consu'):
                for vals in self._prepare_order_line_move(cr, uid, order, order_line, picking_id, new_group, context=context):
                    vals.update({'uom_qty':order_line.uom_qty})
                    vals.update({'uom_price':order_line.uom_based_price})
                    move = stock_move.create(cr, uid, vals, context=context)
                    todo_moves.append(move)

        todo_moves = stock_move.action_confirm(cr, uid, todo_moves)
        stock_move.force_assign(cr, uid, todo_moves)

    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        """Collects require data from purchase order line that is used to create invoice line
        for that purchase order line
        :param account_id: Expense account of the product of PO line if any.
        :param browse_record order_line: Purchase order line browse record
        :return: Value for fields of invoice lines.
        :rtype: dict
        """
        return {
            'name': order_line.name,
            'account_id': account_id,
            'price_unit': order_line.price_unit or 0.0,
            'quantity': order_line.product_qty,
            'product_id': order_line.product_id.id or False,
            'uos_id': order_line.product_uom.id or False,
            'invoice_line_tax_id': [(6, 0, [x.id for x in order_line.taxes_id])],
            'account_analytic_id': order_line.account_analytic_id.id or False,
            'purchase_line_id': order_line.id,
            'uom_based_price':order_line.uom_based_price,
            'uom_mode':order_line.uom_mode.id,
            'uom_qty':order_line.uom_qty,
        }

class purchase_order_line(osv.osv):
    _inherit = 'purchase.order.line'
    
    _columns = {  
        'product_id': fields.many2one('product.product', 'Product', domain=[('purchase_ok','=',True)]),
        'uom_mode': fields.many2one('product.uom', 'Mode', domain = [('approved_supplier','=',True)]),
        'uom_based_price': fields.float('Pack/Case Price', digits_compute= dp.get_precision('Product Price'),),
        'uom_qty': fields.float('Pack/Case Qty', digits_compute= dp.get_precision('Product UoS')),
    }   
    
    def _get_uom_mode_id(self, cr, uid, context=None):
        res = self.pool.get('product.uom').search(cr, uid, [('approved_supplier','=',True), ('mode_type','=','bottle')], context=context)
        if res:
            return res[0]
        else:
            return False
    
    _defaults = {
        'uom_mode' : _get_uom_mode_id,
        'uom_based_price': 0.0,
        #~ 'uom_qty': 1.0,
        'uom_qty': lambda *a: 1.0,
    }
    
    def onchange_product_uom(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', uom_mode=False, uom_qty=False, context=None):
        """
        onchange handler of product_uom.
        """
        if context is None:
            context = {}
        if not uom_id:
            return {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        context = dict(context, purchase_uom_check=True)
        return self.onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=date_order, fiscal_position_id=fiscal_position_id, date_planned=date_planned,
            name=name, price_unit=price_unit, state=state, uom_mode=uom_mode, uom_qty=uom_qty, context=context)
    
    def onchange_product_uom_mode(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', uom_mode=False, uom_qty=False, context=None):
        """
        onchange handler of uom_mode.
        """
        if context is None:
            context = {}
        if not uom_mode:
            return {'value': {'price_unit': 0.0, 'uom_qty': 0.0, 'product_qty':0.0}}        
        return self.onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=date_order, fiscal_position_id=fiscal_position_id, date_planned=date_planned,
            name=name, price_unit=price_unit, state=state, uom_mode=uom_mode, uom_qty=uom_qty, context=context)
    
    def onchange_product_uom_qty(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', uom_mode=False, uom_qty=False, context=None):
        """
        onchange handler of uom_mode.
        """
        if context is None:
            context = {}
        if not uom_qty:
            return {'value': {'price_unit': 0.0, 'uom_qty': 0.0, 'product_qty':0.0}}        
        return self.onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=date_order, fiscal_position_id=fiscal_position_id, date_planned=date_planned,
            name=name, price_unit=price_unit, state=state, uom_mode=uom_mode, uom_qty=uom_qty, context=context)
    
    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', uom_mode=False,
            uom_qty=False, context=None):
        """
        onchange handler of product_id.
        """
        if context is None:
            context = {}
        res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        if not product_id:
            return res
        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')
        res_partner = self.pool.get('res.partner')
        product_pricelist = self.pool.get('product.pricelist')
        account_fiscal_position = self.pool.get('account.fiscal.position')
        account_tax = self.pool.get('account.tax')

        # - check for the presence of partner_id and pricelist_id
        #if not partner_id:
        #    raise osv.except_osv(_('No Partner!'), _('Select a partner in purchase order to choose a product.'))
        #if not pricelist_id:
        #    raise osv.except_osv(_('No Pricelist !'), _('Select a price list in the purchase order form before choosing a product.'))

        # - determine name and notes based on product in partner lang.
        context_partner = context.copy()
        if partner_id:
            lang = res_partner.browse(cr, uid, partner_id).lang
            context_partner.update( {'lang': lang, 'partner_id': partner_id} )
        product = product_product.browse(cr, uid, product_id, context=context_partner)
        #call name_get() with partner in the context to eventually match name and description in the seller_ids field
        dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
        if product.description_purchase:
            name += '\n' + product.description_purchase
        res['value'].update({'name': name})

        # - set a domain on product_uom
        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}

        # - check that uom and product uom belong to the same category
        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id

        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
                res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        # - determine product_qty and date_planned based on seller info
        if not date_order:
            date_order = fields.datetime.now()


        supplierinfo = False   
        precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure')
        qty_vals = {}
        for supplier in product.seller_ids:
            if partner_id and (supplier.name.id == partner_id):
                supplierinfo = supplier
                if uom_mode:
                    if supplier.uom_mode.mode_type == 'bottle':
                        qty_vals['b_qty'] = supplier.no_of_pcs  
                    elif supplier.uom_mode.mode_type == 'case':
                        qty_vals['c_qty'] = supplier.no_of_pack_per_case    
                    elif supplier.uom_mode.mode_type == 'pack':
                        qty_vals['p_qty'] = supplier.no_of_pcs_per_pack     
                        
                #~ if supplierinfo.product_uom.id != uom_id:
                    #~ res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
                #~ min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
                #~ if float_compare(min_qty , qty, precision_digits=precision) == 1: # If the supplier quantity is greater than entered from user, set minimal.
                    #~ if qty:
                        #~ res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
                    #~ qty = min_qty
        uom_mode_br = product_uom.browse(cr, uid, uom_mode)
        if uom_mode_br.mode_type == 'bottle' and qty_vals.get('b_qty'):
            qty = qty_vals.get('b_qty') * uom_qty
        elif uom_mode_br.mode_type == 'pack' and qty_vals.get('p_qty'):
            qty = qty_vals.get('p_qty') * uom_qty
        elif uom_mode_br.mode_type == 'case' and qty_vals.get('c_qty') and qty_vals.get('p_qty'):            
            qty = (qty_vals.get('c_qty') * qty_vals.get('p_qty')) * uom_qty
        else:
            qty = qty or 1.0
        dt = self._get_date_planned(cr, uid, supplierinfo, date_order, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)        
        res['value'].update({'date_planned': date_planned or dt})
        res['value'].update({'uom_qty': uom_qty})
        if qty:
            res['value'].update({'product_qty': qty})

        price = price_unit
        if price_unit is False or price_unit is None:
            # - determine price_unit and taxes_id
            if pricelist_id:
                date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
                
                price = product_pricelist.price_get(cr, uid, [pricelist_id],
                        product.id, qty or 1.0, partner_id or False, {'uom': uom_id, 'date': date_order_str, 'uom_mode':uom_mode})[pricelist_id]
                res['value'].update({'uom_based_price': price})
                if partner_id and qty and product:
                    price = (price * uom_qty)/ qty
            else:
                price = product.standard_price

        taxes = account_tax.browse(cr, uid, map(lambda x: x.id, product.supplier_taxes_id))
        fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
        taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes)
        res['value'].update({'price_unit': price, 'taxes_id': taxes_ids})
        return res
    
