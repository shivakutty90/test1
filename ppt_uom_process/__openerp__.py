# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'UOM Process',
    'version': '1.0',
    'author': 'PPTS',
    'category': 'Purchase Management',
    'depends': ['base','product','purchase','report','stock','stock_account','account'],
    'website': 'https://www.odoo.com',
    'description': """
This is the base module for managing products and pricelists in OpenERP.
========================================================================

Products support variants, different pricing methods, suppliers information,
make to stock/order, different unit of measures, packaging and properties.

Pricelists support:
-------------------
    * Multiple-level of discount (by product, category, quantities)
    * Compute price based on different criteria:
        * Other pricelist
        * Cost price
        * List price
        * Supplier price

Pricelists preferences by product and/or partners.

Print product labels with barcode.
    """,
    'data': [
        'wizard/stock_transfer_details.xml',          
        #~ 'views/report_picking_ppt.xml',
        'product_uom_view.xml',
        'product_view.xml',
        'pricelist_view.xml',
        'purchase_view.xml',
        'account_invoice_view.xml',
        'stock_view.xml',
        'report/report_stock_view.xml',      
        #~ 'stock_report.xml',
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
