# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime
from openerp import SUPERUSER_ID, api
from openerp.tools.float_utils import float_compare, float_round


class stock_pack_operation(osv.osv):
    _inherit = "stock.pack.operation"
    _description = "Packing Operation"
    
    _columns = {  
        'uom_mode': fields.many2one('product.uom', 'Mode', domain = [('approved_supplier','=',True)]),
        'uom_qty': fields.float('Pack/Case Qty', digits_compute= dp.get_precision('Product UoS')),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'move_id': fields.many2one('stock.move', 'Move'),
    }   
    
    def _get_uom_mode_id(self, cr, uid, context=None):
        res = self.pool.get('product.uom').search(cr, uid, [('approved_supplier','=',True), ('mode_type','=','bottle')], context=context)
        if res:
            return res[0]
        else:
            return False
    
    _defaults = {
        'uom_mode' : _get_uom_mode_id,
        'uom_qty': lambda *a: 1.0,
    }

class stock_move(osv.osv):
    _inherit = 'stock.move'
    
    def _uom_price_calc(self, cursor, user, ids, name, arg, context=None):
        res = {}
        for move in self.browse(cursor, user, ids, context=context):
            res[move.id] = move.uom_qty * move.uom_price
        return res
        
    _columns = {  
        'uom_mode': fields.related('purchase_line_id', 'uom_mode', type='many2one', relation='product.uom', string="UOM Mode", store=True),
        #~ 'uom_qty': fields.related('purchase_line_id', 'uom_qty', type='float', string="Qty", store=True),
        'uom_qty': fields.float('Pack/Case Qty', digits_compute= dp.get_precision('Product UoS')),
        'remaining_uom_qty': fields.float('Remaining UOM Quantity'),
        'uom_price': fields.float('UOM Price'),
        'total_uom_price': fields.function(_uom_price_calc, string='Total UOM Price', type='float', store=True),
    } 
    
    def split(self, cr, uid, move, qty, restrict_lot_id=False, restrict_partner_id=False, context=None):
        """ Splits qty from move move into a new move
        :param move: browse record
        :param qty: float. quantity to split (given in product UoM)
        :param restrict_lot_id: optional production lot that can be given in order to force the new move to restrict its choice of quants to this lot.
        :param restrict_partner_id: optional partner that can be given in order to force the new move to restrict its choice of quants to the ones belonging to this partner.
        :param context: dictionay. can contains the special key 'source_location_id' in order to force the source location when copying the move

        returns the ID of the backorder move created
        """
        if move.state in ('done', 'cancel'):
            raise osv.except_osv(_('Error'), _('You cannot split a move done'))
        if move.state == 'draft':
            #we restrict the split of a draft move because if not confirmed yet, it may be replaced by several other moves in
            #case of phantom bom (with mrp module). And we don't want to deal with this complexity by copying the product that will explode.
            raise osv.except_osv(_('Error'), _('You cannot split a draft move. It needs to be confirmed first.'))

        if move.product_qty <= qty or qty == 0:
            return move.id

        uom_obj = self.pool.get('product.uom')
        context = context or {}
        r_uom_qty = context.get('remaining_uom_qty') or 0.0
        #HALF-UP rounding as only rounding errors will be because of propagation of error from default UoM
        uom_qty = uom_obj._compute_qty_obj(cr, uid, move.product_id.uom_id, qty, move.product_uom, rounding_method='HALF-UP', context=context)
        uos_qty = uom_qty * move.product_uos_qty / move.product_uom_qty

        defaults = {
            'product_uom_qty': uom_qty,
            'product_uos_qty': uos_qty,
            'procure_method': 'make_to_stock',
            'restrict_lot_id': restrict_lot_id,
            'split_from': move.id,
            'procurement_id': move.procurement_id.id,
            'move_dest_id': move.move_dest_id.id,
            'origin_returned_move_id': move.origin_returned_move_id.id,
            'uom_qty':r_uom_qty,
        }

        if restrict_partner_id:
            defaults['restrict_partner_id'] = restrict_partner_id

        if context.get('source_location_id'):
            defaults['location_id'] = context['source_location_id']
        new_move = self.copy(cr, uid, move.id, defaults, context=context)
        self.write(cr, uid, [new_move], {'uom_qty':r_uom_qty})
        ctx = context.copy()
        ctx['do_not_propagate'] = True
        self.write(cr, uid, [move.id], {
            'product_uom_qty': move.product_uom_qty - uom_qty,
            'product_uos_qty': move.product_uos_qty - uos_qty,
            'uom_qty':move.uom_qty - r_uom_qty,
        }, context=ctx)

        if move.move_dest_id and move.propagate and move.move_dest_id.state not in ('done', 'cancel'):
            new_move_prop = self.split(cr, uid, move.move_dest_id, qty, context=context)
            self.write(cr, uid, [new_move], {'move_dest_id': new_move_prop}, context=context)
        #returning the first element of list returned by action_confirm is ok because we checked it wouldn't be exploded (and
        #thus the result of action_confirm should always be a list of 1 element length)
        return self.action_confirm(cr, uid, [new_move], context=context)[0]
    
    def _get_invoice_line_vals(self, cr, uid, move, partner, inv_type, context=None):
        vals = {}
        fp_obj = self.pool.get('account.fiscal.position')
        # Get account_id
        if inv_type in ('out_invoice', 'out_refund'):
            account_id = move.product_id.property_account_income.id
            if not account_id:
                account_id = move.product_id.categ_id.property_account_income_categ.id
        else:
            account_id = move.product_id.property_account_expense.id
            if not account_id:
                account_id = move.product_id.categ_id.property_account_expense_categ.id
            vals['uom_based_price'] = move.uom_price
            vals['uom_qty'] = move.uom_qty
            vals['uom_mode'] = move.uom_mode.id
        fiscal_position = partner.property_account_position
        account_id = fp_obj.map_account(cr, uid, fiscal_position, account_id)

        # set UoS if it's a sale and the picking doesn't have one
        uos_id = move.product_uom.id
        quantity = move.product_uom_qty
        if move.product_uos:
            uos_id = move.product_uos.id
            quantity = move.product_uos_qty
        vals.update({
            'name': move.name,
            'account_id': account_id,
            'product_id': move.product_id.id,
            'uos_id': uos_id,
            'quantity': quantity,
            'price_unit': self._get_price_unit_invoice(cr, uid, move, inv_type),
            'discount': 0.0,
            'account_analytic_id': False,
        })
        return vals

    #~ def _get_invoice_line_vals(self, cr, uid, move, partner, inv_type, context=None):
        #~ vals = {}
        #~ fp_obj = self.pool.get('account.fiscal.position')
        #~ # Get account_id
        #~ if inv_type in ('out_invoice', 'out_refund'):
            #~ account_id = move.product_id.property_account_income.id
            #~ if not account_id:
                #~ account_id = move.product_id.categ_id.property_account_income_categ.id
        #~ else:
            #~ account_id = move.product_id.property_account_expense.id
            #~ if not account_id:
                #~ account_id = move.product_id.categ_id.property_account_expense_categ.id
            #~ vals['uom_based_price'] = move.uom_price
            #~ vals['uom_qty'] = move.uom_qty
            #~ vals['uom_mode'] = move.uom_mode.id
        #~ fiscal_position = partner.property_account_position
        #~ account_id = fp_obj.map_account(cr, uid, fiscal_position, account_id)
#~ 
        #~ # set UoS if it's a sale and the picking doesn't have one
        #~ uos_id = move.product_uom.id
        #~ quantity = move.product_uom_qty
        #~ if move.product_uos:
            #~ uos_id = move.product_uos.id
            #~ quantity = move.product_uos_qty
#~ 
        #~ taxes_ids = self._get_taxes(cr, uid, move, context=context)
        #~ vals.update({
            #~ 'name': move.name,
            #~ 'account_id': account_id,
            #~ 'product_id': move.product_id.id,
            #~ 'uos_id': uos_id,
            #~ 'quantity': quantity,
            #~ 'price_unit': self._get_price_unit_invoice(cr, uid, move, inv_type),
            #~ 'invoice_line_tax_id': [(6, 0, taxes_ids)],
            #~ 'discount': 0.0,
            #~ 'account_analytic_id': False,
        #~ })
        #~ return vals
        
class stock_picking(osv.osv):
    _inherit = 'stock.picking'
    
    def _prepare_pack_ops(self, cr, uid, picking, quants, forced_qties, context=None):
        """ returns a list of dict, ready to be used in create() of stock.pack.operation.

        :param picking: browse record (stock.picking)
        :param quants: browse record list (stock.quant). List of quants associated to the picking
        :param forced_qties: dictionary showing for each product (keys) its corresponding quantity (value) that is not covered by the quants associated to the picking
        """
        def _picking_putaway_apply(product):
            location = False
            # Search putaway strategy
            if product_putaway_strats.get(product.id):
                location = product_putaway_strats[product.id]
            else:
                location = self.pool.get('stock.location').get_putaway_strategy(cr, uid, picking.location_dest_id, product, context=context)
                product_putaway_strats[product.id] = location
            return location or picking.location_dest_id.id

        # If we encounter an UoM that is smaller than the default UoM or the one already chosen, use the new one instead.
        product_uom = {} # Determines UoM used in pack operations
        for move in picking.move_lines:
            if not product_uom.get(move.product_id.id):
                product_uom[move.product_id.id] = move.product_id.uom_id
            if move.product_uom.id != move.product_id.uom_id.id and move.product_uom.factor > product_uom[move.product_id.id].factor:
                product_uom[move.product_id.id] = move.product_uom

        pack_obj = self.pool.get("stock.quant.package")
        quant_obj = self.pool.get("stock.quant")
        vals = []
        qtys_grouped = {}
        #for each quant of the picking, find the suggested location
        quants_suggested_locations = {}
        product_putaway_strats = {}
        for quant in quants:
            if quant.qty <= 0:
                continue
            suggested_location_id = _picking_putaway_apply(quant.product_id)
            quants_suggested_locations[quant] = suggested_location_id

        #find the packages we can movei as a whole
        top_lvl_packages = self._get_top_level_packages(cr, uid, quants_suggested_locations, context=context)
        # and then create pack operations for the top-level packages found
        for pack in top_lvl_packages:
            pack_quant_ids = pack_obj.get_content(cr, uid, [pack.id], context=context)
            pack_quants = quant_obj.browse(cr, uid, pack_quant_ids, context=context)
            vals.append({
                    'picking_id': picking.id,
                    'package_id': pack.id,
                    'product_qty': 1.0,
                    'location_id': pack.location_id.id,
                    'location_dest_id': quants_suggested_locations[pack_quants[0]],
                    'owner_id': pack.owner_id.id,
                })
            #remove the quants inside the package so that they are excluded from the rest of the computation
            for quant in pack_quants:
                del quants_suggested_locations[quant]

        # Go through all remaining reserved quants and group by product, package, lot, owner, source location and dest location
        for quant, dest_location_id in quants_suggested_locations.items():
            key = (quant.product_id.id, quant.package_id.id, quant.lot_id.id, quant.owner_id.id, quant.location_id.id, dest_location_id)
            if qtys_grouped.get(key):
                qtys_grouped[key] += quant.qty
            else:
                qtys_grouped[key] = quant.qty

        # Do the same for the forced quantities (in cases of force_assign or incomming shipment for example)
        for product, qty in forced_qties.items():
            if qty <= 0:
                continue
            suggested_location_id = _picking_putaway_apply(product)
            key = (product.id, False, False, picking.owner_id.id, picking.location_id.id, suggested_location_id)
            if qtys_grouped.get(key):
                qtys_grouped[key] += qty
            else:
                qtys_grouped[key] = qty

        # Create the necessary operations for the grouped quants and remaining qtys
        uom_obj = self.pool.get('product.uom')
        prevals = {}
        for key, qty in qtys_grouped.items():
            product = self.pool.get("product.product").browse(cr, uid, key[0], context=context)
            uom_id = product.uom_id.id
            qty_uom = qty
            if product_uom.get(key[0]):
                uom_id = product_uom[key[0]].id
                qty_uom = uom_obj._compute_qty(cr, uid, product.uom_id.id, qty, uom_id)
            val_dict = {
                'picking_id': picking.id,
                'product_qty': qty_uom,
                'product_id': key[0],
                'package_id': key[1],
                'lot_id': key[2],
                'owner_id': key[3],
                'location_id': key[4],
                'location_dest_id': key[5],
                'product_uom_id': uom_id,
            }
            if key[0] in prevals:
                prevals[key[0]].append(val_dict)
            else:
                prevals[key[0]] = [val_dict]
        # prevals var holds the operations in order to create them in the same order than the picking stock moves if possible
        processed_products = set()
        for move in picking.move_lines:
            if move.product_id.id not in processed_products:
                data = prevals.get(move.product_id.id, [])
                if data:
                    if data[0].get('product_id'):
                        data[0]['uom_mode'] = move.uom_mode.id
                        data[0]['uom_qty'] = move.uom_qty
                        data[0]['partner_id'] = picking.partner_id.id
                        data[0]['move_id'] = move.id
                vals += data
                processed_products.add(move.product_id.id)
        return vals
    
    @api.cr_uid_ids_context
    def do_transfer(self, cr, uid, picking_ids, context=None):
        """
            If no pack operation, we do simple action_done of the picking
            Otherwise, do the pack operations
        """
        if not context:
            context = {}
        stock_move_obj = self.pool.get('stock.move')
        for picking in self.browse(cr, uid, picking_ids, context=context):
            if not picking.pack_operation_ids:
                self.action_done(cr, uid, [picking.id], context=context)
                continue
            else:
                need_rereserve, all_op_processed = self.picking_recompute_remaining_quantities(cr, uid, picking, context=context)
                #create extra moves in the picking (unexpected product moves coming from pack operations)
                todo_move_ids = []
                if not all_op_processed:
                    todo_move_ids += self._create_extra_moves(cr, uid, picking, context=context)

                #split move lines if needed
                toassign_move_ids = []
                for move in picking.move_lines:
                    remaining_qty = move.remaining_qty
                    if move.state in ('done', 'cancel'):
                        #ignore stock moves cancelled or already done
                        continue
                    elif move.state == 'draft':
                        toassign_move_ids.append(move.id)
                    if float_compare(remaining_qty, 0,  precision_rounding = move.product_id.uom_id.rounding) == 0:
                        if move.state in ('draft', 'assigned', 'confirmed'):
                            todo_move_ids.append(move.id)
                    elif float_compare(remaining_qty,0, precision_rounding = move.product_id.uom_id.rounding) > 0 and \
                                float_compare(remaining_qty, move.product_qty, precision_rounding = move.product_id.uom_id.rounding) < 0:
                        ctx = context.copy()
                        ctx['remaining_uom_qty'] = move.remaining_uom_qty
                        new_move = stock_move_obj.split(cr, uid, move, remaining_qty, context=ctx)
                        todo_move_ids.append(move.id)
                        #Assign move as it was assigned before
                        toassign_move_ids.append(new_move)
                if need_rereserve or not all_op_processed: 
                    if not picking.location_id.usage in ("supplier", "production", "inventory"):
                        self.rereserve_quants(cr, uid, picking, move_ids=todo_move_ids, context=context)
                    self.do_recompute_remaining_quantities(cr, uid, [picking.id], context=context)
                if todo_move_ids and not context.get('do_only_split'):
                    self.pool.get('stock.move').action_done(cr, uid, todo_move_ids, context=context)
                elif context.get('do_only_split'):
                    context = dict(context, split=todo_move_ids)
            self._create_backorder(cr, uid, picking, context=context)
            if toassign_move_ids:
                stock_move_obj.action_assign(cr, uid, toassign_move_ids, context=context)
        return True
        
