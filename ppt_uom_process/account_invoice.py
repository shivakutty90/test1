# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp

class account_invoice_line(models.Model):
    _inherit = "account.invoice.line"
    
    #~ @api.model
    #~ def _get_default_name(self):
        #~ res = self.env['product.uom'].search([('approved_supplier','=',True), ('mode_type','=','bottle')])
        #~ if res:
            #~ return res[0]
        #~ else:
            #~ return False
    
    uom_based_price = fields.Float(string='Pack/Case Price', digits= dp.get_precision('Product Price'), default=0.0, readonly=True)
    uom_qty = fields.Float(string='Pack/Case Qty', digits= dp.get_precision('Product Unit of Measure'), default=0.0, readonly=True)
    uom_mode = fields.Many2one('product.uom', string='Mode',domain = [('approved_supplier','=',True)], readonly=True)
    
