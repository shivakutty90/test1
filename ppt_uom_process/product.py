# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class product_supplierinfo(osv.osv):
    _inherit = 'product.supplierinfo'
    _columns = {
        'uom_mode': fields.many2one('product.uom', 'Mode [Purchase UoM]', domain = [('approved_supplier','=',True)], ondelete='cascade'),
        'no_of_pcs': fields.float('No.of Each'),
        'no_of_pcs_per_pack': fields.float('No.of Each Per Pack'),
        'no_of_pack_per_case': fields.float('No.of.Pack per Case'),
        'mode_type': fields.selection(
                [('bottle', 'Bottle'),('case', 'Case'),('pack', 'Pack')],
                'Mode Type')
    }
    def _get_default_name(self, cr, uid, context=None):
        res = self.pool.get('product.uom').search(cr, uid, [('approved_supplier','=',True), ('mode_type','=','bottle')], context=context)
        if res:
            return res[0]
        else:
            return False
    _defaults = {
        'uom_mode':_get_default_name,
        'mode_type':'bottle',
    }
    
    def onchange_uom_mode(self, cr, uid, ids, uom_mode, context=None):
        if not uom_mode:
            return {'value': {}}

        uom_br = self.pool.get('product.uom').browse(cr, uid, uom_mode, context=context)
        if uom_br.mode_type:
            val = {
                'mode_type': uom_br.mode_type,
            }        
        return {'value': val}
