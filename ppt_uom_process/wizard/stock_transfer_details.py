# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.odoo.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime

class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'
    _description = 'Picking wizard'

    #~ partner_id = fields.Many2one('res.partner', 'Partner')
    

    def default_get(self, cr, uid, fields, context=None):
        if context is None: context = {}
        res = super(stock_transfer_details, self).default_get(cr, uid, fields, context=context)
        picking_ids = context.get('active_ids', [])
        active_model = context.get('active_model')

        if not picking_ids or len(picking_ids) != 1:
            # Partial Picking Processing may only be done for one picking at a time
            return res
        assert active_model in ('stock.picking'), 'Bad context propagation'
        picking_id, = picking_ids
        picking = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
        items = []
        packs = []
        
        if not picking.pack_operation_ids:
            picking.do_prepare_partial()
        for op in picking.pack_operation_ids:
            item = {
                'packop_id': op.id,
                'product_id': op.product_id.id,
                'product_uom_id': op.product_uom_id.id,
                'quantity': op.product_qty,
                'package_id': op.package_id.id,
                'lot_id': op.lot_id.id,
                'sourceloc_id': op.location_id.id,
                'destinationloc_id': op.location_dest_id.id,
                'result_package_id': op.result_package_id.id,
                'date': op.date, 
                'owner_id': op.owner_id.id,
                'uom_mode': op.uom_mode.id,
                'uom_qty':op.uom_qty,
                'partner_id':op.partner_id.id,
                'move_id':op.move_id.id,
                'tot_uom_qty':op.uom_qty,
            }
            if op.product_id:
                items.append(item)
            elif op.package_id:
                packs.append(item)
        res.update(item_ids=items)
        res.update(packop_ids=packs)
        return res
    
    @api.one
    def do_detailed_transfer(self):
        processed_ids = []
        # Create new and update existing pack operations
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                    'uom_mode': prod.uom_mode.id,
                    'uom_qty':prod.uom_qty,
                    'partner_id':prod.partner_id.id,
                    
                }
                prod.move_id.write({'remaining_uom_qty':prod.tot_uom_qty - prod.uom_qty})
                if prod.packop_id:
                    prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
        # Delete the others
        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        packops.unlink()

        # Execute the transfer of the picking
        self.picking_id.do_transfer()

        return True
    

class stock_transfer_details_items(models.TransientModel):
    _inherit = 'stock.transfer_details_items'
    _description = 'Picking wizard items'    
    
    uom_mode = fields.Many2one('product.uom', 'UOM Mode')
    uom_qty = fields.Float('Pack/Case Qty', digits=dp.get_precision('Product Unit of Measure'), default = 1.0)
    partner_id = fields.Many2one('res.partner', 'Partner')
    move_id = fields.Many2one('stock.move', 'Stock Move')
    tot_uom_qty = fields.Float('TOT QTY', digits=dp.get_precision('Product Unit of Measure'), default = 1.0)


    @api.multi
    def uom_qty_change(self, product, uom_mode, uom_qty, partner_id):
        result = {}
        if product:
            prod = self.env['product.product'].browse(product)
            uom_qty_vals = {}
            for s_line in prod.seller_ids:
                if s_line.name.id == partner_id:
                    if s_line.uom_mode.mode_type == 'bottle':
                        uom_qty_vals['b_qty'] = s_line.no_of_pcs
                    elif s_line.uom_mode.mode_type == 'case':
                        uom_qty_vals['c_qty'] = s_line.no_of_pack_per_case
                    elif s_line.uom_mode.mode_type == 'pack':
                        uom_qty_vals['p_qty'] = s_line.no_of_pcs_per_pack
                        
            if uom_qty_vals:
                uom_mode_br = self.env['product.uom'].browse(uom_mode)
                if uom_mode_br.mode_type == 'bottle' and uom_qty_vals.get('b_qty'):
                    result['quantity'] = uom_qty_vals.get('b_qty') * uom_qty
                elif uom_mode_br.mode_type == 'pack' and uom_qty_vals.get('p_qty'):
                    result['quantity'] = uom_qty_vals.get('p_qty') * uom_qty
                elif uom_mode_br.mode_type == 'case' and uom_qty_vals.get('c_qty') and uom_qty_vals.get('p_qty'):
                    result['quantity'] = uom_qty_vals.get('c_qty') * uom_qty_vals.get('p_qty') * uom_qty
                else:
                    result['quantity'] = qty or 1.0
        return {'value': result, 'domain': {}, 'warning':{} }
